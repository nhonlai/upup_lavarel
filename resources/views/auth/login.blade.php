@extends('layouts.base')

@section('content.base')
    <section id="login" class="vertical-center">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="formWrapper">
                        <form method="POST" action="{{ url('/admin/login') }}">
                            {{ csrf_field() }}
                            <div class="text-center">
                                <img src="/images/logo_login.png" width="207" height="88"/>
                                <h4>ADMIN</h4>
                            </div>
                            <div class="form-group{{ ($errors->has('email'))? ' has-error' : '' }}">
                                <input type="email"
                                       class="form-control{{ ($errors->has('email'))? ' has-error' : '' }}"
                                       name="email" placeholder="Email" required="" autofocus="" value="{{ old('email') }}" />
                            </div>
                            <div class="form-group{{ ($errors->has('email'))? ' has-error' : '' }}">
                                <input type="password"
                                       class="form-control{{ ($errors->has('email'))? ' has-error' : '' }}"
                                       name="password" placeholder="Password" required=""/>
                            </div>
                            <div class="alert alert-danger" role="alert">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="checkbox text-center">
                                <input type="checkbox" id="rememberMe" name="remember"/>
                                <label for="rememberMe">Remember me</label>
                            </div>
                            <button class="btn btn-lg btn-success btn-block" type="submit">LOGIN</button>
                            <div class="form-group text-center">
                                <a href="{{ url('/admin/password/reset') }}" class="linkForgotPassword">Forgot
                                    Password</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop