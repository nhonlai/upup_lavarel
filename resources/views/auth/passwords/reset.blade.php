@extends('layouts.base')

@section('content.base')
    <section id="login" class="vertical-center">
        <div class="container"  style="min-height: 750px">
            <div class="text-center">
                <img src="/images/logo_login.png" width="207" height="88"/>
                <h4>PASSWORD RESET</h4>
            </div>
            @if (isset($message))
                <div class="row" style="margin-top:30px">
                    <div class="alert alert-success text-center" role="alert">{{  $message }}</div>
                </div>
            @elseif (!$errors->isEmpty() && $errors->has('token'))
                <div class="row" style="margin-top:30px">
                    <div class="alert alert-danger text-center" role="alert">{{  $errors->first('token') }}</div>
                </div>
            @else
            <div class="row" style="margin-top:30px">
                <div class="col-md-8 col-md-offset-2">
                    <div class="formWrapper sectionForm">
                    <form class="form-horizontal" role="form" method="POST" action="">
                        {{ csrf_field() }}
                            <div class="row">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}" style="margin-bottom: 8px;">
                                    <label class="col-md-4 control-label">Password</label>

                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="password" value="" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="row form-group {{ $errors->count() === 1 && $errors->has('confirm_password') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">Confirm Password</label>
                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="confirm_password" required>

                                        @if (!$errors->isEmpty())
                                            <div class="has-error">
                                            <span class="help-block">
                                                <strong>{{ $errors->first() }}</strong>
                                            </span>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-refresh"></i>Reset Password
                                    </button>
                                </div>
                            </div>

                    </form>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </section>
    {!! HTML::script('/js/jquery-2.1.0.min.js') !!}
    <script>
        $(document).ready(function(){
            $( 'input[name=password]').val('');
            $( 'input[name=confirm_password]').val('');
        });
    </script>
@endsection
