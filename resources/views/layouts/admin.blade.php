<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="<?= csrf_token() ?>">

    <title>@yield('title', 'FBM')</title>

    {!! HTML::style('/css/bootstrap.min.css', ['charset' => 'UTF-8']) !!}
    @yield('css.admin')
    {!! HTML::style('/css/main.css', ['charset' => 'UTF-8']) !!}

</head>

<body>


<!-- Begin page content -->
@yield('content.admin')



@include('layouts.partials.footer')

{!! HTML::script('/js/jquery-2.1.0.min.js') !!}
{!! HTML::script('/js/bootstrap.min.js') !!}

@yield('js.admin')


</body>
</html>
