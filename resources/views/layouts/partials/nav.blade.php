
<?php
$routeName = Route::currentRouteName();
?>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img src="/images/logo_small@3x.png" width="168" height="42" />
            </a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="{{ (str_contains($routeName, 'admin.users')) ? 'active' : '' }}">
                    <a href="{{ url('admin/users') }}">Users</a>
                </li>
                <li class="{{ (str_contains($routeName, 'admin.promotions')) ? 'active' : '' }}">
                    <a href="{{ url('admin/promotions') }}">Promotions</a>
                </li>
                <li class="{{ (str_contains($routeName, 'admin.locations')) ? 'active' : '' }}">
                    <a href="{{ url('admin/locations') }}">Locations</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Profile</a></li>
                <li><a href="{{ url('admin/logout') }}">Log Out</a></li>
            </ul>
        </div>
    </div>
</nav>