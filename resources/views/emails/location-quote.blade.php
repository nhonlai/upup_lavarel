<?php
$titles = [
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'phone' => 'Phone',
    'email' => 'Email',
    'company' => 'Company Name',
    'account_number' => 'FBM Account Number',
    'project_name' => 'Project Name',
    'project_number' => 'Job/Project #',
    'country' => 'Country',
    'state' => 'State',
    'city' => 'City',
];
$defaultNAFields = [
    'account_number',
    'project_name',
    'project_number',
    'country',
    'state',
    'city',
];
?>
Requester Details:
<br><br>
@foreach($titles as $key => $title)
    @if(isset($params[$key]) && strlen($params[$key]) > 0)
        {{ $title }}: {{ $params[$key] }}<br>
    @elseif(in_array($key, $defaultNAFields))
        {{ $title }}: NA<br>
    @endif
@endforeach
<br>
Project Type: {{ isset($params['project_type']) && strlen($params['project_type']) > 0 ? $params['project_type'] : 'NA' }}<br>
<br>
When do you need this material: {{ isset($params['when_you_need']) && strlen($params['when_you_need']) > 0 ? $params['when_you_need'] : 'NA' }}<br>
<br>
Project Summary:
@if(!empty($params['project_summary']))
    <br>{!! nl2br(htmlentities($params['project_summary'])) !!}<br>
@else
    NA
@endif
<br>
Product Categories:
@if(!empty($params['product_categories']))
    <br>
    @foreach($params['product_categories'] as $value)
        {{$value}}<br>
    @endforeach
@else
    NA
@endif
