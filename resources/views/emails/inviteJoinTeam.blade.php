<p>Hello,</p>

<p>You're receiving this e-mail because you have an invitation to join {{ $team->name }} team on UpUp</p>

<p>Click the link below to join.</p>

<a href="{{ $link }}" target="_blank">{{ $link }}</a>

<p>Thank you!</p>
<strong>UpUp Team</strong>
