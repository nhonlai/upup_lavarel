<p>Hello {{ $user->name }}</p>

<p>Welcome to FBM!</p>

<p>Please verify your email on your account by using the link below:</p>

<a href="{{ $link }}" target="_blank">{{ $link }}</a>

<p>Thank you!</p>
<strong>FBM Team</strong>
