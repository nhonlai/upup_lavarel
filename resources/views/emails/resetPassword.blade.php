<p>Hello {{ $user->name }}</p>

<p>You're receiving this e-mail because you have requested to reset your password</p>

<p>Click the link below to reset your password.</p>

<a href="{{ $link }}" target="_blank">{{ $link }}</a>

<p>Thank you!</p>
<strong>UpUp Team</strong>
