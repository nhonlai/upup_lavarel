<p>Hello {{ $user->name }},</p>

<p>You're receiving this e-mail because the following order has been delivered!</p>

<p>Message: {{ $text }}</p>

<p>Order:<br>
@foreach($params as $key => $value)
    @if(is_array($value))
    &emsp;{{ $key }}: {{ implode(', ', $value) }}
    @else
    &emsp;{{ $key }}: {{ $value }}<br>
    @endif
@endforeach
</p>

<p>Thank you!</p>
<strong>FBM Team</strong>
