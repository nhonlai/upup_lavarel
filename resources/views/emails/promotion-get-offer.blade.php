<?php
$titles = [
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'company' => 'Company Name',
    'country' => 'Country',
    'state' => 'State',
    'city' => 'City',
    'email' => 'Email',
    'phone' => 'Phone',
];
?>
@foreach($titles as $key => $title)
    @if(isset($params[$key]) && strlen($params[$key]) > 0)
        {{ $title }}: {{ $params[$key] }}<br>
    @endif
@endforeach
<br>
