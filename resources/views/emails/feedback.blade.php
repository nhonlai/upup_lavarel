<?php
$titles = [
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'phone' => 'Phone',
    'email' => 'Email',
    'company' => 'Company',
    'country' => 'Country',
    'state' => 'State',
    'city' => 'City',
];
?>
@foreach($titles as $key => $title)
    @if(isset($params[$key]) && strlen($params[$key]) > 0)
        {{ $title }}: {{ $params[$key] }}<br>
    @endif
@endforeach
<br>
Message:<br>
{!! nl2br(htmlentities($params['message'])) !!}

