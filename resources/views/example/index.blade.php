@extends('layouts.frontend')

@section('title')
    Example Page Title
    @stop

@section('main-title')
    Example Page2
    @stop

@section('additional.css')
    {!! HTML::styleLink('/assets/css/dashboard.css') !!}
@stop

@section('content')
    <p class="lead">Pin a fixed-height footer to the bottom of the viewport in desktop browsers with this custom HTML and CSS. A fixed navbar has been added with <code>padding-top: 60px;</code> on the <code>body > .container</code>.</p>
    <p>Back to <a href="../sticky-footer">the default sticky footer</a> minus the navbar.</p>

    @stop

@section('additional.js')
    {!! HTML::scriptLink('/assets/js/example.js') !!}
    @stop