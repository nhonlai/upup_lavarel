@extends('layouts.admin')

@section('css.admin')
    {!! HTML::style('/css/jquery.mCustomScrollbar.min.css') !!}
@endsection

@section('js.admin')
    {!! HTML::script('/js/jquery.mCustomScrollbar.concat.min.js') !!}
    {!! HTML::script('/js/mustache.min.js') !!}
    {!! HTML::script('/js/pages/users.js') !!}
@endsection

@section('content.admin')
    @include('layouts.partials.nav')

    <section>
        <div class="container">
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Users</li>
                </ol>
            </div>
            <div class="row sectionHeader">
                <div class="col-md-3">
                    <h4>
                        <span class="title">USERS</span>
                        <span class="total" id="totalUser">0</span>
                    </h4>
                </div>
                <div class="col-md-6">
                    <div class="searchWrapper">
                        <input id="searchText" type="search" placeholder="Search..." />
                    </div>
                </div>
                <div class="col-md-3 text-right">
                    <a id="btnAddUser" class="btn btn-primary" href="{{ url('admin/users/add') }}">+ADD USER</a>
                </div>
            </div>
            <div class="table-wrapper content">
                <table class="table mt-20 mb-0 sectionTable">
                    <thead>
                    <tr>
                        <th width="25%">Name</th>
                        <th width="25%">Company</th>
                        <th>Company ID</th>
                        <th width="200">&nbsp;</th>
                    </tr>
                    </thead>
                </table>
                <div class="table-body-wrapper">
                    <table id="users" class="table sectionTable">
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="row text-center">
                <span id="loading" style="display:none;">loading...</span>
            </div>
        </div>
    </section>
    <script id="user-template" type="x-template">
        <tr>
            <td width="25%" style="max-width:200px;word-wrap: break-word;">@{{ first_name }} </td>
            <td width="25%" style="max-width:200px;word-wrap: break-word;">@{{ last_name }}</td>
            <td>@{{ account_number }}</td>
            <td class="sectionLink" width="200">
                <a href="{{url('/')}}/admin/users/@{{ id }}/edit">Edit</a>
                <a href="#" class="delete">Delete</a>
            </td>
        </tr>
    </script>
    <div class="modal fade sectionModal" id="deleteUserModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">DELETE USER</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this user?</p>
                    <div class="info">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    <button type="button" class="btn btn-danger" id="btnDelete">DELETE</button>
                </div>
            </div>
        </div>
    </div>
@endsection
