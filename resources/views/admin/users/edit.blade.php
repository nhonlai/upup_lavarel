@extends('layouts.admin')

@section('css.admin')
@endsection

@section('js.admin')
    {!! HTML::script('/js/jquery.inputmask.bundle.js') !!}
    {!! HTML::script('/js/inputmask.phone.js') !!}
    {!! HTML::script('/js/pages/users.edit.js') !!}
@endsection

@section('content.admin')
    @include('layouts.partials.nav')

    <section>
        <form method="POST" action="{{ $user->exists ? url('admin/users/'.$user->id.'/edit') : url('admin/users/add') }}">
            {{ csrf_field() }}
            <div class="container">
                <div class="row">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('admin.users') }}">Users</a></li>
                        <li class="active">{{ ($user->exists) ? 'Edit User' : 'Add User' }}</li>
                    </ol>
                </div>
                <div class="row sectionHeader">
                    <div class="col-md-9">
                        <h4>
                            <span class="title">{{ $user->name }}</span>
                        </h4>
                    </div>
                    <div class="col-md-3 text-right">
                        <a class="btn btn-default" href="{{ $user->exists ? url('admin/users/'.$user->id) : url('admin/users') }}">CANCEL</a>
                        <button id="btnSave" class="btn btn-success" type="submit">SAVE</button>
                    </div>
                </div>
                <div class="row sectionForm mt-30">
                    @if(isset($message) || !$errors->isEmpty())
                    <div class="col-md-12 mb-10">
                        @if (isset($message))
                            <div class="alert alert-info" role="alert">{{  $message }}</div>
                        @endif
                        @if (!$errors->isEmpty())
                        <div class="alert alert-danger" role="alert">
                            <ul>
                            @foreach($errors->keys() as $key)
                                <li>{{ $errors->first($key) }}</li>
                            @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                    @endif
                    <div class="col-md-4">
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name">First name</label>
                            <input type="text" class="form-control" id="first_name" name="first_name"
                                   value="{{ $user->first_name }}" required />
                        </div>
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name">Last name</label>
                            <input type="text" class="form-control" id="last_name" name="last_name"
                                   value="{{ $user->last_name }}" required />
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone #</label>
                            <input type="tel" class="form-control" id="phone" name="phone" value="{{ $user->phone }}" />
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}"
                                   autocomplete="off" required />
                        </div>
                        @if(!$user->exists)
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : ''  }}">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password"
                                   value="{{ $user->company }}" autocomplete="off" required />
                        </div>
                        @endif
                        <div class="form-group">
                            <label for="company">Company</label>
                            <input type="text" class="form-control" id="company" name="company" value="{{ $user->company }}" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="companyID">Company ID / Account #</label>
                            <input type="text" class="form-control" id="companyID" name="account_number" value="{{ $user->account_number }}" />
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" id="address" name="address" value="{{ $user->address }}" />
                        </div>
                        <div class="form-group">
                            <label for="address2">Address 2</label>
                            <input type="text" class="form-control" id="address2" name="address2" value="{{ $user->address2 }}" />
                        </div>
                        <div class="form-group">
                            <label for="city">City</label>
                            <input type="text" class="form-control" id="city" name="city" value="{{ $user->city }}" />
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="state">State</label>
                                    <input type="text" class="form-control" id="state" name="state" value="{{ $user->state }}" />
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="zip">Zip</label>
                                    <input type="number" class="form-control" id="zip" name="zip" value="{{ $user->zip }}" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
