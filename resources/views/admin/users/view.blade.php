@extends('layouts.admin')

@section('css.admin')
@endsection

@section('js.admin')
    {!! HTML::script('/js/jquery.inputmask.bundle.js') !!}
    {!! HTML::script('/js/inputmask.phone.js') !!}
    {!! HTML::script('/js/pages/users.edit.js') !!}
@endsection

@section('content.admin')
    @include('layouts.partials.nav')

    <form>
        <div class="container">
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="{{ route('admin.users') }}">Users</a></li>
                    <li class="active">Details</li>
                </ol>
            </div>
            <div class="row sectionHeader">
                <div class="col-md-9">
                    <h4>
                        <span class="title">{{ $user->name }}</span>
                    </h4>
                </div>
                <div class="col-md-3 text-right">
                    <a href="{{ url('admin/users/'.$user->id.'/edit') }}" type="button" class="btn btn-warning">EDIT</a>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#deleteUserModal">DELETE</button>
                </div>
            </div>
            <div class="row sectionForm mt-30">
                @if($message || !$errors->isEmpty())
                    <div class="col-md-12 mb-10">
                        @if ($message)
                            <div class="alert alert-info" role="alert">{{  $message }}</div>
                        @endif
                        @if (!$errors->isEmpty())
                            <div class="alert alert-danger" role="alert">
                                <ul>
                                    @foreach($errors->keys() as $key)
                                        <li>{{ $errors->first($key) }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                @endif
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="company">Company</label>
                        <input type="text" class="form-control" id="company" readonly value="{{ $user->company }}" />
                    </div>
                    <div class="form-group">
                        <label for="companyID">Company ID / Account #</label>
                        <input type="text" class="form-control" id="companyID" readonly value="{{ $user->account_number }}" />
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone #</label>
                        <input type="tel" class="form-control" id="phone" readonly value="{{ $user->phone }}" />
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" readonly value="{{ $user->email }}" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" id="address" readonly value="{{ $user->address }}" />
                    </div>
                    <div class="form-group">
                        <label for="address2">Address 2</label>
                        <input type="text" class="form-control" id="address2" readonly value="{{ $user->address2 }}" />
                    </div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" class="form-control" id="city" readonly value="{{ $user->city }}" />
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="state">State</label>
                                <input type="text" class="form-control" id="state" readonly value="{{ $user->state }}" />
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="zip">Zip</label>
                                <input type="text" class="form-control" id="zip" readonly value="{{ $user->zip }}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="modal fade sectionModal" id="deleteUserModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">DELETE USER</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this user?</p>
                    <div class="info">
                        <p>{{ $user->name }}</p>
                        <p>{{ $user->company }}</p>
                        <p>{{ $user->phone }}</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    <form action="{{ route('admin.users.delete', ['id' => $user->id]) }}" method="POST" style="display:inline;">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-danger" id="delete-user">DELETE</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
