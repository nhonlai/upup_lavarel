@extends('layouts.admin')

@section('css.admin')
@endsection

@section('js.admin')
    {!! HTML::script('/js/select2.min.js') !!}
    {!! HTML::script('/js/moment-with-locales.js') !!}
    {!! HTML::script('/js/bootstrap-datetimepicker.js') !!}
    {!! HTML::script('/js/pages/promotions.edit.js') !!}
@endsection

@section('content.admin')
    @include('layouts.partials.nav')

    <section>
        <form>
            <div class="container">
                <div class="row">
                    <ol class="breadcrumb">
                        <li><a href="{{ route("admin.promotions") }}">Promotions</a></li>
                        <li class="active">Details</li>
                    </ol>
                </div>
                <div class="row sectionHeader">
                    <div class="col-md-3">
                        <h4>
                            <span class="title">{{ $promotion->name }}</span>
                        </h4>
                    </div>
                    <div class="col-md-offset-6 col-md-3 text-right">
                        <a href="{{ route('admin.promotions.edit.form', ['id' => $promotion->id]) }}"
                           type="button" class="btn btn-warning">EDIT</a>
                        <button class="btn btn-danger" type="button" data-toggle="modal"
                                data-target="#deletePromotionModal">DELETE
                        </button>
                    </div>
                </div>
                <div class="row sectionForm mt-30">
                    @if($message || !$errors->isEmpty())
                        <div class="col-md-12 mb-10">
                            @if ($message)
                                <div class="alert alert-info" role="alert">{{  $message }}</div>
                            @endif
                            @if (!$errors->isEmpty())
                                <div class="alert alert-danger" role="alert">
                                    <ul>
                                        @foreach($errors->keys() as $key)
                                            <li>{{ $errors->first($key) }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    @endif
                    <div class="col-md-4">
                        <div class="form-group">
                            @if ($promotion->image)
                                <div class="previewImage"
                                     style="background-image: url('{{ $promotion->getImageUrl() }}')"></div>
                            @else
                                <div class="previewImage"
                                     style="background-image: url('/images/img_default.jpg')"></div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group customSelectGroup">
                                    <label for="type">Type</label>
                                    <input type="text" class="form-control" id="type"
                                           value="{{ $promotion->type->name }}" readonly/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="typeCustom">&nbsp;</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control" id="typeCustom"
                                               value="{{ $promotion->amount }}" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="expDate">Exp. Date</label>
                                    <input type="text" class="form-control" id="expDate"
                                           value="{{ $promotion->expired_at ? $promotion->expired_at->format('m/d/Y') : '' }}" readonly/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="productName">Product Name</label>
                            <input type="text" class="form-control" id="productName"
                                   value="{{ $promotion->product_name }}" readonly/>
                        </div>
                    </div>
                </div>
                <div class="row sectionForm">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea id="description" class="form-control"
                                      readonly>{{ $promotion->description }}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="legalTerms">Legal Terms</label>
                            <textarea id="legalTerms" class="form-control" readonly>{{ $promotion->terms }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
    <div class="modal fade sectionModal" id="deletePromotionModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">DELETE PROMOTION</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this promotion?</p>
                    <div class="info">
                        <p>{{ $promotion->name }}</p>
                        <p>{{ $promotion->type->name }} - ${{ $promotion->amount }}</p>
                        <p>{{ $promotion->product_name }}</p>
                    </div>
                    <textarea class="form-control" style="height:120px;" readonly>{{ $promotion->description }}</textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    <form action="{{ route('admin.promotions.delete', ['id' => $promotion->id]) }}"
                          method="POST" style="display:inline;">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-danger" id="delete-promotion">DELETE</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
