@extends('layouts.admin')

@section('css.admin')
    {!! HTML::style('css/select2.min.css') !!}
    {!! HTML::style('css/bootstrap-datetimepicker.css') !!}
@endsection

@section('js.admin')
    {!! HTML::script('/js/select2.min.js') !!}
    {!! HTML::script('/js/moment-with-locales.js') !!}
    {!! HTML::script('/js/bootstrap-datetimepicker.js') !!}
    {!! HTML::script('/js/pages/promotions.edit.js') !!}
@endsection

@section('content.admin')
    @include('layouts.partials.nav')

    <section>
        <?php $formAction = $promotion->exists ?
                route('admin.promotions.edit', ['id' => $promotion->id]) :
                route('admin.promotions.add');
        ?>
        <form method="POST" action="{{ $formAction }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="container">
                <div class="row">
                    <ol class="breadcrumb">
                        <li><a href="{{ route("admin.promotions") }}">Promotions</a></li>
                        <li class="active">{{ $promotion->exists ? 'Edit' : 'Add' }} Promotion</li>
                    </ol>
                </div>
                <div class="row sectionHeader">
                    <div class="col-md-3">
                        <h4>
                            <span class="title">{{ $promotion->exists ? 'Edit' : 'Add' }} Promotion</span>
                        </h4>
                    </div>
                    <div class="col-md-offset-6 col-md-3 text-right">
                        <a href="{{ route('admin.promotions.view', ['id' => $promotion->id]) }}"
                           class="btn btn-default">CANCEL</a>
                        <button id="save-promotion" type="submit" class="btn btn-success">SAVE</button>
                    </div>
                </div>
                <div class="row sectionForm mt-30">
                    @if(!empty($message) || !$errors->isEmpty())
                        <div class="col-md-12 mb-10">
                            @if (isset($message))
                                <div class="alert alert-info" role="alert">{{  $message }}</div>
                            @endif
                            @if (!$errors->isEmpty())
                                <div class="alert alert-danger" role="alert">
                                    <ul>
                                        @foreach($errors->keys() as $key)
                                            <li>{{ $errors->first($key) }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    @endif
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Promotion Name</label>
                            <input type="text" class="form-control" id="name"
                                   name="name" value="{{ $promotion->name }}"/>
                        </div>
                    </div>
                </div>
                <div class="row sectionForm">
                    <div class="col-md-4">
                        <div class="form-group">
                            @if ($promotion->image)
                                <div class="previewImage" id="previewImage"
                                     style="background-image: url('{{ $promotion->getImageUrl() }}')"></div>
                            @else
                                <div class="previewImage" id="previewImage"
                                     style="background-image: url('/images/img_default.jpg')"></div>
                            @endif
                            <label class="btn btn-warning2 btn-file mt-30 mb-10">
                                REPLACE <input id="replaceImage" name="replaceImage" class="customUploadFile"
                                               type="file" style="display: none;">
                            </label>
                            <btn id="removeImage" class="btn btn-danger2 pt-10">REMOVE</btn>
                            <input type="hidden" id="removeImageOption" name="removeImageOption" value="0"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group customSelectGroup">
                                    <label for="type">Type</label>
                                    <select class="form-control customSelect" id="type" name="promotion_type_id">
                                        @foreach($types as $type)
                                            <option @selected($type->id == $promotion->promotion_type_id)
                                                    value="{{$type->id}}">{{$type->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="typeCustom">&nbsp;</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="number" step="any" class="form-control" id="typeCustom"
                                               name="amount" value="{{ $promotion->amount }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="expDate">Exp. Date</label>
                                    <div class="input-group date" id="datetimepicker1">
                                        <input type="text" class="form-control" id="expDate"
                                               placeholder="mm/dd/yyyy" name="expired_at"
                                               value="{{ $promotion->expired_at ? $promotion->expired_at->format('m/d/Y') : date('m/d/Y') }}"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="productName">Product Name</label>
                            <input type="text" class="form-control" id="productName" name="product_name"
                                   value="{{ $promotion->product_name }}"/>
                        </div>
                    </div>
                </div>
                <div class="row sectionForm">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea id="description" name="description"
                                      class="form-control">{{ $promotion->description }}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="legalTerms">Legal Terms</label>
                            <textarea id="legalTerms" name="terms"
                                      class="form-control">{{ $promotion->terms }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
