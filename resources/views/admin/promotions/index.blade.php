@extends('layouts.admin')

@section('css.admin')
    {!! HTML::style('/css/jquery.mCustomScrollbar.min.css') !!}
@endsection

@section('js.admin')
    {!! HTML::script('/js/jquery.mCustomScrollbar.concat.min.js') !!}
    {!! HTML::script('/js/mustache.min.js') !!}
    {!! HTML::script('/js/pages/paginator.js') !!}
    {!! HTML::script('/js/pages/promotions.js') !!}
@endsection

@section('content.admin')
    @include('layouts.partials.nav')

    <section>
        <div class="container">
            <div class="row">
                <ol class="breadcrumb">
                    <li class="active">Promotions</li>
                </ol>
            </div>
            <div class="row sectionHeader">
                <div class="col-md-3">
                    <h4>
                        <span class="title">PROMOTIONS</span>
                        <span class="total" id="totalPromotion">0</span>
                    </h4>
                </div>
                <div class="col-md-6">
                    <div class="searchWrapper">
                        <input id="search-text" type="search" placeholder="Search..." />
                    </div>
                </div>
                <div class="col-md-3 text-right">
                    <a href="{{ route('admin.promotions.add.form') }}" class="btn btn-primary">+ADD PROMOTION</a>
                </div>
            </div>
            <div class="table-wrapper content">
                <table class="table mt-20 mb-0 sectionTable">
                    <thead>
                    <tr>
                        <th width="100">&nbsp;</th>
                        <th width="30%">Promotion Name</th>
                        <th width="15%">Exp. Date</th>
                        <th width="15%">Type</th>
                        <th>Product</th>
                    </tr>
                    </thead>
                </table>
                <div class="table-body-wrapper">
                    <table id="promotion-list" class="table sectionTable">
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row text-center">
                <span id="loading" style="display:none;">loading...</span>
            </div>
        </div>
    </section>
    <script id="promotion-tpl" type="x-template">
        <tr>
            <td width="100" align="center">
                @{{ #image }}
                <img src="@{{ image }}" width="43" height="34" />
                @{{ /image }}
                @{{ ^image }}
                <img src="/images/img_default.png" />
                @{{ /image }}
            </td>
            <td width="30%"><a href="/admin/promotions/@{{ id }}" class="btn-link">@{{ name }}</a></td>
            <td width="15%">@{{ expired_at }}</td>
            <td width="15%">@{{ promotion_type }}</td>
            <td>@{{ product_name }}</td>
        </tr>
    </script>
@endsection
