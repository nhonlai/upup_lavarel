@extends('layouts.admin')

@section('css.admin')
    {!! HTML::style('css/select2.min.css') !!}
    {!! HTML::style('css/bootstrap-datetimepicker.css') !!}
@endsection

@section('js.admin')
    {!! HTML::script('/js/select2.min.js') !!}
    {!! HTML::script('/js/moment-with-locales.js') !!}
    {!! HTML::script('/js/bootstrap-datetimepicker.js') !!}
    {!! HTML::script('/js/pages/locations.edit.js') !!}
    {!! HTML::script('/js/pages/locations.map.js') !!}
    <script src="//maps.googleapis.com/maps/api/js?key={{ config('services.googlemaps.api_key') }}&libraries=places&callback=initMap"
            async defer></script>
@endsection

@section('content.admin')
    @include('layouts.partials.nav')

    <section>
        <?php $formAction = $location->exists ?
                route('admin.locations.edit', ['id' => $location->id]) :
                route('admin.locations.add');
        ?>
        <form action="{{ $formAction }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="container">
                <div class="row">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('admin.locations') }}">Locations</a></li>
                        <li class="active">{{ $location->id ? 'Edit' : 'New' }} Location</li>
                    </ol>
                </div>
                <div class="row sectionHeader">
                    <div class="col-md-3">
                        <h4>
                            <span class="title">{{ $location->id ? 'Edit' : 'New' }} Location</span>
                        </h4>
                    </div>
                    <div class="col-md-offset-6 col-md-3 text-right">
                        <a href="{{ route('admin.locations.view', ['id' => $location->id]) }}"
                           class="btn btn-default">CANCEL</a>
                        <button id="save-location" class="btn btn-success">SAVE</button>
                    </div>
                </div>
                <div class="row sectionForm mt-30">
                    @if(!empty($message) || !$errors->isEmpty())
                        <div class="col-md-12 mb-10">
                            @if (isset($message))
                                <div class="alert alert-info" role="alert">{{  $message }}</div>
                            @endif
                            @if (!$errors->isEmpty())
                                <div class="alert alert-danger" role="alert">
                                    <ul>
                                        @foreach($errors->keys() as $key)
                                            <li>{{ $errors->first($key) }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    @endif
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="locationName">Location Name</label>
                            <input type="text" class="form-control google-maps-input" id="locationName"
                                   value="{{ $location->name }}" name="name" required />
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="branch">Branch #</label>
                                    <input type="text" class="form-control" id="branch"
                                           value="{{ $location->branch_number }}" name="branch_number" />
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group customSelectGroup">
                                    <label for="type">Type</label>
                                    <select class="form-control customSelect" id="type" name="location_type_id">
                                        @foreach($types as $type)
                                            <option @selected($type->id == $location->location_type_id)
                                                    value="{{$type->id}}">{{$type->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" id="address"
                                   value="{{ $location->address }}" name="address" required placeholder="Enter an address"/>
                            <input type="hidden" id="lat" name="lat" value="{{ $location->lat }}"/>
                            <input type="hidden" id="lng" name="lng" value="{{ $location->lng }}"/>
                        </div>
                        <div class="form-group">
                            <label for="address2">Address 2</label>
                            <input type="text" class="form-control" id="address2"
                                   value="{{ $location->address2 }}" name="address2" />
                        </div>
                        <div class="form-group">
                            <label for="city">City</label>
                            <input type="text" class="form-control" id="city"
                                   value="{{ $location->city }}" name="city" required />
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="state">State/Province</label>
                                    <input type="text" class="form-control" id="state"
                                           value="{{ $location->state }}" name="state"  />
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="zip">Zip</label>
                                    <input type="text" class="form-control" id="zip"
                                           value="{{ $location->zip }}" name="zip" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="country">Country</label>
                            <input type="text" class="form-control" id="country"
                                   value="{{ $location->country }}" name="country" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="phone">Phone #</label>
                            <input type="tel" class="form-control" id="phone"
                                   value="{{ $location->phone }}" name="phone" />
                        </div>
                        <div class="form-group">
                            <label for="emailToPlaceOrder">Email to Place Order</label>
                            <input type="email" class="form-control" id="emailToPlaceOrder"
                                   value="{{ $location->order_email }}" name="order_email" required />
                        </div>
                        <div class="form-group">
                            <label for="emailToGetQuote">Email to Get Quote</label>
                            <input type="email" class="form-control" id="emailToGetQuote"
                                   value="{{ $location->quote_email }}" name="quote_email" required />
                        </div>
                        <div class="form-group">
                            <label for="emailToReceiveQuestion">Email to receive Question</label>
                            <input type="email" class="form-control" id="emailToReceiveQuestion"
                                   value="{{ $location->question_email }}" name="question_email" required />
                        </div>
                        <div class="form-group uploadFileAltWrapper">
                            @if ($location->image)
                                <div id="previewImage" class="previewImage" style="background-image: url('{{ $location->getImageUrl() }}')"></div>
                            @else
                                <div id="previewImage" class="previewImage"></div>
                            @endif
                            <label class="btn btn-warning2 btn-file mt-20">
                                REPLACE <input class="customUploadFile" id="replaceImage" name="replaceImage" type="file" style="display: none;">
                            </label>
                            <button type="button" id="removeImage" class="btn btn-danger2 mt-20 pt-10 ml-10">REMOVE</button>
                            <input type="hidden" id="removeImageOption" name="removeImageOption" value="0"/>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
    <div id="map" style="height: 100%; display:none"></div>
    <script type="text/javascript">//<![CDATA[

        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        function initMap() {
            var myMap = new LocationMap('address');
            myMap.initialize();
        }
        //]]>

    </script>
@endsection
