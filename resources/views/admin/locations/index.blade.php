@extends('layouts.admin')

@section('css.admin')
    {!! HTML::style('/css/jquery.mCustomScrollbar.min.css', ['charset' => 'UTF-8']) !!}
@endsection

@section('js.admin')
    {!! HTML::script('/js/jquery.mCustomScrollbar.concat.min.js') !!}
    {!! HTML::script('/js/mustache.min.js') !!}
    {!! HTML::script('/js/pages/paginator.js') !!}
    {!! HTML::script('/js/pages/locations.js') !!}
@endsection

@section('content.admin')
    @include('layouts.partials.nav')

    <section>
        <div class="container">
            <div class="row">
                <ol class="breadcrumb">
                    <li class="active">Locations</li>
                </ol>
            </div>
            <div class="row sectionHeader">
                <div class="col-md-3">
                    <h4>
                        <span class="title">LOCATIONS</span>
                        <span class="total" id="total">0</span>
                    </h4>
                </div>
                <div class="col-md-6">
                    <div class="searchWrapper">
                        <input id="search-text" type="search" placeholder="Search..." />
                    </div>
                </div>
                <div class="col-md-3 text-right">
                    <a href="{{ route('admin.locations.add') }}" class="btn btn-primary">+ADD LOCATION</a>
                </div>
            </div>
            <div class="table-wrapper content">
                <table class="table mt-20 mb-0 sectionTable">
                    <thead>
                    <tr>
                        <th width="30%">Name</th>
                        <th width="35%">Address</th>
                        <th>Phone</th>
                    </tr>
                    </thead>
                </table>
                <div class="table-body-wrapper">
                    <table id="location-list" class="table sectionTable">
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row text-center">
                <span id="loading" style="display:none;">loading...</span>
            </div>
        </div>
    </section>
    <script id="location-tpl" type="x-template">
        <tr>
            <td width="30%"><a href="/admin/locations/@{{ id }}" class="btn-link">@{{ name }}</a></td>
            <td width="35%">@{{ address }}</td>
            <td>@{{ phone }}</td>
        </tr>
    </script>
@endsection
