@extends('layouts.admin')

@section('css.admin')
@endsection

@section('js.admin')
    {!! HTML::script('/js/select2.min.js') !!}
    {!! HTML::script('/js/moment-with-locales.js') !!}
    {!! HTML::script('/js/bootstrap-datetimepicker.js') !!}
    {!! HTML::script('/js/pages/locations.edit.js') !!}
@endsection

@section('content.admin')
    @include('layouts.partials.nav')

    <section>
        <form>
            <div class="container">
                <div class="row">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('admin.locations') }}">Locations</a></li>
                        <li class="active">Details</li>
                    </ol>
                </div>
                <div class="row sectionHeader">
                    <div class="col-md-3">
                        <h4>
                            <span class="title">Location Details</span>
                        </h4>
                    </div>
                    <div class="col-md-offset-6 col-md-3 text-right">
                        <a href="{{ route('admin.locations.edit.form', ['id' => $location->id]) }}"
                           type="button" class="btn btn-warning">EDIT</a>
                        <button class="btn btn-danger" type="button" data-toggle="modal"
                                data-target="#deletePromotionModal">DELETE
                        </button>
                    </div>
                </div>
                <div class="row sectionForm mt-30">
                    @if(!empty($message) || !$errors->isEmpty())
                        <div class="col-md-12 mb-10">
                            @if (isset($message))
                                <div class="alert alert-info" role="alert">{{  $message }}</div>
                            @endif
                            @if (!$errors->isEmpty())
                                <div class="alert alert-danger" role="alert">
                                    <ul>
                                        @foreach($errors->keys() as $key)
                                            <li>{{ $errors->first($key) }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    @endif
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="locationName">Location Name</label>
                            <input type="text" class="form-control" id="locationName"
                                   value="{{ $location->name }}" readonly />
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="branch">Branch #</label>
                                    <input type="text" class="form-control" id="branch"
                                           value="{{ $location->branch_number }}" readonly />
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group customSelectGroup">
                                    <label for="type">Type</label>
                                    <input type="text" class="form-control" id="type"
                                           value="{{ $location->type->name }}" readonly/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" id="address"
                                   value="{{ $location->address }}" readonly />
                        </div>
                        <div class="form-group">
                            <label for="address2">Address 2</label>
                            <input type="text" class="form-control" id="address2"
                                   value="{{ $location->address2 }}" readonly />
                        </div>
                        <div class="form-group">
                            <label for="city">City</label>
                            <input type="text" class="form-control" id="city"
                                   value="{{ $location->city }}" readonly/>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="state">State/Province</label>
                                    <input type="text" class="form-control" id="state"
                                           value="{{ $location->state }}" readonly />
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="zip">Zip</label>
                                    <input type="text" class="form-control" id="zip"
                                           value="{{ $location->zip }}" readonly />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="country">Country</label>
                            <input type="text" class="form-control" id="country"
                                   value="{{ $location->country }}" readonly />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="phone">Phone #</label>
                            <input type="tel" class="form-control" id="phone" value="{{ $location->phone }}" readonly />
                        </div>
                        <div class="form-group">
                            <label for="emailToPlaceOrder">Email to Place Order</label>
                            <input type="email" class="form-control" id="emailToPlaceOrder" value="{{ $location->order_email }}" readonly/>
                        </div>
                        <div class="form-group">
                            <label for="emailToGetQuote">Email to Get Quote</label>
                            <input type="email" class="form-control" id="emailToGetQuote" value="{{ $location->quote_email }}" readonly/>
                        </div>
                        <div class="form-group">
                            <label for="emailToReceiveQuestion">Email to receive Question</label>
                            <input type="email" class="form-control" id="emailToReceiveQuestion" value="{{ $location->question_email }}" readonly />
                        </div>
                        <div class="form-group uploadFileAltWrapper">
                            @if ($location->image)
                                <div class="previewImage" style="background-image: url('{{ $location->getImageUrl() }}')"></div>
                            @else
                                <div class="previewImage"></div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
    <div class="modal fade sectionModal" id="deletePromotionModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">DELETE PROMOTION</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this promotion?</p>
                    <div class="info">
                        <p>{{ $location->name }}</p>
                        <p>{{ $location->address }}</p>
                        <p>{{ $location->address2 }}</p>
                        <p>{{ $location->city }} {{ $location->state ? ', ' . $location->state : '' }} {{ $location->zip }}</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    <form action="{{ route('admin.locations.delete', ['id' => $location->id]) }}"
                          method="POST" style="display:inline;">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-danger" id="delete-location">DELETE</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
