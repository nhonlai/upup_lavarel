#!/bin/bash

# This script is executed with normal (vagrant user) permissions

cd /vagrant

# Create sample configuration
if [ ! -f .env ]; then
	cp .env.homestead .env
fi

. /tmp/functions_common.sh

#header "Install Composer dependencies"
#composer global require "hirak/prestissimo" # Speed up Composer install
#composer install --prefer-source --no-interaction

#header "Install Bower dependencies"
#bower install

#header "Install NPM dependencies"
#npm set progress=false # Speed Up NPM install
#npm install --quiet

header "Initialize application"
# Initialize application
php artisan key:generate
php artisan migrate
php artisan db:seed

# Update static files
#grunt build
