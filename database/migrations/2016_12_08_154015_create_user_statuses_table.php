<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_statuses', function (Blueprint $table) {
            $table->smallInteger('id', false, true);
            $table->string('name');
            $table->primary('id');
        });

        $data = [
            [
                'id' => 1,
                'name' => 'pending',
            ],
            [
                'id' => 2,
                'name' => 'verified',
            ],
            [
                'id' => 3,
                'name' => 'active',
            ]

        ];
        foreach($data as $status) {
            \App\Model\UserStatus::create($status);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_statuses');
    }
}
