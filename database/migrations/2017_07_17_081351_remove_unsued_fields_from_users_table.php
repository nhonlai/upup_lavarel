<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUnsuedFieldsFromUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['badges']);
            $table->dropColumn(['points']);
            $table->dropColumn(['level']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedMediumInteger('badges')->default(0);
            $table->unsignedMediumInteger('points')->default(0)->after('badges');
            $table->smallInteger('level')->unsigned()->default(1)->after('points');
        });
    }
}
