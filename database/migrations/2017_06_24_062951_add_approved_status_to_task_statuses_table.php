<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApprovedStatusToTaskStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                'id' => 4,
                'name' => 'Approved',
                'is_system' => 1,
            ],
        ];
        foreach($data as $status) {
            \App\Model\TaskStatus::create($status);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Model\TaskStatus::find(4)->delete();
    }
}
