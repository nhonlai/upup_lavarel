<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveOrganizationIdAndTeamIdFromUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['organization_id']);
            $table->dropColumn(['organization_id']);
            $table->dropForeign(['team_id']);
            $table->dropColumn(['team_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('organization_id')->nullable()->after('status_id');
            $table->foreign('organization_id')->references('id')->on('organizations');
            $table->unsignedInteger('team_id')->nullable()->after('organization_id')->index();
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('set null');
        });
    }
}
