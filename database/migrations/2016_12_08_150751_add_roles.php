<?php

use Illuminate\Database\Migrations\Migration;
use Bican\Roles\Models\Role;

class AddRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Role::create([
            'name' => 'Founder',
            'slug' => 'founder'
        ]);
        Role::create([
            'name' => 'Manager',
            'slug' => 'manager'
        ]);
        Role::create([
            'name' => 'Member',
            'slug' => 'member'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
