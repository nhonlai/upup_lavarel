<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('task_statuses');
        Schema::create('task_statuses', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('name', 40);
            $table->string('description')->nullable();
            $table->string('icon')->nullable();
            $table->string('color')->nullable();
            $table->unsignedTinyInteger('is_system', false)->default(0);
            $table->unsignedInteger('organization_id')->nullable()->index();
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('set null');
            $table->timestamps();
        });

        $data = [
            [
                'id' => 1,
                'name' => 'Open',
                'is_system' => 1,
            ],
            [
                'id' => 2,
                'name' => 'In Progress',
                'is_system' => 1,
            ],
            [
                'id' => 3,
                'name' => 'Done',
                'is_system' => 1,
            ]

        ];
        foreach($data as $status) {
            \App\Model\TaskStatus::create($status);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('task_statuses');
    }
}
