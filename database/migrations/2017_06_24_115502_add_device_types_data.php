<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeviceTypesData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $deviceType = new \App\Model\DeviceType();
        $deviceType->id = 1;
        $deviceType->name = 'ios';
        $deviceType->save();

        $deviceType = new \App\Model\DeviceType();
        $deviceType->id = 2;
        $deviceType->name = 'android';
        $deviceType->save();

        $deviceType = new \App\Model\DeviceType();
        $deviceType->id = 3;
        $deviceType->name = 'web';
        $deviceType->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Model\DeviceType::whereIn('id', [1,2,3])->delete();
    }
}
