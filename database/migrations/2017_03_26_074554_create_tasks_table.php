<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tasks');
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->timestamp('due_date')->nullable()->index();
            $table->unsignedMediumInteger('points')->default(0);
            $table->unsignedSmallInteger('required_level', false)->default(0);
            $table->unsignedSmallInteger('status_id', false)->default(1)->index();
            $table->unsignedSmallInteger('badge_reward_id', false)->nullable()->index();
            $table->unsignedInteger('key_result_id')->nullable()->index();
            $table->unsignedInteger('organization_id')->nullable()->index();
            $table->unsignedInteger('assignee_id')->nullable()->index();
            $table->unsignedInteger('created_by')->nullable()->index();
            $table->foreign('status_id')->references('id')->on('task_statuses');
            $table->foreign('badge_reward_id')->references('id')->on('badge_rewards')->onDelete('set null');
            $table->foreign('key_result_id')->references('id')->on('key_results')->onDelete('set null');
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('set null');
            $table->foreign('assignee_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasks');
    }
}
