<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKeyResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('key_results', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->timestamp('due_date')->nullable()->index();
            $table->unsignedMediumInteger('start_value')->default(0);
            $table->unsignedMediumInteger('target_value')->default(0);
            $table->unsignedMediumInteger('weight')->default(0);
            $table->unsignedMediumInteger('completed_percent')->default(0);
            $table->unsignedInteger('objective_id')->index();
            $table->unsignedInteger('team_id')->nullable()->index();
            $table->unsignedInteger('organization_id')->nullable()->index();
            $table->unsignedInteger('created_by')->nullable()->index();
            $table->foreign('objective_id')->references('id')->on('objectives')->onDelete('cascade');
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('set null');
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('key_results');
    }
}
