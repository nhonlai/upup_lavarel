<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBadgeRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('badge_rewards');
        Schema::create('badge_rewards', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('title', 40);
            $table->unsignedMediumInteger('badges')->default(0);
            $table->unsignedMediumInteger('points')->default(0);
        });

        $titles = [
            'A new rise Hero',
            'So far so good',
            'You make the difference',
            'You\'r Rock dude',
        ];
        $data = [
            [
                'badges' => 20,
                'points' => 10,
            ],
            [
                'badges' => 50,
                'points' => 30,
            ],
        ];
        foreach($data as $item) {
            foreach($titles as $title) {
                $item['title'] = $title;
                \App\Model\BadgeReward::create($item);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('badge_rewards');
    }
}
