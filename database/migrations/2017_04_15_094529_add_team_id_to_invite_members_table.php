<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTeamIdToInviteMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invite_members', function (Blueprint $table) {
            $table->unsignedInteger('organization_id')->nullable()->change();
            $table->unsignedInteger('team_id')->nullable()->after('organization_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invite_members', function (Blueprint $table) {
            $table->dropColumn(['team_id']);
        });
    }
}
