<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBadgeRewards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        \App\Model\BadgeReward::query()->truncate();

        Schema::table('badge_rewards', function (Blueprint $table) {
            $table->text('description')->nullable();
            $table->string('icon')->nullable();
            $table->unsignedInteger('organization_id')->nullable();
            $table->foreign('organization_id')->references('id')->on('organizations');
            $table->timestamps();
        });

        $this->addData();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

    public function addData()
    {
        $defaultItems = [
            [
                'id' => 1,
                'title' => '20% Badge Reward',
                'description' => '20% Badge Reward',
                'badges' => 20,
                'points' => 10,
            ],
            [
                'id' => 2,
                'title' => '50% Badge Reward',
                'description' => '50% Badge Reward',
                'badges' => 50,
                'points' => 20,
            ],
            [
                'id' => 3,
                'title' => '80% Badge Reward',
                'description' => '80% Badge Reward',
                'badges' => 80,
                'points' => 40,
            ],
            [
                'id' => 4,
                'title' => '100% Badge Reward',
                'description' => '100% Badge Reward',
                'badges' => 100,
                'points' => 50,
            ],
        ];
        foreach ($defaultItems as $data) {
            $model = new \App\Model\BadgeReward();
            $model->fill($data);
            $model->id = $data['id'];
            $model->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('badge_rewards', function (Blueprint $table) {
            $table->dropForeign(['organization_id']);
            $table->dropColumn(['organization_id']);
            $table->dropColumn(['description']);
            $table->dropColumn(['icon']);
            $table->dropTimestamps();
        });
    }
}
