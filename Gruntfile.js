module.exports = function(grunt) {
  // load all grunt-* tasks automatically
  // so you don't need to provision them manually inside
  // gruntfile
  require('time-grunt')(grunt);
  //require('load-grunt-tasks')(grunt);
  grunt.loadNpmTasks('grunt-banner');
  grunt.loadNpmTasks('grunt-remove-logging');
  require('jit-grunt')(grunt);

  // setup configuration
  grunt.initConfig({

    // global variables to avoid hardcoding paths in Grunt
    // tasks and commands
    globalConfig: {
      assets: {
        // our custom JS code
        js: ['resources/assets/js/*.js'],
        // our custom JS code per page
        pagesjs: ['resources/assets/js/pages'],
        // our custom CSS code
        css: ['resources/assets/css/*.css'],
        // our custom CSS code per page
        pagescss: ['resources/assets/css/pages'],
        // our custom scss per page
        pagesscss: ['resources/assets/sass/pages'],
        // client's SASS code
        sass: ['resources/assets/sass/**/*.scss'],
        // our custom Pug code
        pug: ['resources/assets/pug/*.pug'],
        // our custom HTML code
        html: ['templates/**/*.html'],
        // our handlerbars templates
        hbs: ['templates/handlebars/**/*.handlebars'],
        // our images
        img: ['resources/assets/img/**/*.{png,jpg,gif}'],
        // our fonts
        font: ['resources/assets/fonts/**/*.{woff,woff2,ttf}'],
        // where to put assembled (concatentated) files, as well as
        // their minimized versions
        dist: 'public/assets',
        bower_components: 'bower_components',
        local: 'resources/assets'
      }
    },
    // task to generate html pages from markdown files
    // we will use it as internal 'changelog/forum' implementation
    // where we share specific details about the implementation
    // (developers to developers)
    pages: {
      posts: {
        // where our markdown files are located
        src: 'docs/dev/posts',
        // where to put html files created from markdown files
        dest: 'public/.dev',
        // template layout for each post
        layout: 'docs/dev/layouts/post.jade',
        // url schema
        url: 'pydoc/:title/'
      }
    },
    copy: {
      main: {
        files: []
      }
    },
    // task to generate icon fonts from SVG
    // we will use fontforge, read https://github.com/sapegin/grunt-webfont
    webfont: {
      icons: {
        src: '<%= globalConfig.assets.local %>/fonts/svg/*.svg',
        dest: '<%= globalConfig.assets.dist %>/fonts',
        destCss: '<%= globalConfig.assets.local %>/sass/base',
        options: {
          engine: 'node',
          stylesheet: 'scss',
          relativeFontPath: '<%= globalConfig.assets.dist %>/fonts',
          autoHint: false,
          types: 'woff, woff2, ttf',
          htmlDemo: false,
          templateOptions: {
            baseClass: 'icon',
            classPrefix: 'icon-',
            mixinPrefix: 'icon-'
          }
        }
      }
    },
    // task to check CSS files syntax against
    // best practices
    csslint: {
      // where to take configuration for csslint
      options: {
        csslintrc: '.csslintrc'
      },
      strict: {
        options: {
          import: 2
        },
        // what files to check
        // we specifically exclude external vendor files that aren't part of
        // bower, as well as assets/css/client.css which is compiled
        // SASS of the client
        // and we do not check for validity client's markup
        src: [
          '<%= globalConfig.assets.css %>',
          '<%= globalConfig.assets.pagescss %>/**/*.css'
        ]
      }
    },
    // additional js checks to the source code we create that
    // are not available in jshint
    jscs: {
      options: {
        // where to take configuration for csslint
        config: '.jscsrc',
        // Autofix code style violations when possible.
        // If you set to true, then git watch will run
        // in multiple cycles on change.
        fix: false,
        requireCurlyBraces: ['if']
      },
      // what files to check
      src: [
        '<%= globalConfig.assets.js %>',
        '<%= globalConfig.assets.pagesjs %>/**/*.js'
      ]
    },
    eslint: {
      options: {
        configFile: '.eslintrc.js'
      },
      target: [
        '<%= globalConfig.assets.js %>',
        '<%= globalConfig.assets.pagesjs %>/**/*.js'
      ]
    },
    // sass pre-css parser that converts sass into
    // files and code
    sass: {
      dev: {
        options: {
          style: 'expanded',
          quiet: true // stop depreciation errors
        },
        files: {}
      }
    },
    // minify images
    imagemin: {
      dynamic: {
        files: [
          {
            expand: true,
            cwd: '<%= globalConfig.assets.local %>',
            src: ['**/*.{png,jpg,gif}'],
            dest: '<%= globalConfig.assets.dist %>'
          }
        ]
      }
    },
    // task to watch changes in real time to specified files and run tasks
    // if we get bower.json updated and install the change immediately
    // also run jshint, jscs checker on changed JS files
    watch: {
      // check if bower is updated and run build task
      bower: {
        // what files to check
        files: ['bower.json'],
        tasks: ['exec:bower_install', 'build']
      },
      // check if JS files got updated and run linters along
      // with js concatenation. Live reload enabled
      scripts: {
        // what files to check
        files: '<%= globalConfig.assets.js %>',
        tasks: ['concat:js', 'jscs', 'eslint', 'usebanner:app'],
        options: {
          livereload: true
        }
      },
      // check if JS files got updated and run linters along
      // with js concatenation. Live reload enabled
      pages: {
        // what files to check
        files: ['<%= globalConfig.assets.pagesjs %>/**/*.js', '<%= globalConfig.assets.pagescss %>/**/*.css'],
        tasks: ['pages_concat_prepare', 'concat', 'jscs', 'eslint', 'usebanner:app'],
        options: {
          livereload: true
        }
      },
      // check if css files got updated and run linters along
      // with css concatentation. Live reload enabled
      css: {
        // what files to check
        files: ['<%= globalConfig.assets.css %>'],
        tasks: ['concat:css', 'csslint', 'usebanner:app'],
        options: {
          livereload: true
        }
      },
      // check if sass files got updated and run sass compiler
      sass: {
        // what files to check
        files: ['<%= globalConfig.assets.sass %>'],
        tasks: ['sass_prepare', 'sass', 'concat:css', 'usebanner:client'],
        options: {
          livereload: true
        }
      },
      // check if html files got updated and run live reload
      html: {
        // what files to check
        files: '<%= globalConfig.assets.html %>',
        options: {
          livereload: true
        }
      },
      hbs: {
        // what files to check
        files: '<%= globalConfig.assets.hbs %>',
        tasks: ['shell:handlebars'],
        options: {
          livereload: true
        }
      },
      img: {
        files: '<%= globalConfig.assets.img %>',
        tasks: ['imagemin']
      },
      pug: {
        files: '<%= globalConfig.assets.local %>/pug/**/*.pug',
        tasks: ['pug_prepare', 'pug']
      }
    },
    // place banner text in compiled files to avoid editing
    usebanner: {
      bower: {
        options: {
          position: 'top',
          banner: '/* !!! SARITASA Team: \nAVOID editing this file, \n' +
            'as we produced it using concatenation of bower dependencies (components) \n' +
            'located in assets/components/. \n' +
            '*/\n',
          linebreak: true
        },
        files: {
          src: ['<%= globalConfig.assets.dist %>/css/bower.css', '<%= globalConfig.assets.dist %>/js/bower.js']
        }
      },
      client: {
        options: {
          position: 'top',
          banner: '/* !!! SARITASA Team: \nAVOID editing this file, \n' +
            'as we produced it using SASS compiler. \n' +
            '*/',
          linebreak: true
        },
        files: {
          src: ['<%= globalConfig.assets.dist %>/css/styles.css']
        }
      },
      app: {
        options: {
          position: 'top',
          banner: '/* !!! SARITASA Team: \nAVOID editing this file, \n' +
            'as we produced it using concatenation of assets css and js files. \n' +
            '*/\n',
          linebreak: true
        },
        files: {
          src: ['<%= globalConfig.assets.dist %>/css/app.css', '<%= globalConfig.assets.dist %>/js/app.js']
        }
      }
    },
    // custom command available inside watch config above
    exec: {
      bower_install: {
        cmd: 'bower install'
      }
    },
    // task to concantenate all JS and CSS of all installed Bower components and
    // generate unminified bower.js and bower.css files
    bower_concat: {
      all: {
        // where to put assembled files
        dest: {
          'js': '<%= globalConfig.assets.dist %>/js/bower.js',
          'css': '<%= globalConfig.assets.dist %>/css/bower.css'
        },
        // define dependencies here, so the concatenation happens
        // in correct order
        dependencies: {
          'bootstrap': 'jquery'
        },
        // in some cases bower_concat gets bower packages that do
        // not properly define their distr folders/files
        // we can fix it by doing it here
        mainFiles: {
          'bootstrap': [
            'dist/css/bootstrap.css',
            'dist/js/bootstrap.js'
          ]
        }
      }
    },
    // task concatenate our own css and js files
    // so we can use app.js and app.css in debug mode
    concat: {
      js: {
        src: '<%= globalConfig.assets.js %>',
        dest: '<%= globalConfig.assets.dist %>/js/app.js'
      },
      css: {
        src: '<%= globalConfig.assets.css %>',
        dest: '<%= globalConfig.assets.dist %>/css/app.css'
      }
    },
    // task minimize bower.js and produce bower.min.js file
    // as well ass app.min.js from our custom JS files
    uglify: {
      // bower.min.js minification
      bower: {
        options: {
          mangle: true,
          compress: true
        },
        files: {
          '<%= globalConfig.assets.dist %>/js/bower.min.js': ['<%= globalConfig.assets.dist %>/js/bower.js']
        }
      },
      // app.min.js minification
      app: {
        options: {
          mangle: true,
          compress: true
        },
        files: {
          '<%= globalConfig.assets.dist %>/js/app.min.js': ['<%= globalConfig.assets.dist %>/js/app.js']
        }
      }
    },
    // task to minimize bower.css and produce bower.min.css file
    // as well ass app.min.css from our custom CSS files
    cssmin: {
      options: {
        shorthandCompacting: false,
        roundingPrecision: -1
      },
      // bower.min.css minification
      bower: {
        files: {
          '<%= globalConfig.assets.dist %>/css/bower.min.css': ['<%= globalConfig.assets.dist %>/css/bower.css']
        }
      },
      // app.min.css minification
      app: {
        files: {
          '<%= globalConfig.assets.dist %>/css/app.min.css': ['<%= globalConfig.assets.dist %>/css/app.css']
        }
      },
      // styles.min.css minification
      styles: {
        files: {
          '<%= globalConfig.assets.dist %>/css/styles.min.css': ['<%= globalConfig.assets.dist %>/css/styles.css']
        }
      }
    },
    jsbeautifier: {
      files: ['<%= globalConfig.assets.js %>',
        '<%= globalConfig.assets.pagesjs %>/**/*.js'
      ],
      options: {
        config: '.jsbeautifyrc'
      }
    },
    //remove console.log()
    removelogging: {
      dist: {
        src: '<%= globalConfig.assets.dist %>/js/**/*.js'
      }
    },
    // https://github.com/nDmitry/grunt-postcss
    // http://webdesign.tutsplus.com/tutorials/postcss-quickstart-guide-grunt-setup--cms-24545
    // allows to run post-processing tasks after our custom
    // css files got concatenated
    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer')({
            browsers: ['last 2 version', 'ie 10', 'ie 11']
          }),
          require('cssnext')(),
          require('precss')()
        ]
      },
      dist: {
        src: '<%= globalConfig.assets.dist %>/css/app.css',
        dest: '<%= globalConfig.assets.dist %>/css/app.css'
      }
    },
    // send notifications when task complete
    notify: {
      build: {
        options: {
          message: 'Build is completed successfully!'
        }
      }
    },
    // this should be used as isolated command
    // it will compile app.min.css, app.min.js
    // bower.min.css, bower.min.js under new revisions
    // insisde assets/revs folder which could be useful for
    // deplpyment in CDN scenario when files are cached by CDN
    // https://www.npmjs.com/package/grunt-filerev
    // we can also use this for images and other binary files
    filerev: {
      options: {
        algorithm: 'md5',
        length: 8
      },
      appfiles: {
        src: ['assets/*.min.css', 'assets/*.min.js'],
        dest: 'assets/revisions'
      }
    },
    // generate javascript documentation
    jsdoc: {
      dist: {
        src: ['assets/js/**/*.js', 'templates/users/*.html', 'README.md'],
        options: {
          destination: '.dev/jsdoc',
          template: 'node_modules/ink-docstrap/template',
          configure: '.jsdoc.conf.json'
        }
      }
    },
    // apidoc generation
    apidoc: {
      apps: {
        src: 'app/',
        dest: 'public/.dev/apidoc/'
      }
    }, // these are additional shell commands available inside grunt tasks
    shell: {
      // shell command to install bower package
      bowerinstall: {
        command: function(libname) {
          return 'bower install ' + libname + ' -S';
        }
      },
      // shell command to uninstall bower package
      boweruninstall: {
        command: function(libname) {
          return 'bower uninstall ' + libname + ' -S';
        }
      },
      // shell command to update bower package
      bowerupdate: {
        command: function(libname) {
          return 'bower update ' + libname;
        }
      },
      // command to copy git hooks from repo inside your own
      // .git folder
      hooks: {
        command: 'cp .git-hooks/pre-commit .git/hooks/'
      },
      // shell command to minify and pre-compile handlebars templates
      handlebars: {
        command: 'handlebars templates/handlebars ' +
          ' -m -f assets/handlebars/templates.js'
      },
      // generate tags
      ctags: {
        command: 'ctags -e -R apps/ libs/ assets/js/* templates TAGS'
      },
      // beautify just a single file
      beautify: {
        command: function(file) {
          return 'js-beautify --config .jsbeautifyrc -r -f ' + file;
        }
      }
    },
    //create md5 checksum file
    md5sum: {
      build: {
        files: [
          {
            cwd : '<%= globalConfig.assets.dist %>/',
            src : ['**/*.{js,css}'],
            dest: '<%= globalConfig.assets.dist %>/sum.md5'
          }
        ]
      }
    },

    pug: {
      compile: {
        options: {
          data: {
            debug: false
          },
          basedir: '<%= globalConfig.assets.local %>/pug/'
        },
        files: {}
      }
    },

    // clean any pre-commit hooks in .git/hooks directory
    clean: {
      hooks: ['.git/hooks/pre-commit']
    }
  });

  // task for prepare configuration for concat static js and css files separated by pages
  grunt.registerTask("pages_concat_prepare", "Finds and prepares files for concatenation.", function() {
    // get the current concat object from initConfig
    var concat = grunt.config.get('concat') || {};
    var uglify = grunt.config.get('uglify') || {};
    var cssmin = grunt.config.get('cssmin') || {};

    var directories = [
      {
        'dir': grunt.config.get('globalConfig').assets.pagesjs + '/*',
        'type': 'js'
      },
      {
        'dir': grunt.config.get('globalConfig').assets.pagescss + '/*',
        'type': 'css'
      }
    ];

    directories.forEach(function(directory){

      // get all page directories
      grunt.file.expand(directory.dir).forEach(function (dir) {

        // get the module name from the directory name
        var dirName = dir.substr(dir.lastIndexOf('/')+1);


        // create a subtask for each page, find all src files
        // and combine into a single js file per module

        concat[dirName + directory.type] = {
          src: [dir + '/**/*.' + directory.type],
          dest: grunt.config.get('globalConfig').assets.dist + '/' + directory.type + '/' + dirName + '.' + directory.type
        };
        grunt.config.set('concat', concat);

        if (directory.type == 'js') {

          uglify[dirName + directory.type] = {
            options: {
              mangle: true,
              compress: true
            },
            files: {}
          };
          uglify[dirName + directory.type].files[grunt.config.get('globalConfig').assets.dist + '/' + directory.type + '/' + dirName + '.min.js'] = [grunt.config.get('globalConfig').assets.dist + '/' + directory.type + '/' + dirName + '.js'];
          // add module subtasks to the concat task in initConfig
          grunt.config.set('uglify', uglify);

        } else if (directory.type == 'css') {

          cssmin[dirName + directory.type] = {
            files: {}
          };
          cssmin[dirName + directory.type].files[grunt.config.get('globalConfig').assets.dist + '/' + directory.type + '/' + dirName + '.min.css'] = [grunt.config.get('globalConfig').assets.dist + '/' + directory.type + '/' + dirName + '.css'];
          grunt.config.set('cssmin', cssmin);

        }

      });
    });
  });

  // task for prepare configuration for sass separated by pages
  grunt.registerTask("sass_prepare", "Finds and prepares scss files for compilation.", function() {
    // get the current concat object from initConfig
    var sass = grunt.config.get('sass') || {};
    var cssmin = grunt.config.get('cssmin') || {};

    var findFile = function (dir) {
      var filename = null;
      grunt.file.expand(dir + '/*.scss').forEach(function (file) {
        var name = file.substr(file.lastIndexOf('/')+1);
        if (name.substr(0,1) !== '_') {
          filename = file;
        }
      });
      return filename;
    }

    // get all page directories
    grunt.file.expand(grunt.config.get('globalConfig').assets.pagesscss + '/*').forEach(function (dir) {
      var dirName = dir.substr(dir.lastIndexOf('/')+1);
      var filename = findFile(dir);
      if (filename) {
        sass.dev.files[grunt.config.get('globalConfig').assets.dist + '/css/' + dirName + '.css'] = filename;
        cssmin[dirName + 'css'] = {
          files: {}
        };
        cssmin[dirName + 'css'].files[grunt.config.get('globalConfig').assets.dist + '/css/' + dirName + '.min.css'] = [grunt.config.get('globalConfig').assets.dist + '/css/' + dirName + '.css'];
      }
    });

    var filename = findFile(grunt.config.get('globalConfig').assets.local + '/sass');
    if (filename) {
      sass.dev.files[grunt.config.get('globalConfig').assets.dist + '/css/styles.css'] = filename;
    }
    grunt.config.set('sass', sass);
    grunt.config.set('cssmin', cssmin);
  });

  // task for prepare configuration for pug separated by pages
  grunt.registerTask("pug_prepare", "Finds and prepares scss files for compilation.", function() {
    // get the current concat object from initConfig
    var pug = grunt.config.get('pug') || {};
    grunt.file.expand(grunt.config.get('globalConfig').assets.pug).forEach(function (file) {
      var name = file.substr(file.lastIndexOf('/')+1);
      if (name.substr(0,1) === '_') {
        return;
      }
      pug.compile.files[grunt.config.get('globalConfig').assets.local + '/html/' + name.substr(0, name.lastIndexOf('.')) + '.html'] = file;
    });

    grunt.config.set('pug', pug);
  });

  //task for prepare copy configuration. Used for copy fonts, images from bower_components to public directory
  grunt.registerTask("copy_prepare", "Finds and prepares files for copy.", function() {
    var extensions = '{ttf,woff,woff2,svg,otf,png,gif,jpg}';
    var files = [
      grunt.config.get('globalConfig').assets.bower_components + '/**/*.' + extensions,
      grunt.config.get('globalConfig').assets.font
    ];
    var copy = grunt.config.get('copy') || {main: {files: []}};

    pathes = [];
    files.forEach(function(search){
      grunt.file.expand(search).forEach(function (file) {
        var path = file.substring(0, file.lastIndexOf("/"));
        if (pathes.indexOf(path) == -1) {
          pathes.push(path);
        }
      });
    });

    pathes.forEach(function(path){
      var dirname = path.substring(path.lastIndexOf("/") + 1, path.length);
      copy.main.files.push({expand: true, flatten: true, src: [path + '/*.' + extensions], dest: grunt.config.get('globalConfig').assets.dist + '/' + dirname + '/'});
    });
    grunt.config.set('copy', copy);
  });

  // default task to run when you run grunt from CLI
  // it will run linters against JS code
  grunt.registerTask('default', ['jscs', 'eslint', 'csslint']);

  // beautify just a single file
  grunt.registerTask('beautify', function(file) {
    grunt.
    task.
    run('shell:beautify:' + file);
  });

  // aggregated build bower assets into single file
  // and then minify it
  grunt.registerTask('build', [
    'bower_concat',
    'usebanner:bower',
    'webfont',
    'sass_prepare',
    'sass',
    'usebanner:client',
    'pages_concat_prepare',
    'concat',
    'usebanner:app',
    'postcss',
    'uglify',
    'cssmin',
    'copy_prepare',
    'copy',
    'imagemin',
    'md5sum',
    //'gendocs',
    //'shell:ctags',
    //'shell:handlebars',
    'removelogging',
    'notify:build'
  ]);

  // Build assets into single file, without long bower tasks
  grunt.registerTask('build-local', [
    'webfont',
    'sass_prepare',
    'sass',
    'usebanner:client',
    'pages_concat_prepare',
    'concat',
    'usebanner:app',
    'postcss',
    'copy_prepare',
    'copy',
    'imagemin',
    'md5sum',
    'removelogging',
    'notify:build'
  ]);

  // aggregated build bower assets into single file on local, without long bower tasks
  grunt.registerTask('pug-compile', [
    'pug_prepare',
    'pug'
  ]);


  // if you want to install any bower dep and quickly rebuild the bower.css/js
  // and produce their min versions simply type
  // grunt bowerinstall <libname>
  // like
  // grunt bowerinstall stringjs
  // you will get this output:
  // https://jingus.saritasa.com/DmitrySemenov/index.php?name=screenshot-02-22-16-16-51-27.png
  grunt.registerTask('bowerinstall', function(library) {
    grunt.
    task.
    run('shell:bowerinstall:' + library);

    grunt.
    task.
    run('build');
  });
  // same purpose as above but to uninstall the package
  grunt.registerTask('boweruninstall', function(library) {
    grunt.
    task.
    run('shell:boweruninstall:' + library);

    grunt.
    task.
    run('build');
  });
  // same purpose as above but to update the package
  grunt.registerTask('bowerupdate', function(library) {
    grunt.
    task.
    run('shell:bowerupdate:' + library);

    grunt.
    task.
    run('build');
  });

  // generate docs
  grunt.registerTask('gendocs', function() {
    grunt.task.run('pages');

    grunt.
    task.
    run('jsdoc');

    grunt.
    task.
    run('apidoc');
  });

  // clean the .git/hooks/pre-commit file then copy in the latest version
  grunt.registerTask('hookmeup', [
    'clean:hooks',
    'shell:hooks'
  ]);
};
