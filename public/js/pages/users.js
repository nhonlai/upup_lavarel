'use strict';

(function ($) {

  var userTemplate;
  var userList;
  var deleteModal;
  var totalPages, currentPage, limit = 20, totalItems = 0;
  var loading, searchText;


  function renderUser(user) {
    var userRow = $(Mustache.render(userTemplate, user));
    userList.append(userRow);
    userRow.click(function (e) {
      location.pathname = 'admin/users/' + user.id;
      e.preventDefault();
      return false;
    });
    userRow.find('a.delete').click(function (e) {
      e.preventDefault();
      deleteModal.find('.modal-body .info').html(
        '<p>' + user.first_name + ' ' + user.last_name + '</p>' +
        '<p>' + user.company + '</p>' +
        '<p>' + user.phone + '</p>'
      );
      deleteModal.find('#btnDelete').data('id', user.id);
      deleteModal.modal('show');
      return false;
    });
  }

  function deleteUser(id, successCallback, errorCallback) {
    $.ajax({
      method: 'DELETE',
      url: '/admin/users/' + id,
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      success: function (res) {
        if (res.status == 'success') {
          if ('function' == typeof successCallback) {
            successCallback(res);
          }
        }
      },
      error: function (jqXHR) {
        if (jqXHR.responseJSON && jqXHR.responseJSON.status == 'error') {
          var message = '';
          if (jqXHR.responseJSON.errors) {
            jqXHR.responseJSON.errors.forEach(function(error) {
              message = message + "\n" + error.value.join("\n");
            });
          } else {
            message = jqXHR.responseJSON.message;
          }
          alert(message);
          if ('function' == typeof errorCallback) {
            errorCallback(jqXHR);
          }
        }
      }
    });
  }

  function refreshUserList(page) {
    if (page == undefined) page = 1;
    if (page == 1) {
      userList.html('');
    }
    loading.show();
    $.get('/admin/users?page=' + page + '&limit=' + limit + '&search=' + searchText.val(), function (res) {
      /**
       * There is an issue about response for meta requests on production:
       * - without "meta" parameter (content of meta.pagination is parsed directly to response body).
       * - without "status" parameter.
       */
      if (res.status == 'success' || res.total !== undefined) {
        if (res.total !== undefined) {
          totalPages = res.last_page;
          currentPage = res.current_page;
          totalItems = res.total;
        } else {
          totalPages = res.meta.pagination.total_pages;
          currentPage = res.meta.pagination.current_page;
          totalItems = res.meta.pagination.total;
        }
        $('#totalUser').text(totalItems);
        res.data.forEach(renderUser);
        $(".table-body-wrapper").mCustomScrollbar('update');
        loading.hide();
        if (res.meta.pagination.total == 0) {
          userList.append('<tr class="text-center"><td>No result</td></tr>');
        }
      }
    });
  }

  $(function () {
    loading = $("#loading");
    searchText = $('#searchText');
    searchText.on('keypress', function (e) {
      if (e.keyCode == 13) {
        refreshUserList(1);
      }
    });
  });

  $(window).on("load", function () {
    deleteModal = $('#deleteUserModal');
    userTemplate = $('#user-template').html();
    userList = $('#users tbody');
    Mustache.parse(userTemplate);

    $(".table-body-wrapper").mCustomScrollbar({
      scrollbarPosition: "inside",
      theme: 'minimal-dark',
      callbacks: {
        alwaysTriggerOffsets: true,
        onTotalScrollOffset: 300,
        onTotalScroll: function () {
          console.log('onTotalScroll');
          if (totalPages > currentPage) {
            console.log('load more ...');
            refreshUserList(currentPage + 1);
          }
        }
      },
      advanced: {
        autoUpdateTimeout: 20
      }
    });

    deleteModal.find('#btnDelete').click(function (e) {
      var self = $(this);
      e.preventDefault();
      self.prop('disabled', true);
      deleteUser($(this).data('id'), function () {
        refreshUserList();
        self.prop('disabled', false);
      }, function() {
        self.prop('disabled', false);
      });
      deleteModal.modal('hide');
      return false;
    });

    refreshUserList();
  });
})(jQuery);
