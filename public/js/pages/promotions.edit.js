'use strict';

(function ($) {
  var defaultImage = "url('/images/img_default.jpg')";

  $(function () {
    var previewImage = $('#previewImage');
    var removeImage = $('#removeImage');
    var replaceImage = $('#replaceImage');
    var removeImageOption = $('#removeImageOption');

    $('#save-promotion, #delete-promotion').click(function () {
      if ($(this).closest('form').get(0).checkValidity()) {
        $(this).prop('disabled', true);
        $(this).closest('form').submit();
      }
    });

    $('.customSelect').select2({
      placeholder: "Select",
      minimumResultsForSearch: -1
    });

    $('#datetimepicker1').datetimepicker({
      format: 'MM/DD/YYYY'
    });
    replaceImage.on('change', function () {
      console.log(this.files[0]);
      var reader = new FileReader();
      reader.onload = function (e) {
        console.log(e.target.result);
        previewImage.css('background-image', 'url(' + e.target.result + ')');
        removeImageOption.val(0);
      };
      reader.readAsDataURL(this.files[0]);
    });
    removeImage.click(function () {
      previewImage.css('background-image', defaultImage);
      removeImageOption.val(1);
      replaceImage.val('');
    });
  });

})(jQuery);
