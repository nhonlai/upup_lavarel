function Paginator(config) {
  var defaultConfig = {
    url: null,
    limit: 20,
    success: function (res) {
    },
    noResult: function (res) {
    },
    error: function (res) {
      console.log(res);
    },
    fail: function (jqXHR) {
      if (jqXHR.responseJSON && jqXHR.responseJSON.status == 'error') {
        alert(jqXHR.responseJSON.message);
      }
    },
    showLoading: function () {
      $('#loading').show();
    },
    hideLoading: function () {
      $('#loading').hide();
    }
  };

  config = $.extend(defaultConfig, config);
  if (!config.url) throw new Error('Paginator: url must be set');

  console.log(config);

  var _totalPage = 1,
    _currentPage = 0,
    _totalItem = 0;

  function initialize() {
    _totalPage = 1;
    _currentPage = 0;
  }

  this.isEnd = function () {
    return _totalPage <= _currentPage;
  };

  this.firstSearch = function (search) {
    if (search == undefined) search = '';
    initialize();
    this.nextPage(search);
  };

  this.nextPage = function (search) {
    if (search == undefined) search = '';
    if (this.isEnd()) return;

    var data = {
      limit: config.limit,
      page: _currentPage + 1,
      search: search
    };
    config.showLoading();
    $.get(config.url, data, function (res) {
      /**
       * There is an issue about response for meta requests on production:
       * - without "meta" parameter (content of meta.pagination is parsed directly to response body).
       * - without "status" parameter.
       * For example:
       * - On dev: {"status":"success","data":[{"id":11,"name":"test 3","branch_number":"","location_type_id":1,"order_email":"jonathon@saritasa.com","quote_email":"jonathon@saritasa.com","question_email":"jonathon@saritasa.com","phone":"","address":"120 Pitt Street","address2":"","city":"Sydney","state":"NSW","zip":"2000","country":"Australia","lat":-33.8679944,"lng":151.2088341,"image":null,"created_at":"2016-11-22 04:23:05","updated_at":"2016-11-22 04:23:05"},{"id":10,"name":"test 3","branch_number":"","location_type_id":1,"order_email":"jonathon@saritasa.com","quote_email":"jonathon@saritasa.com","question_email":"jonathon@saritasa.com","phone":"","address":"45 Pacific Highway","address2":"","city":"Ourimbah","state":"NSW","zip":"2258","country":"Australia","lat":-33.36101,"lng":151.3697125,"image":null,"created_at":"2016-11-22 04:22:39","updated_at":"2016-11-22 04:22:39"},{"id":9,"name":"test 2","branch_number":"","location_type_id":1,"order_email":"jonathon@saritasa.com","quote_email":"jonathon@saritasa.com","question_email":"jonathon@saritasa.com","phone":"","address":"73 Darlinghurst Road","address2":"","city":"Potts Point","state":"NSW","zip":"2011","country":"Australia","lat":-33.8739227,"lng":151.2232338,"image":null,"created_at":"2016-11-22 04:22:11","updated_at":"2016-11-22 04:22:11"},{"id":8,"name":"Teset1","branch_number":"","location_type_id":1,"order_email":"jonathon@saritasa.com","quote_email":"jonathon@saritasa.com","question_email":"jonathon@saritasa.com","phone":"","address":"18 Enmore Road","address2":"","city":"Newtown","state":"NSW","zip":"2042","country":"Australia","lat":-33.8982565,"lng":151.1774918,"image":null,"created_at":"2016-11-22 04:21:41","updated_at":"2016-11-22 04:21:41"},{"id":7,"name":"Starbucks, George Street, Sydney, New South Wales, Australia","branch_number":"","location_type_id":1,"order_email":"","quote_email":"","question_email":"","phone":"","address":"2\/525 George St, Sydney NSW 2000, Australia","address2":"","city":"Council of the City of Sydney","state":"NSW","zip":"2000","country":"Australia","lat":-33.8756993,"lng":151.2060448,"image":null,"created_at":"2016-10-05 10:13:32","updated_at":"2016-10-05 10:13:32"},{"id":6,"name":"1 Wall St, New York, NY 10005, USA","branch_number":"23123","location_type_id":1,"order_email":"","quote_email":"","question_email":"","phone":"","address":"1 Wall St, New York, NY 10005, USA","address2":"1 Wall St, New York, NY 10005, USA","city":"New York County","state":"NY","zip":"10005","country":"United States","lat":40.707491,"lng":-74.0116385,"image":null,"created_at":"2016-10-05 09:27:24","updated_at":"2016-10-05 09:28:26"},{"id":5,"name":"New position 123456788910111","branch_number":"34234","location_type_id":1,"order_email":"jonathon@saritasa.com","quote_email":"jonathon@saritasa.com","question_email":"jonathon@saritasa.com","phone":"","address":"Ashfield, New South Wales, Australia","address2":"fasdf","city":"New South Wales","state":"","zip":"","country":"","lat":null,"lng":null,"image":null,"created_at":"2016-10-05 09:20:07","updated_at":"2016-10-26 05:04:24"},{"id":4,"name":"Saritasa Vietnam","branch_number":"1232","location_type_id":1,"order_email":"","quote_email":"","question_email":"","phone":"","address":"58 Nguy\u1ec5n \u0110\u00ecnh Chi\u1ec3u, \u0110a Kao, Qu\u1eadn 1, H\u1ed3 Ch\u00ed Minh, Vietnam","address2":"test","city":"H\u1ed3 Ch\u00ed Minh","state":"","zip":"","country":"Vietnam","lat":10.78915,"lng":106.699949,"image":null,"created_at":"2016-10-05 09:00:44","updated_at":"2016-10-05 09:00:44"},{"id":1,"name":"San Diego Branch","branch_number":"1","location_type_id":2,"order_email":"thuan.chau@saritasa.com","quote_email":"thuan.chau@saritasa.com","question_email":"thuan.chau@saritasa.com","phone":null,"address":"San Diego, CA","address2":null,"city":"San Diego","state":"CA","zip":null,"country":"United States","lat":null,"lng":null,"image":null,"created_at":"2016-09-14 04:48:37","updated_at":"2016-09-14 04:49:23"},{"id":2,"name":"Ho Chi Minh Branch","branch_number":"2","location_type_id":1,"order_email":"","quote_email":"","question_email":"","phone":"","address":"Ho Chi Minh, Vietnam","address2":"111","city":"H\u1ed3 Ch\u00ed Minh","state":"","zip":"","country":"","lat":0,"lng":0,"image":null,"created_at":"2016-09-14 04:48:37","updated_at":"2016-10-05 09:11:39"},{"id":3,"name":"Los Angeles Branch","branch_number":"3","location_type_id":1,"order_email":null,"quote_email":null,"question_email":null,"phone":null,"address":"Los Angeles, CA","address2":null,"city":"Los Angeles","state":"CA","zip":null,"country":"United States","lat":null,"lng":null,"image":null,"created_at":"2016-09-14 04:48:37","updated_at":"2016-09-14 04:49:23"}],"meta":{"pagination":{"total":11,"count":11,"per_page":20,"current_page":1,"total_pages":1,"links":{"previous":null,"next":null}}}}
       * - On production: {"total":1,"per_page":20,"current_page":1,"last_page":1,"next_page_url":null,"prev_page_url":null,"from":1,"to":1,"data":[{"id":1,"name":"Saritasa US","branch_number":"123456","location_type_id":1,"order_email":"thuan.chau@saritasa.com","quote_email":"thuan.chau@saritasa.com","question_email":"thuan.chau@saritasa.com","phone":"","address":"20411 Southwest Birch Street","address2":"Suite 330","city":"Newport Beach","state":"CA","zip":"92660","country":"United States","lat":33.657023,"lng":-117.879661,"image":"https:\/\/fbm-prod.s3.amazonaws.com\/prod\/locations\/2ff937e56905cf560620ecf9f443f0dd.jpeg","created_at":"2016-11-17 10:58:26","updated_at":"2016-11-17 10:58:26"}]}
       */
      if (res.status == 'success' || res.total !== undefined) {
        if (res.total !== undefined) {
          _totalPage = res.last_page;
          _currentPage = res.current_page;
          _totalItem = res.total;
        } else {
          _totalPage = res.meta.pagination.current_page;
          _currentPage = res.meta.pagination.current_page;
          _totalItem = res.meta.pagination.total;
        }
        if (_totalItem == 0) {
          config.noResult(res);
        } else {
          config.success(res);
        }
      } else {
        config.error(res);
      }
    }).fail(config.fail).always(function () {
      config.hideLoading();
    });
  };

  this.totalItem = function () {
    return _totalItem;
  };
  this.currentPage = function () {
    return _currentPage;
  };
}
