'use strict';

(function ($) {
  var defaultImage = "url('/images/img_default.jpg')";

  $(document).ready(function () {
    var previewImage = $('#previewImage');
    var removeImage = $('#removeImage');
    var replaceImage = $('#replaceImage');
    var removeImageOption = $('#removeImageOption');

    $('.customSelect').select2({
      placeholder: "Select",
      minimumResultsForSearch: -1
    });

    replaceImage.on('change', function () {
      console.log(this.files[0]);
      var reader = new FileReader();
      reader.onload = function (e) {
        //console.log(e.target.result);
        previewImage.css('background-image', 'url(' + e.target.result + ')');
        removeImageOption.val(0);
      };
      reader.readAsDataURL(this.files[0]);
    });

    removeImage.click(function () {
      previewImage.css('background-image', defaultImage);
      removeImageOption.val(1);
      replaceImage.val('');
    });

    $('#delete-location, #save-location').click(function() {
      $(this).prop('disabled', true);
      if ($(this).closest('form').get(0).checkValidity()) {
        $(this).closest('form').submit();
      } else {
        $(this).prop('disabled', false);
      }
    });

    //Prevent form submit by pressing Enter in INPUT
    $('form input').keypress(function(e){
      // Enter key = keycode 13
      if ( e.which == 13 ) {
        return false;
      }
    });
  })
})(jQuery);
