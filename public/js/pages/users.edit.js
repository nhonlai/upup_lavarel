'use strict';

(function ($) {

  $(function () {
    $('#btnSave, #delete-user').click(function () {
      if ($(this).closest('form').get(0).checkValidity()) {
        $(this).prop('disabled', true);
        $(this).closest('form').submit();
      }
    });
    $('input[type=tel]').inputmask({
      alias: 'phone',
      countrycode: 'US',
    });
  });

})(jQuery);
