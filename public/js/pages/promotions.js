'use strict';

(function ($) {
  var promotionList,
    promotionTpl,
    searchText;

  var paginator = new Paginator({
    url: '/admin/promotions',
    success: updatePromotionList,
    noResult: findZeroPromotion
  });

  $(function () {
    promotionList = $('#promotion-list tbody');
    promotionTpl = $('#promotion-tpl').html();
    searchText = $('#search-text');

    Mustache.parse(promotionTpl);

    paginator.firstSearch(searchText.val());

    searchText.on('keypress', function (e) {
      if (e.keyCode == 13) {
        paginator.firstSearch(searchText.val());
      }
    });
  });

  $(window).on("load", function () {
    $(".table-body-wrapper").mCustomScrollbar({
      scrollbarPosition: "inside",
      theme: 'minimal-dark',
      callbacks: {
        alwaysTriggerOffsets: true,
        onTotalScrollOffset: 300,
        onTotalScroll: function () {
          paginator.nextPage(searchText.val());
        }
      },
      advanced: {
        autoUpdateTimeout: 20
      }
    });
  });

  function updatePromotionList(res) {
    if (paginator.currentPage() == 1) {
      promotionList.html('');
    }
    res.data.forEach(renderPromotion);
    promotionList.parent().mCustomScrollbar('update');
    $('#totalPromotion').text(paginator.totalItem());
  }

  function renderPromotion(promotion) {
    var row = $(Mustache.render(promotionTpl, promotion));
    promotionList.append(row);
  }

  function findZeroPromotion() {
    promotionList.html('<tr class="text-center"><td>No result</td></tr>');
  }
})(jQuery);
