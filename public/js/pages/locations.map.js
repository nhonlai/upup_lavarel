function LocationMap(searchControl) {
    this.place = {};
    var defaultConfig = {
        center: {lat: -33.8688, lng: 151.2195},
        zoom: 13,
        searchControl: 'address'
    };

    config = $.extend(defaultConfig, {searchControl: searchControl});
    if (!config.center) throw new Error('LocationMap: center must be set');
    if (!config.searchControl) throw new Error('LocationMap: searchControl must be set');

    this.initialize = function() {
        var self = this;
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -33.8688, lng: 151.2195},
            zoom: 13
        });
        var input = /** @type {!HTMLInputElement} */(
            document.getElementById(config.searchControl));

        // do not add controls to the map
        //var types = document.getElementById('type-selector');
        //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        //map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                self.place = {};
                console.log("Autocomplete's returned place contains no geometry");
                return;
            }
            self.place = place;
            console.log(place);

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setIcon(/** @type {google.maps.Icon} */({
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(35, 35)
            }));
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
            infowindow.open(map, marker);

            if (self.isValid()) {
                $('#address').val(self.getAddress());
                $('#address2').val(self.getAddress2());
                $('#city').val(self.getCity());
                $('#state').val(self.getState());
                $('#zip').val(self.getZip());
                $('#country').val(self.getCountry());
                $('#lat').val(place.geometry.location.lat());
                $('#lng').val(place.geometry.location.lng());
                console.log(place.geometry.location.lat(), place.geometry.location.lng());
            } else {
                $('#address2').val('');
                $('#city').val('');
                $('#state').val('');
                $('#zip').val('');
                $('#country').val('');
                $('#lat').val('');
                $('#lng').val('');
            }
        });
    }

    /**
     * Check if a location is valid or not.
     * @returns boolean
     */
    this.isValid = function() {
        return this.place.geometry && this.getAddress() ? true : false;
    }

    /**
    * Get address of location (include street number and route).
    * @returns string
    */
    this.getAddress = function() {
        var streetNumber = this.findAddressComponent('street_number');
        var route = this.findAddressComponent('route');
        if (streetNumber && route) {
            return streetNumber.long_name + ' ' + route.long_name;
        } else {
            return '';
        }
    }

    /**
     * Get address2 of location (include suite number).
     * @returns string
     */
    this.getAddress2 = function() {
        var suite = this.findAddressComponent('subpremise');
        if (suite) {
          return suite.short_name;
        } else {
          return '';
        }
    }

    /**
     * Get city of location (it's locality component of address).
     * @returns string
     */
    this.getCity = function() {
        if (this.isValid()) {
            var locality = this.findAddressComponent('locality');
            if (locality) {
                return locality.long_name;
            }
            var componentName = this.getState() ? 'administrative_area_level_2' : 'administrative_area_level_1';
            var component = this.findAddressComponent(componentName);
            return component ? component.long_name : '';
        }
        return '';
    }

    /**
     * Get state of location (it's administrative_area_level_1 component of address).
     * @returns string
     */
    this.getState = function() {
        if (this.isValid()) {
            var component = this.findAddressComponent('administrative_area_level_1');
            return component && component.short_name !== component.long_name ? component.short_name : '';
        }
        return '';
    }

    /**
     * Get zip of location (it's postal_code component of address).
     * @returns string
     */
    this.getZip = function() {
        if (this.isValid()) {
            var component = this.findAddressComponent('postal_code');
            return component ? component.long_name : '';
        }
        return '';
    }

    /**
     * Get country of location (it's country component of address).
     * @returns string
     */
    this.getCountry = function() {
        if (this.isValid()) {
            var component = this.findAddressComponent('country');
            return component ? component.long_name : '';
        }
        return '';
    }

    /**
     * Find a component in address components by name.
     * @param name
     */
    this.findAddressComponent = function(name) {
        if (this.place.address_components) {
            for (var i = 0; i < this.place.address_components.length; ++i) {
                if (this.place.address_components[i].types[0] === name) {
                    return this.place.address_components[i];
                }
            }
        }
        return null;
    }
}
