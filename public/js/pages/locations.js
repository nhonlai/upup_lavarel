'use strict';

(function ($) {
  var modelList,
    modelTpl,
    searchText;

  var paginator = new Paginator({
    url: '/admin/locations',
    success: updateList,
    noResult: renderNoResult
  });

  $(function () {
    modelList = $('#location-list tbody');
    modelTpl = $('#location-tpl').html();
    searchText = $('#search-text');

    Mustache.parse(modelTpl);

    paginator.firstSearch(searchText.val());

    searchText.on('keypress', function (e) {
      if (e.keyCode == 13) {
        paginator.firstSearch(searchText.val());
      }
    });
  });

  $(window).on("load", function () {
    $(".table-body-wrapper").mCustomScrollbar({
      scrollbarPosition: "inside",
      theme: 'minimal-dark',
      callbacks: {
        alwaysTriggerOffsets: true,
        onTotalScrollOffset: 300,
        onTotalScroll: function () {
          paginator.nextPage(searchText.val());
        }
      },
      advanced: {
        autoUpdateTimeout: 20
      }
    });
  });

  function updateList(res) {
    if (paginator.currentPage() == 1) {
      modelList.html('');
    }
    res.data.forEach(renderRow);
    modelList.parent().mCustomScrollbar('update');
    $('#total').text(paginator.totalItem());
  }

  function renderRow(model) {
    var row = $(Mustache.render(modelTpl, model));
    modelList.append(row);
  }

  function renderNoResult() {
    modelList.html('<tr class="text-center"><td>No result</td></tr>');
  }
})(jQuery);
