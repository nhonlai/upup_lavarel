#!/bin/bash

if [ `which apt-get` ]; then
  packageInstall="sudo apt-get install -y "
elif [ `which dnf` ]; then
  packageInstall="sudo dnf install -y "
elif [ `which yum` ]; then
  packageInstall="sudo yum install -y "
fi

if [ -z "${packageInstall}" ]; then
  echo "Package manager not found (apt-get, dnf, yum supported)"
  exit 1
fi

# Install package $2 if command $1 not found
function ensureInstalled {
  if [ ! `which $1` ]; then
    ${packageInstall} ${2:-$1}
  fi
}

# Install required tools
ensureInstalled cowsay

# Install pre-commit hook
cp .git-hooks/pre-commit .git/hooks/pre-commit

# Instal vagrant plugin, that registeres a VM its internal hostname in /etc/hosts file
if [ `which vagrant` ]; then
  vagrant plugin install vagrant-hostsupdater
fi

# Mark installed
echo `date` >> .install
