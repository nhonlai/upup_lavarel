<?php

/**
 * Provide configs about push notification on apps
 */
return [
    'PUSH_TOKEN' => env('PUSH_TOKEN', 'mE3hzB9Ctyw9PF5K'),
    'IS_DEBUG' => env('PUSH_IS_DEBUG', 1),
    'IS_SANDBOX' => env('PUSH_IS_SANDBOX', 0),
    'LOG_FILE' => env('PUSH_LOG_FILE', storage_path('logs/push.log')),
    'GOOGLE_API_KEY' => env('PUSH_GOOGLE_API_KEY', 'AIzaSyD3NWxubslwCdFOfGCqw3vro6fTJNCdmAM'),
    'IOS_SANDBOX_PASSPHARSE' => env('PUSH_IOS_SANDBOX_PASSPHARSE', ''),
    'IOS_PRODUCTION_PASSPHARSE' => env('PUSH_IOS_PRODUCTION_PASSPHARSE', 'sar1tasa'),
];