<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => \App\Model\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'app_id' => env('FACEBOOK_APP_ID', '267313637049366'),
        'app_secret' => env('FACEBOOK_APP_SECRET', 'c0eb4bd2b51467ebe740aec59272c433'),
        'default_graph_version' => 'v2.8',
    ],

    'linked_in' => [
        'app_id' => env('LINKED_IN_APP_ID', 'dyccsp1nslrn'),
        'app_secret' => env('LINKED_IN_APP_SECRET', 'BJqTpVqFiFJdSzv6'),
    ],

    'googlemaps' => [
        'api_key' => env('GOOGLE_MAP_API_KEY', 'AIzaSyAKQHQhre8zT5RVa9PdqcNNrCiV6BhXecE'),
    ],

];
