FROM docker.saritasa.com/centos7-php56-nginx-phpfpm-speedbracket

LABEL Vendor=CentOS \
    License=GPLv2 \
    Version=0.1

ENTRYPOINT []
WORKDIR /home/www/app

### Environment arguments
ARG PHP_ENV

### Add composer to image for speedup builds
COPY ./composer.* /home/www/app/
RUN mv /home/www/app/composer.json /tmp/composer.json \
    && cat /tmp/composer.json | jq 'del(.["scripts", "autoload", "autoload-dev"])' > /home/www/app/composer.json \
    && cd /home/www/app && composer install --prefer-source --no-interaction

### Add node packages and precompile saas
COPY ./*.js* /home/www/app/
##RUN npm install && npm install -g grunt \&& echo Y | bower install --allow-root

### Default environment variables
#ENV APP_ENV=${PHP_ENV:-development} APP_DEBUG=true
 
### Copy configuration files
#ADD docker/${APP_ENV} /
ADD docker/development /

### Copy application files
ADD . /home/www/app

### Assemble static files
#RUN grunt build
 
### Run our container
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
