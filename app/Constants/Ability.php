<?php
namespace App\Constants;

use App\Helpers\Enum;

/**
 * Class Ability
 * @package App\Constants
 */
class Ability extends Enum
{
    const TASK_APPROVE = 'task-approve';
    const SECTION_CREATE = 'section-create';
    const SECTION_UPDATE = 'section-update';
    const SECTION_DELETE = 'section-delete';
}
