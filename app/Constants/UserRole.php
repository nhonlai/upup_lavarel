<?php
namespace App\Constants;

use App\Helpers\Enum;

/**
 * Class UserRole
 * @package App\Constants
 */
class UserRole extends Enum
{
    const FOUNDER = 'founder';
    const MANAGER = 'manager';
    const MEMBER = 'member';
}
