<?php

namespace App\Listeners;

use App\Events\TaskDone;
use App\Jobs\SendTaskDoneNotification;
use App\Repositories\TaskRepository;

class TaskDoneListener
{

    /** @var  TaskRepository */
    protected $taskRepository;

    public function __construct()
    {
        $this->taskRepository = app(TaskRepository::class);
    }

    /**
     * Handle the event.
     *
     * @param  TaskDone $event
     * @return void
     */
    public function handle(TaskDone $event)
    {

        \Log::debug(['task done', $event->task->toArray()]);
        dispatch(new SendTaskDoneNotification($event->task));
    }
}
