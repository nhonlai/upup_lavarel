<?php

namespace App\Listeners;

use Dingo\Api\Event\ResponseWasMorphed;

class AddPaginationLinksToResponse
{
    /**
     * @param ResponseWasMorphed $event
     */
    public function handle(ResponseWasMorphed $event)
    {
        if (isset($event->content['meta']) && isset($event->content['meta']['pagination'])) {
            $links = &$event->content['meta']['pagination']['links'];
            // correct the links parameter of pagination
            $links = [
                'previous' => empty($links['previous']) ? null : $links['previous'],
                'next' => empty($links['next']) ? null : $links['next'],
            ];
        }
    }
}