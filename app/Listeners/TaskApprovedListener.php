<?php

namespace App\Listeners;

use App\Events\TaskApproved;
use App\Repositories\TaskRepository;

class TaskApprovedListener
{

    /** @var  TaskRepository */
    protected $taskRepository;

    public function __construct()
    {
        $this->taskRepository = app(TaskRepository::class);
    }

    /**
     * Handle the event.
     *
     * @param  TaskApproved $event
     * @return void
     */
    public function handle(TaskApproved $event)
    {

        \Log::debug(['task approved', $event->task->toArray()]);
        if ($event->task->keyResult) {
            $event->task->keyResult->goToNextMileStone();
        }
        $this->taskRepository->addBadgeRewardForMember($event->task);
        $this->taskRepository->addBadgeRewardForManager($event->task);
    }
}
