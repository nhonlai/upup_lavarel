<?php
namespace App\Services\SocialService;

interface SocialTypeInterface
{
    /**
     * Facebook social type.
     * @var string
     */
    const FB_TYPE = 'fb';

    /**
     * Instagram social type.
     * @var string
     */
    const IN_TYPE = 'in';

    /**
     * LinkedIn social type.
     * @var string
     */
    const LINKED_IN_TYPE = 'li';

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $type
     * @return SocialType
     */
    public static function create($type);
}
