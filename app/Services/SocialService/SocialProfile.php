<?php

namespace App\Services\SocialService;

/**
 * Class SocialProfile
 */
class SocialProfile
{
    public $id;
    public $email;
    public $firstName;
    public $lastName;
    public $middleName;
    public $avatar;
    public $gender;
    public $dob;

    /**
     * @var SocialTypeInterface
     */
    public $type;
    protected $data;

    /**
     * SocialProfile constructor.
     * @param SocialTypeInterface $type
     */
    protected function __construct(SocialTypeInterface $type)
    {
        $this->type = $type;
        $this->data = [];
    }

    /**
     * @param \Facebook\GraphNodes\GraphUser $user
     * @return SocialProfile
     */
    public static function createFromFacebookProfile(\Facebook\GraphNodes\GraphUser $user)
    {
        $profile = new static(SocialType::create(SocialTypeInterface::FB_TYPE));
        return $profile->loadProfileFromFacebook($user);
    }

    /**
     * @param \Facebook\GraphNodes\GraphUser $user
     * @return $this
     */
    protected function loadProfileFromFacebook(\Facebook\GraphNodes\GraphUser $user)
    {
        $this->data = $user->all();
        $this->id = $user->getId();
        $this->email = strtolower($user->getEmail());
        $this->gender = $user->getGender() == 'male' ? 1 : 0;
        $this->firstName = $user->getFirstName();
        $this->lastName = $user->getLastName();
        if ($user->getMiddleName()) {
            $this->middleName= $user->getMiddleName();
        }
        if ($birthday = $user->getBirthday()) {
            $dateParts = explode('/', $birthday);
            if (count($dateParts) == 3) {
                $this->dob = $dateParts[2] . '-' . $dateParts[1] . '-' . $dateParts[0];
            }
        }
        $picture = $user->getPicture();
        if ($picture && $picture->getUrl()) {
            $this->avatar = $picture->getUrl();
        }
        return $this;
    }

    /**
     * @param array $data
     * @return SocialProfile
     */
    public static function createFromInstagramProfile(array $data)
    {
        $profile = new static(SocialType::create(SocialTypeInterface::IN_TYPE));
        return $profile->loadProfileFromInstagram($data);
    }

    /**
     * @param array $data
     * @return $this
     */
    protected function loadProfileFromInstagram(array $data)
    {
        $this->data = $data;
        $this->id = $data['id'];
        $this->email = $this->getEmailFromProfileId($this->type->getName(), $this->id);
        $this->gender = 1;
        $names = explode(' ', $data['full_name']);
        $this->firstName = $names[0];
        if (count($names) > 1) {
            $this->lastName = array_pop($names);
        }
        if (count($names) > 1) {
            $this->middleName = implode(' ', array_slice($names, 1));
        }
        if (!empty($data['profile_picture'])) {
            $this->avatar = $data['profile_picture'];
        }
        return $this;
    }

    /**
     * @param array $data
     * @return SocialProfile
     */
    public static function createFromLinkedInProfile(array $data)
    {
        $profile = new static(SocialType::create(SocialTypeInterface::LINKED_IN_TYPE));
        return $profile->loadProfileFromLinkedIn($data);
    }

    /**
     * @param array $data
     * @return $this
     */
    protected function loadProfileFromLinkedIn(array $data)
    {
        $this->data = $data;
        $this->id = $data['id'];
        $this->email = strtolower($data['emailAddress']);
        $this->firstName = $data['firstName'];
        $this->lastName = $data['lastName'];
        $this->avatar = $data['pictureUrl'];
        $this->gender = 1;

        return $this;
    }

    /**
     * @param string $type
     * @param string $profileId
     * @return string
     */
    protected function getEmailFromProfileId($type, $profileId)
    {
        return "${type}_${profileId}@internal.google.com";
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return json_decode(json_encode($this), true);
    }
}
