<?php
namespace App\Services\SocialService;

class SocialType implements SocialTypeInterface
{
    /**
     * @var string
     */
    protected $type;

    /**
     * SocialType constructor.
     * @param string $type
     */
    protected function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return SocialType
     */
    public static function create($type) {
        return new static($type);
    }
}
