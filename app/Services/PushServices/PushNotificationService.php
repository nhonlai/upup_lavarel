<?php

namespace App\Services\PushServices;

/**
 * Interface IPushNotificationService
 *
 * @package App\Services\PushServices
 */
interface IPushNotificationService
{
    /**
     * Send push notification to given device tokens or registration ids.
     *
     * @param mixed $to
     * @param string $message
     * @param array $options
     * @return bool
     */
    public function send($to, $message, $options = array());
}

/**
 * Abstract class PushNotificationService
 *
 * @package App\Services\PushServices
 */
abstract class PushNotificationService implements IPushNotificationService, ILogInterface
{
    use LogTrait;
}
