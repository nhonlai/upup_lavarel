<?php
namespace App\Services\PushServices;

/**
 * Interface ILogInterface is used for implementation logging messages.
 *
 * @package App\Services\PushServices
 */
interface ILogInterface
{
    /**
     * Log type error.
     * @var string
     */
    const LOG_TYPE_ERROR = 'ERROR';

    /**
     * Log type info.
     * @var string
     */
    const LOG_TYPE_INFO = 'INFO';
}