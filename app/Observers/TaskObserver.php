<?php

namespace App\Observers;

use App\Events\TaskApproved;
use App\Events\TaskDone;
use App\Model\KeyResult;
use App\Model\Task;
use App\Model\TaskStatus;

class TaskObserver
{
    /**
     * @var array
     */
    protected static $dirty = [];

    /**
     * @param Task $task
     */
    public function saving(Task $task)
    {
        // cache dirty
        static::$dirty = $task->getDirty();
    }

    /**
     * @param Task $task
     */
    public function saved(Task $task)
    {
        if (array_has(static::$dirty, 'status_id') || array_has(static::$dirty, 'key_result_id')) {
            $oldKeyResultId = $task->getOriginal('key_result_id');
//            if ($oldKeyResultId && $oldKeyResultId != $task->key_result_id) {
//                $keyResult = KeyResult::find($oldKeyResultId);
//                $keyResult->updateCompletedPercent();
//            }
//            if ($task->keyResult) {
//                $task->keyResult->updateCompletedPercent();
//            }
        }
        if (array_has(static::$dirty, 'status_id')) {
            $this->performWhenStatusChanged($task);
        }
    }

    /**
     * @param Task $task
     */
    public function performWhenStatusChanged(Task $task)
    {
        $oldStatusId = $task->getOriginal('status_id');
        \Log::debug("Old status $oldStatusId; new status $task->status_id");
        if ($task->status_id == TaskStatus::APPROVED) {
            event(new TaskApproved($task));
        }
        if ($task->status_id == TaskStatus::DONE) {
            if ($oldStatusId != TaskStatus::APPROVED) {
                event(new TaskDone($task));
            }
        }
    }
}
