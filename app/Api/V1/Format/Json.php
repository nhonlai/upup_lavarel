<?php

namespace App\Api\V1\Format;

class Json extends \Dingo\Api\Http\Response\Format\Json
{
    /**
     * Encode the content to its JSON representation.
     *
     * @param string $content
     *
     * @return string
     */
    protected function encode($content)
    {
        if (!isset($content['status'])) {
            $content = ['status' => 'success'] + $content;
        }
        if (isset($content['errors']) && is_array($content['errors'])) {
            $errors = [];
            foreach($content['errors'] as $key => $value) {
                if (is_string($key)) {
                    $errors[] = [
                        'name' => $key,
                        'value' => $value,
                    ];
                } else {
                    $errors[] = $value;
                }
            }
            $content['errors'] = $errors;
        }
        return json_encode($content);
    }
}
