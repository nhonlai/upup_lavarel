<?php
namespace App\Api\V1\Transformers;

use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;

class BaseTransformer extends TransformerAbstract
{

    public function transform(Model $model)
    {
        return $model->toArray();
    }

}
?>