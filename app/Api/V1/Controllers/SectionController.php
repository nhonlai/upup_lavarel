<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Transformers\BaseTransformer;
use App\Constants\Ability;
use App\Model\TaskStatus;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Http\Request;
use App\Repositories\SectionRepository;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Section resource representation.
 *
 * @Resource("Sections", uri="/api/v1/sections")
 */
class SectionController extends BaseController
{
    /**
     * @var SectionRepository
     */
    public $sectionRepository;

    /**
     * Construct method. Define repo of resource
     *
     * @param SectionRepository $sectionRepository
     */
    public function __construct(SectionRepository $sectionRepository)
    {
        $this->sectionRepository = $sectionRepository;
    }

    /**
     * @return \Dingo\Api\Http\Response
     * @throws UpdateResourceFailedException
     */
    public function store()
    {
        if (\Gate::forUser($this->user)->denies(Ability::SECTION_CREATE)) {
            throw new AccessDeniedHttpException('You have no permission.');
        }

        if ($section = $this->sectionRepository->store(null, Input::all(), $this->user)) {
            return $this->response->item($section, new BaseTransformer);
        } else {
            throw new UpdateResourceFailedException('Could not create section.', $this->sectionRepository->errors());
        }
    }

    /**
     * @param integer $id
     * @return \Dingo\Api\Http\Response
     */
    public function update($id)
    {
        /** @var TaskStatus $section */
        $section = $this->sectionRepository->getModelByID($id);
        if (\Gate::forUser($this->user)->denies(Ability::SECTION_UPDATE, $section)) {
            throw new AccessDeniedHttpException('You have no permission.');
        }
        if ($section = $this->sectionRepository->store($id, Input::all(), $this->user)) {
            return $this->response->item($section, new BaseTransformer);
        } else {
            throw new UpdateResourceFailedException('Could not update section.', $this->sectionRepository->errors());
        }
    }

    /**
     * @param integer $id
     * @return \Dingo\Api\Http\Response
     */
    public function show($id)
    {
        return $this->response->item($this->sectionRepository->getModelByID($id), new BaseTransformer);
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response->paginator(
            $this->sectionRepository->listSections($this->user->organization_id, $request),
            new BaseTransformer
        );
    }

    /**
     * @param integer $id
     * @return \Dingo\Api\Http\Response
     * @return \Dingo\Api\Http\Response
     */
    public function destroy($id)
    {
        /** @var TaskStatus $section */
        $section = $this->sectionRepository->getModelByID($id);
        if (\Gate::forUser($this->user)->denies(Ability::SECTION_DELETE, $section)) {
            throw new AccessDeniedHttpException('You have no permission.');
        }

        try {
            $section->delete();
        } catch (\Exception $e) {
            throw new BadRequestHttpException('This section is already in used.');
        }
        return $this->success();
    }
}
