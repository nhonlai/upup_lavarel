<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Transformers\BaseTransformer;
use Illuminate\Http\Request;
use App\Repositories\ObjectiveRepository;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Support\Facades\Input;

/**
 * Objective resource representation.
 *
 * @Resource("Objectives", uri="/api/v1/objectives")
 */
class ObjectiveController extends BaseController
{
    /**
     * @var ObjectiveRepository
     */
    public $objectiveRepository;

    /**
     * Construct method. Define repo of resource
     *
     * @param ObjectiveRepository $objectiveRepository
     */
    public function __construct(ObjectiveRepository $objectiveRepository)
    {
        $this->objectiveRepository = $objectiveRepository;
    }

    /**
     * @param integer $id
     * @return \Dingo\Api\Http\Response
     * @throws UpdateResourceFailedException
     */
    public function store($id)
    {
        if ($objective = $this->objectiveRepository->store($id, Input::all(), $this->user)) {
            return $this->response->item($objective, new BaseTransformer);
        } else {
            throw new UpdateResourceFailedException('Could not create objective.', $this->objectiveRepository->errors());
        }
    }

    /**
     * @param integer $id
     * @return \Dingo\Api\Http\Response
     */
    public function update($id)
    {
        if ($objective = $this->objectiveRepository->update($id, Input::all(), $this->user)) {
            return $this->response->item($objective, new BaseTransformer);
        } else {
            throw new UpdateResourceFailedException('Could not update objective.', $this->objectiveRepository->errors());
        }
    }

    /**
     * @param integer $id
     * @return \Dingo\Api\Http\Response
     */
    public function show($id)
    {
        return $this->response->item($this->objectiveRepository->getModelByID($id), new BaseTransformer);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function index($id, Request $request)
    {
        return $this->response->paginator(
            $this->objectiveRepository->listObjectives($id, $request),
            new BaseTransformer
        )->addMeta(
            'total_completed_percent',
            $this->objectiveRepository->calculateTotalCompletedPercent($id)
        );
    }
}
