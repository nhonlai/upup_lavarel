<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Transformers\BaseTransformer;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;
use App\Repositories\BadgeRewardRepository;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Support\Facades\Input;

/**
 * BadgeReward resource representation.
 *
 * @Resource("BadgeRewards", uri="/api/v1/badgeRewards")
 */
class BadgeRewardController extends BaseController
{
    /**
     * @var BadgeRewardRepository
     */
    public $badgeRewardRepository;

    /**
     * Construct method. Define repo of resource
     *
     * @param BadgeRewardRepository $badgeRewardRepository
     */
    public function __construct(BadgeRewardRepository $badgeRewardRepository)
    {
        $this->badgeRewardRepository = $badgeRewardRepository;
    }

    /**
     * @param integer $id
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function update($id, Request $request)
    {
        if ($badgeReward = $this->badgeRewardRepository->store($id, $request->input(), $this->user)) {
            return $this->response->item($badgeReward, new BaseTransformer);
        } else {
            throw new UpdateResourceFailedException('Could not update badgeReward.', $this->badgeRewardRepository->errors());
        }
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function index($id)
    {
        return $this->response->collection($this->badgeRewardRepository->listBadgeRewardsByOrganization($id, $this->user), new BaseTransformer);
    }
}
