<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Transformers\BaseTransformer;
use App\Repositories\ObjectiveRepository;
use App\Repositories\KeyResultRepository;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * KeyResult resource representation.
 *
 * @Resource("KeyResults", uri="/api/v1/key_results")
 */
class KeyResultController extends BaseController
{
    /**
     * @var KeyResultRepository
     */
    public $keyResultRepository;

    /**
     * @var ObjectiveRepository
     */
    public $objectiveRepository;

    /**
     * Construct method. Define repo of resource
     *
     * @param KeyResultRepository $keyResultRepository
     * @param ObjectiveRepository $objectiveRepository
     */
    public function __construct(KeyResultRepository $keyResultRepository, ObjectiveRepository $objectiveRepository)
    {
        $this->keyResultRepository = $keyResultRepository;
        $this->objectiveRepository = $objectiveRepository;
    }

    /**
     * @param integer $id_objective
     * @return \Dingo\Api\Http\Response
     * @throws UpdateResourceFailedException
     */
    public function store($id_objective)
    {
        $objective = $this->objectiveRepository->get($id_objective, $this->user);

        if ($keyResult = $this->keyResultRepository->store(null, Input::all(), $objective, $this->user)) {
            return $this->response->item($keyResult, new BaseTransformer);
        } else {
            throw new UpdateResourceFailedException('Could not create key result.', $this->keyResultRepository->errors());
        }
    }

    /**
     * @param integer $id_objective
     * @param integer $id
     * @return \Dingo\Api\Http\Response
     */
    public function update($id_objective, $id)
    {
        $objective = $this->objectiveRepository->get($id_objective, $this->user);

        if ($keyResult = $this->keyResultRepository->store($id, Input::all(), $objective, $this->user)) {
            return $this->response->item($keyResult, new BaseTransformer);
        } else {
            throw new UpdateResourceFailedException('Could not update key result.', $this->keyResultRepository->errors());
        }
    }

    /**
     * @param integer $id_objective
     * @param integer $id
     * @return \Dingo\Api\Http\Response
     */
    public function show($id_objective, $id)
    {
        $objective = $this->objectiveRepository->get($id_objective, $this->user);
        $keyResult = $this->keyResultRepository->getModelByID($id);
        if (!$objective || !$keyResult || $objective->id != $keyResult->objective_id) {
            throw new NotFoundHttpException();
        }

        return $this->response->item($keyResult, new BaseTransformer);
    }

    /**
     * integer $id_objective
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function index($id_objective, Request $request)
    {
        return $this->response->paginator($this->keyResultRepository->listKeyResults($id_objective, $request, $this->user), new BaseTransformer);
    }

    /**
     * @param integer $id_objective
     * @param integer $id
     * @return \Dingo\Api\Http\Response
     */
    public function destroy($id_objective, $id)
    {
        $objective = $this->objectiveRepository->get($id_objective, $this->user);
        $keyResult = $this->keyResultRepository->getModelByID($id);
        if (!$objective || !$keyResult || $objective->id != $keyResult->objective_id) {
            throw new NotFoundHttpException();
        }

        if ($this->keyResultRepository->destroy($id)) {
            return $this->success();
        } else {
            throw new DeleteResourceFailedException('Could not delete key result.', $this->keyResultRepository->errors());
        }
    }
}
