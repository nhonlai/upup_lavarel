<?php

namespace App\Api\V1\Controllers;
use Illuminate\Http\Request;
use Cloudinary;

/**
 * Authenticate resource representation.
 *
 * @Resource("Authenticate", uri="/api/v1")
 */
class UploadController extends BaseController
{
    /**
     * Get Signature
     *
     * @param Request $request
     * @return
     */
    public function getSignature(Request $request)
    {
        $options = [
            'api_key' => config('cloudder.apiKey'),
            'api_secret' => config('cloudder.apiSecret')
        ];
        $signRequest = Cloudinary::sign_request([
            'timestamp' => time()
        ], $options);
        return $this->success($signRequest, 'Get upload signature successfully');
    }
}
