<?php

namespace App\Api\V1\Controllers;
use App\Api\V1\Transformers\BaseTransformer;
use App\Repositories\InviteMemberRepository;
use App\Repositories\PasswordResetRepository;
use App\Repositories\UserRepository;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\ResourceException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

/**
 * User resource representation.
 *
 * @Resource("Users", uri="/api/v1/users")
 */
class UserController extends BaseController
{
    /**
     * @var UserRepository
     */
    public $userRepo;

    /**
     * @var InviteMemberRepository
     */
    public $inviteMemberRepository;

    /**
     * Construct method. Define repo of resource
     *
     * @param UserRepository          $userRepo
     * @param InviteMemberRepository $inviteMemberRepository
     */
    public function __construct(UserRepository $userRepo, InviteMemberRepository $inviteMemberRepository)
    {
        $this->userRepo          = $userRepo;
        $this->inviteMemberRepository = $inviteMemberRepository;
    }


    /**
     * Register user
     *
     * Register a new user with a `email` and `password`.
     */
    public function store()
    {
        if ($user = $this->userRepo->initUserInfo(Input::all())) {
            $token = \JWTAuth::fromUser($user);
            $user = $this->userRepo->getAdditionalInfo($user);
            return $this->success(['user' => $user, 'token' => $token]);
        } else {
            throw new StoreResourceFailedException('Could not create new user.', $this->userRepo->errors());
        }
    }


    /**
     * Update user
     *
     * @Put("/{id}")
     * @Versions({"v1"})
     * @Request({
     *     "first_name" : "John",
     *     "last_name" : "Doe",
     *     "email": "email@example.com",
     *     "password": "123456",
     *     "company" : "Saritasa",
     *     "account_number" : "E111",
     *     "phone" : "",
     *     "address" : "",
     *     "address2" : "",
     *     "city" : "",
     *     "state" : "WA",
     *     "zip" : "",
     *     "country" : "United States"
     * }, headers={"Authorization": "Bearer {AccessToken}", "Content-Type": "application/json"})
     * @Response(200, body={
     *     "status":"success",
     *     "data" : {
     *          "first_name" : "John",
     *          "last_name" : "Doe",
     *          "email": "email@example.com",
     *          "password": "123456",
     *          "company" : "Saritasa",
     *          "account_number" : "E111",
     *          "phone" : "",
     *          "address" : "",
     *          "address2" : "",
     *          "city" : "",
     *          "state" : "WA",
     *          "zip" : "",
     *          "country" : "United States",
     *          "id" : 99
     *     }
     * })
     *
     * @param $id
     * @param $request
     *
     * @return \Dingo\Api\Http\Response
     */
    public function update($id, Request $request)
    {
        if ($user = $this->userRepo->store($id, $request->input())) {
            return $this->response->item($user, new BaseTransformer);
        } else {
            throw new UpdateResourceFailedException('Could not update user.', $this->userRepo->errors());
        }
    }

    /**
     * Update user password.
     *
     * @Put("/me/password")
     * @Versions({"v1"})
     * @Request({
     *     "current_password" : "current password",
     *     "new_password" : "new password"
     * }, headers={"Authorization": "Bearer {AccessToken}", "Content-Type": "application/json"})
     * @Response(200, body={
     *     "status":"success",
     *     "data" : {
     *        "status": "success",
     *        "message": "Your account password has been changed successfully!"
     *     }
     * })
     *
     * @param $request
     *
     * @return \Dingo\Api\Http\Response
     */
    public function updatePassword(Request $request)
    {
        if ($this->passwordResetRepo->updatePassword($request->input(), $this->user)) {
            return $this->success(null, 'Your account password has been changed successfully!');
        } else {
            throw new UpdateResourceFailedException('Could not update user password.', $this->passwordResetRepo->errors());
        }
    }

    /**
     * Delete user
     *
     * @Delete("/{id}")
     * @Versions({"v1"})
     * @Request(headers={"Authorization": "Bearer {AccessToken}", "Content-Type": "application/json"})
     * @Response(200, body={"status":"success"})
     *
     */
    public function destroy($id)
    {
        if ($this->userRepo->destroy($id)) {
            return $this->success();
        } else {
            throw new DeleteResourceFailedException('Could not update user.', $this->userRepo->errors());
        }
    }


    /**
     * Show all users
     *
     * Get a JSON representation of all the registered users.
     *
     * @Get("/{?page,limit,fields,search,sort}")
     * @Versions({"v1"})
     * @Request(headers={"Authorization": "Bearer {AccessToken}"})
     * @Parameters({
     *      @Parameter("page", type="integer", description="The page of results to view.", default=1),
     *      @Parameter("limit", type="integer", description="The amount of results per page.", default=10),
     *      @Parameter("fields", description="List of fields separated by comma", default="*"),
     *      @Parameter("search", description="Search keyword by searcheble fields", default="none"),
     *      @Parameter("filter", description="List of filters (field:value) separated by comma", default="none"),
     *      @Parameter("sort", description="List of sort fields separated by comma", default="none")
     * })
     * @Response(200, body={
     *     "status" : "success",
     *     "data" : "<array of each user profile>",
     *     "meta" : {
     *       "pagination" : {
     *          "total": 10,
     *          "count": 1,
     *          "per_page": 1,
     *          "current_page": 1,
     *          "total_pages": 10,
     *          "links": {
     *              "previous": null,
     *              "next": "http://fbm.dev/api/v1/users?limit=1&page=2"
     *          }
     *        }
     *     }
     * })
     *
     */
    public function index(Request $request)
    {
        return $this->response->paginator($this->userRepo->getList($request), new BaseTransformer);
    }


    /**
     * Show user details
     *
     * @get("/{id}")
     * @Versions({"v1"})
     * @Request(headers={"Authorization": "Bearer {AccessToken}", "Content-Type": "application/json"})
     * @Response(200, body={
     *     "status":"success",
     *     "data" : {
     *          "first_name" : "John",
     *          "last_name" : "Doe",
     *          "email": "email@example.com",
     *          "password": "123456",
     *          "company" : "Saritasa",
     *          "account_number" : "E111",
     *          "phone" : "",
     *          "address" : "",
     *          "address2" : "",
     *          "city" : "",
     *          "state" : "WA",
     *          "zip" : "",
     *          "country" : "United States",
     *          "id" : 99,
     *          "created_at" : "2016-09-15 03:58:56",
     *          "updated_at" : "2016-09-15 04:58:56"
     *     }
     * })
     */
    public function show($id)
    {
        return $this->response->item($this->userRepo->getProfile($id, $this->user), new BaseTransformer);
    }

    /**
     * Show authenticated user details
     *
     * @get("/profile")
     * @Versions({"v1"})
     * @Request(headers={"Authorization": "Bearer {AccessToken}", "Content-Type": "application/json"})
     * @Response(200, body={
     *     "status":"success",
     *     "data" : {
     *          "first_name" : "John",
     *          "last_name" : "Doe",
     *          "email": "email@example.com",
     *          "password": "123456",
     *          "company" : "Saritasa",
     *          "account_number" : "E111",
     *          "phone" : "",
     *          "address" : "",
     *          "address2" : "",
     *          "city" : "",
     *          "state" : "WA",
     *          "zip" : "",
     *          "country" : "United States",
     *          "id" : 99,
     *          "created_at" : "2016-09-15 03:58:56",
     *          "updated_at" : "2016-09-15 04:58:56"
     *     }
     * })
     */
    public function profile()
    {
        return $this->response->item($this->userRepo->getProfile($this->user->id, $this->user), new BaseTransformer);
    }
}
