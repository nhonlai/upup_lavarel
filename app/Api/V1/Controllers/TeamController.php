<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Transformers\BaseTransformer;
use App\Repositories\InviteMemberRepository;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;
use App\Repositories\TeamRepository;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Support\Facades\Input;

/**
 * Team resource representation.
 *
 * @Resource("Teams", uri="/api/v1/teams")
 */
class TeamController extends BaseController
{
    /**
     * @var TeamRepository
     */
    public $teamRepository;

    /**
     * @var InviteMemberRepository
     */
    public $inviteMemberRepository;

    /**
     * Construct method. Define repo of resource
     *
     * @param TeamRepository $teamRepository
     * @param InviteMemberRepository $inviteMemberRepository
     */
    public function __construct(TeamRepository $teamRepository, InviteMemberRepository $inviteMemberRepository)
    {
        $this->teamRepository = $teamRepository;
        $this->inviteMemberRepository = $inviteMemberRepository;
    }

    /**
     * @param integer $id
     * @return \Dingo\Api\Http\Response
     * @throws UpdateResourceFailedException
     */
    public function store($id)
    {
        if ($team = $this->teamRepository->store($id, Input::all(), $this->user)) {
            return $this->response->item($team, new BaseTransformer);
        } else {
            throw new UpdateResourceFailedException('Could not create team.', $this->teamRepository->errors());
        }
    }

    /**
     * @param integer $id
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function update($id, Request $request)
    {
        if ($team = $this->teamRepository->update($id, $request->input(), $this->user)) {
            return $this->response->item($team, new BaseTransformer);
        } else {
            throw new UpdateResourceFailedException('Could not update team.', $this->teamRepository->errors());
        }
    }

    /**
     * @param integer $id
     * @return \Dingo\Api\Http\Response
     */
    public function show($id)
    {
        return $this->response->item($this->teamRepository->getModelByID($id), new BaseTransformer);
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function index($id, Request $request)
    {
        return $this->response->paginator($this->teamRepository->listTeamsByUser($id, $request, $this->user), new BaseTransformer);
    }

    /**
     * @param integer $id
     */
    public function destroy($id)
    {
        if ($this->teamRepository->destroy($id, $this->user)) {
            return $this->success();
        } else {
            throw new DeleteResourceFailedException('Could not delete team.', $this->teamRepository->errors());
        }
    }

    /**
     * @param integer $teamId
     * @param Request $request
     * @return mixed
     * @throws StoreResourceFailedException
     */
    public function invite($teamId, Request $request)
    {
        if (!$this->inviteMemberRepository->inviteTeamMember($teamId, $request->input(), $this->user)) {
            throw new StoreResourceFailedException('Could not send invite', $this->inviteMemberRepository->errors());
        }
        return $this->success(null, 'Sent invitation email.');
    }

    /**
     * @param integer $id
     * @param Request $request
     * @return mixed
     */
    public function getMembers($id, Request $request)
    {
        return $this->response->paginator($this->teamRepository->getTeamMembers($request, $id, $this->user), new BaseTransformer);
    }
}
