<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Exceptions\ModelException;
use App\Model\User;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;


/**
 * Class BaseController
 * @package App\Api\V1\Controllers
 *
 * @property User $user
 */
class BaseController extends Controller
{
    use Helpers;

    public function success( $data = null, $message = null)
    {
        $content = [];
        
        if ($data !== null)
            $content['data'] = $data;

        if ($message !== null)
            $content['message'] = $message;

        return $this->response->array($content);
    }

    public function validate(Request $request, array $rules)
    {
        $payload = $request->only(array_keys($rules));
        $validator = app('validator')->make($payload, $rules);
        if ($validator->fails()) {
            throw new ModelException(400, 'invalid', $validator->errors());
        }
    }
}
