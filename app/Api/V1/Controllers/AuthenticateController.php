<?php

namespace App\Api\V1\Controllers;
use App\Model\UserOrganization;
use App\Model\UserStatus;
use App\Repositories\DeviceRepository;
use App\Repositories\UserOrganizationRepository;
use App\Repositories\UserRepository;
use App\Repositories\PasswordResetRepository;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Dingo\Api\Exception\ResourceException;


/**
 * Authenticate resource representation.
 *
 * @Resource("Authenticate", uri="/api/v1")
 */
class AuthenticateController extends BaseController
{
    /**
     * @var UserRepository
     */
    public $userRepo;

    /**
     * @var DeviceRepository
     */
    public $deviceRepo;

    /**
     * @var PasswordResetRepository
     */
    public $passwordResetRepo;
    public $userOrganizationRepo;


    /**
     * Construct method. Define repo of resource
     *
     * @param UserRepository $userRepo
     * @param DeviceRepository $deviceRepo
     * @param PasswordResetRepository $passwordResetRepo
     */
    public function __construct(UserRepository $userRepo, DeviceRepository $deviceRepo, PasswordResetRepository $passwordResetRepo, UserOrganizationRepository $userOrganizationRepo)
    {
        $this->userRepo          = $userRepo;
        $this->deviceRepo        = $deviceRepo;
        $this->passwordResetRepo = $passwordResetRepo;
        $this->userOrganizationRepo = $userOrganizationRepo;
    }


    /**
     * Request confirmation code by `email`.
     *
     * @param Request $request
     * @return
     */
    public function requestConfirmationCode(Request $request)
    {
        $rules = $this->userRepo->getRules('code_request');
        $this->validate($request, $rules);
        if ($code = $this->userRepo->requestConfirmationCode($request->input('email'))) {
            // @todo remove confirmation code in response
            return $this->success(['code' => $code . ' (should be removed later)'], 'The confirmation code was sent. Please check your email!');
        } else {
            throw new StoreResourceFailedException('Could not request confirmation code', $this->userRepo->errors());
        }
    }

    /**
     * Verify confirmation code by `email` and `code`.
     *
     * @param Request $request
     * @return
     */
    public function verifyConfirmationCode(Request $request)
    {
        $rules = $this->userRepo->getRules('code_verify');
        $this->validate($request, $rules);
        if ($this->userRepo->verifyConfirmationCode($request->input('email'), $request->input('confirmationCode'))) {
            return $this->success(null, 'Your email is verified successfully.');
        } else {
            throw new StoreResourceFailedException('Could not verify confirmation code', $this->userRepo->errors());
        }
    }

    /**
     * Authenticate user
     *
     * Authenticate user by `email` and `password`.
     *
     * @Post("/login")
     * @Versions({"v1"})
     * @Request({"email": "email@example.com", "password": "123456"}, headers={"Content-Type": "application/json"})
     * @Response(200, body={"status":"success","token":"example-hbGciOiJIUzI1NiJ9"})
     *
     * @param Request $request
     *
     * @return
     */
    public function authenticate(Request $request)
    {
        $rules = $this->userRepo->getRules('auth') + $this->deviceRepo->getRules();
        $this->validate($request, $rules);
        $credentials = $request->only('email', 'password');
        $token = $this->userRepo->JWTAuth($credentials);
        $user = $this->userRepo->getByToken($token);
        if (!$user || $user->status_id !=  UserStatus::ACTIVE) {
            throw new AccessDeniedHttpException('This user is not active.');
        }
        $user = $this->userRepo->getAdditionalInfo($user);

        $this->deviceRepo->store(
            $request->input('device_token'),
            $request->input('device_type'),
            $this->userRepo->getByToken($token)
        );

        return $this->success(['user' => $user, 'token' => $token]);
    }

    /**
     * Login with social token.
     *
     * @Post("/login/social")
     * @Versions({"v1"})
     * @Request({"social_type": "fb", "social_token": "123456"}, headers={"Content-Type": "application/json"})
     * @Response(200, body={"status":"success","token":"example-hbGciOiJIUzI1NiJ9"})
     *
     * @return
     */
    public function socialLogin()
    {
        $user = $this->userRepo->socialLogin(Input::all());

        if ($user && $user->status_id !=  UserStatus::ACTIVE) {
            throw new AccessDeniedHttpException('This user is not active.');
        }

        if ($user) {
            $token = \JWTAuth::fromUser($user);
            return $this->success(['user' => $user, 'token' => $token]);
        } else {
            throw new StoreResourceFailedException('Could not login.', $this->userRepo->errors());
        }
    }

    /**
     * Logout user
     *
     * Invalidate access token
     *
     * @Delete("/logout")
     * @Versions({"v1"})
     * @Request(headers={"Authorization": "Bearer {AccessToken}"})
     * @Response(200, body={"status":"success"})
     *
     * @param Request $request
     *
     * @return
     */
    public function logout(Request $request)
    {
        $this->validate($request, $this->deviceRepo->getRules());
        $token = \JWTAuth::getToken();
        if ($token){
            \JWTAuth::invalidate($token);
            $this->deviceRepo->destroy(
                $request->input('device_token'),
                $request->input('device_type')
            );
        }
        return $this->success();
    }

    /**
     * Ping/pong api
     *
     * @Get("/ping")
     * @Versions({"v1"})
     * @Request(null, headers={"Content-Type": "application/json"})
     * @Response(200, body={"status":"success","data":"pong"})
     *
     * @return array
     */
    public function ping() {
        return $this->success('pong');
    }

    /**
     * Reset password API
     *
     * @Post("/password/reset")
     * @param Request $request
     */
    public function resetPassword(Request $request) {
        if (!$this->passwordResetRepo->submitReset($request->input())) {
            throw new StoreResourceFailedException('Could not submit reset password', $this->passwordResetRepo->errors());
        }
        return $this->success(null, 'Please check your email');
    }

    /**
     * Confirm reset password API
     *
     * @Put("/password/reset")
     * @param Request $request
     */
    public function confirmResetPassword(Request $request)
    {
        if(!$user = $this->passwordResetRepo->confirmReset($request->input())) {
            throw new ResourceException('Could not confirm token', $this->passwordResetRepo->errors());
        }
        return $this->success($user, 'Password was reset');
    }
}
