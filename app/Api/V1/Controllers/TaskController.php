<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Transformers\BaseTransformer;
use App\Constants\Ability;
use App\Model\Task;
use App\Model\TaskStatus;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Http\Request;
use App\Repositories\TaskRepository;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Task resource representation.
 *
 * @Resource("Tasks", uri="/api/v1/tasks")
 */
class TaskController extends BaseController
{
    /**
     * @var TaskRepository
     */
    public $taskRepository;

    /**
     * Construct method. Define repo of resource
     *
     * @param TaskRepository $taskRepository
     */
    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    /**
     * @return \Dingo\Api\Http\Response
     * @throws UpdateResourceFailedException
     */
    public function store($id)
    {
        if ($task = $this->taskRepository->store($id, Input::all(), $this->user)) {
            return $this->response->item($task, new BaseTransformer);
        } else {
            throw new UpdateResourceFailedException('Could not create task.', $this->taskRepository->errors());
        }
    }

    /**
     * @param integer $id
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function update($id, Request $request)
    {
        /** @var Task $task */
        $task = $this->taskRepository->getModelByID($id);
        $allowedData = $task->isApproved() ? $request->only(['title', 'description', 'due_date']) : $request->input();
        if ($task = $this->taskRepository->update($id, $allowedData, $this->user)) {
            return $this->response->item($task, new BaseTransformer);
        } else {
            throw new UpdateResourceFailedException('Could not update task.', $this->taskRepository->errors());
        }
    }

    /**
     * @param integer $id
     * @return \Dingo\Api\Http\Response
     */
    public function show($id)
    {
        return $this->response->item($this->taskRepository->getModelByID($id), new BaseTransformer);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function index($id, Request $request)
    {
        return $this->response->paginator($this->taskRepository->listTasksByUser($id, $request, $this->user), new BaseTransformer);
    }

    /**
     * @param integer $id
     */
    public function destroy($id)
    {
        if ($this->taskRepository->destroy($id, $this->user)) {
            return $this->success();
        } else {
            throw new DeleteResourceFailedException('Could not delete task.', $this->taskRepository->errors());
        }
    }

    /**
     * @param int $id
     */
    public function approve($id)
    {
        /** @var Task $task */
        $task = $this->taskRepository->getModelByID($id);
//        $userOrganization = $this->taskRepository->validateOrganizationManager($this->user, $task->organization_id);
//        if (\Gate::forUser($this->user)->denies(Ability::TASK_APPROVE, $task, )) {
//            throw new AccessDeniedHttpException('You have no permission.');
//        }
        $this->taskRepository->approve($task, $this->user);
        return $this->success();
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function listTaskStatuses($id, Request $request)
    {
        return $this->response->collection($this->taskRepository->listStatusesByUser($id, $request, $this->user), new BaseTransformer);
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function listKeyResults(Request $request)
    {
        return $this->response->collection($this->taskRepository->listKeyResultsByUser($request, $this->user), new BaseTransformer);
    }
}
