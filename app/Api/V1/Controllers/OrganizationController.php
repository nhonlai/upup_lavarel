<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Transformers\BaseTransformer;
use App\Repositories\UserOrganizationRepository;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;
use App\Repositories\OrganizationRepository;
use App\Repositories\InviteMemberRepository;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Support\Facades\Input;

/**
 * Organization resource representation.
 *
 * @Resource("Organizations", uri="/api/v1/organizations")
 */
class OrganizationController extends BaseController
{
    /**
     * @var OrganizationRepository
     */
    public $organizationRepository;

    /**
     * @var InviteMemberRepository
     */
    public $inviteMemberRepository;
    public $userOrganizationRepository;

    /**
     * Construct method. Define repo of resource
     *
     * @param OrganizationRepository $organizationRepository
     * @param UserRepository $userRepository
     */
    public function __construct(OrganizationRepository $organizationRepository, InviteMemberRepository $inviteMemberRepository, UserOrganizationRepository $userOrganizationRepository)
    {
        $this->organizationRepository = $organizationRepository;
        $this->inviteMemberRepository = $inviteMemberRepository;
        $this->userOrganizationRepository = $userOrganizationRepository;
    }

    /**
     * @return \Dingo\Api\Http\Response
     * @throws UpdateResourceFailedException
     */
    public function store()
    {
        if ($organization = $this->organizationRepository->store(Input::all(), $this->user)) {
            return $this->response->item($organization, new BaseTransformer);
        } else {
            throw new UpdateResourceFailedException('Could not create organization.', $this->organizationRepository->errors());
        }
    }

    public function verifyName(Request $request)
    {
//        $rules = $this->organizationRepository->getRules();
//        $this->validate($request, $rules);
//
//        $isExisted = $this->organizationRepository->verifyName($request->input('name'));
//        return $this->success(['isExisted' => $isExisted], 'Verify organization name successfully');
        $this->organizationRepository->copyDefaultBadges(2);
    }

    /**
     * @param integer $id
     * @param Request $request
     * @return mixed
     */
    public function getMembers($id, Request $request)
    {
        return $this->response->paginator($this->userOrganizationRepository->getOrganizationMembers($request, $id, $this->user), new BaseTransformer);
    }

    /**
     * @param integer $id
     * @param Request $request
     * @return mixed
     * @throws StoreResourceFailedException
     */
    public function invite($id, Request $request)
    {
        if (!$this->inviteMemberRepository->inviteOrganizationMember($id, $request->input(), $this->user)) {
            throw new StoreResourceFailedException('Could not send invite', $this->inviteMemberRepository->errors());
        }
        return $this->success(null, 'Sent invitation email.');
    }
}
