<?php

namespace App\Repositories;

use App\Model\KeyResult;
use App\Model\Organization;
use App\Model\Objective;
use App\Model\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ObjectiveRepository extends BaseRepository
{
    /**
     * @return Objective
     */
    public function getModel()
    {
        return new Objective();
    }

    /**
     * @return array
     */
    public function getSearchFields()
    {
        return [
            'title',
        ];
    }

    /**
     * @param string|null $type
     * @param array $params
     *
     * @return array
     */
    public function getRules($type = null, array $params = []) {
        return [
            'title'      => "required|max:255",
            'due_date'      => "required|date_format:Y-m-d H:i:s",
        ];
    }

    /**
     * Store model to database
     *
     * @param integer $organizationId
     * @param array $input
     * @param User $user
     * @return Objective|bool
     */
    public function store($organizationId, array $input, User $user)
    {
        $userOrganization = $this->validateOrganizationManager($user, $organizationId);
        if (!$this->isValid($input, $this->getRules())) {
            return false;
        }
        $objective = new Objective();
        $objective->fill($input);
        $objective->created_by = $user->id;
        $objective->organization_id = $userOrganization->organization_id;
        $objective->save();

        return $objective->fresh();
    }

    public function update($objectiveId, array $input, User $user)
    {
        if (!$objective = $this->getModelByID($objectiveId)) {
            return false;
        }
        $this->validateOrganizationManager($user, $objective->organization_id);
        if (!$this->isValid($input, $this->getRules())) {
            return false;
        }
        $objective->fill($input);
        $objective->save();

        return $objective->fresh();
    }

    /**
     * Get model by given id.
     *
     * @param integer $id
     * @param User $user
     * @return Objective|bool
     */
    public function get($id, User $user)
    {
        /* @var Objective $objective */
        if (!$objective = $this->getModelByID($id)) {
            return false;
        }
        $this->validateOrganizationMember($user, $objective->organization_id);

        return $objective;
    }

    /**
     * @param integer $organizationId
     * @param Request $request
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function listObjectives($organizationId, $request)
    {
        if (!Organization::find($organizationId)) {
            throw new NotFoundHttpException();
        }
        return $this->getList($request, Objective::whereOrganizationId($organizationId));
    }

    /**
     * @param $organizationId
     * @return int
     */
    public function calculateTotalCompletedPercent($organizationId)
    {
        return (int) Objective::whereOrganizationId($organizationId)
            ->avg('completed_percent');
    }

    /**
     * @param $objectiveId
     * @return int
     */
    public function getTotalWeight($objectiveId)
    {
        return (int) KeyResult::whereObjectiveId($objectiveId)->sum('weight');
    }

    /**
     * @param $objectiveId
     * @return float
     */
    public function getCompletedWeight($objectiveId)
    {
        return KeyResult::whereObjectiveId($objectiveId)
            ->selectRaw('SUM(weight * completed_percent / 100) as sum_value')->value('sum_value');
    }

    /**
     * @param $objectiveId
     * @return int
     */
    public function calculateCompletedPercent($objectiveId)
    {
        $total = $this->getTotalWeight($objectiveId);
        if (!$total) {
            return 0;
        }
        $completed = $this->getCompletedWeight($objectiveId);
        return (int) (100 * $completed / $total);
    }
}
