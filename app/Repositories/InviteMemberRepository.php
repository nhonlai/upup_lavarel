<?php


namespace App\Repositories;

use App\Constants\UserRole;
use App\Model\InviteMember;
use App\Model\Organization;
use App\Model\Team;
use App\Model\User;
use App\Model\UserOrganization;
use Illuminate\Mail\Message;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class InviteMemberRepository
 *
 * @package App\Repositories
 */
class InviteMemberRepository extends BaseRepository
{
    const SUBMIT  = 'submit';
    const INVITE  = 'invite';

    /**
     * @var UserRepository
     */
    protected $userRepository;
    protected $organizationRepository;

    /**
     * PasswordResetRepository constructor.
     *
     * @param UserRepository $userRepository
     * @param OrganizationRepository $organizationRepository
     */
    public function __construct(UserRepository $userRepository, OrganizationRepository $organizationRepository)
    {
        $this->userRepository = $userRepository;
        $this->organizationRepository = $organizationRepository;
    }

    /**
     * @return InviteMember
     */
    public function getModel()
    {
        return new InviteMember();
    }

    /**
     * @return array
     */
    public function getSearchFields()
    {
        return [];
    }

    /**
     * @param string|null  $type
     * @param array $params
     *
     * @return array
     */
    public function getRules($type = null, array $params = [])
    {
        $rules = [
            static::SUBMIT  => [
                'email' => "required|max:255|email",
                'organization_id' => "required"
            ],
            static::INVITE  => [
                'email' => "required|max:255|email|unique:users,email",
            ],
        ];
        return isset($rules[$type]) ? $rules[$type] : [];
    }

    /**
     * @param array $input
     *
     * @return bool
     */
    public function submitInvite(array $input)
    {
        if (!$this->isValid($input, $this->getRules(static::SUBMIT))) {
            return false;
        }
        $organization = $this->organizationRepository->getModel()->whereId($input['organization_id'])->first();
        if (!$organization) {
            $this->errors['organization_id'] = 'Organization with such id is not found.';
            return false;
        }
        if ($invite = $this->store($input['email'], $organization->id)) {
            $this->sendInviteEmail($input['email'], $organization, $invite);
        }
        return true;
    }

    /**
     * @param integer $teamId
     * @return Team
     */
    protected function getTeam($teamId)
    {
        /* @var Team $team */
        if (!$team = Team::find(($teamId))) {
            throw new NotFoundHttpException('Team with such id is not found.');
        }
        return $team;
    }
    /**
     * @param integer $organizationId
     * @return Organization
     */
    protected function getOrganization($id)
    {
        /* @var Team $team */
        if (!$organization = Organization::find(($id))) {
            throw new NotFoundHttpException('Organization with such id is not found.');
        }
        return $organization;
    }

    /**
     * @param integer $organizationId
     * @param array $input
     * @param User $user
     *
     * @return string
     */
    public function inviteOrganizationMember($organizationId, array $input, User $user)
    {
        if (!$organization = Organization::find($organizationId)) {
            throw new NotFoundHttpException();
        }
        $this->validateOrganizationManager($user, $organizationId);
        if (!$this->validateEmails('emails', $input)) {
            return false;
        }
        $emails = explode(',', $input['emails']);
        if (!$this->validateExistedMember($emails, $organizationId)) {
            return false;
        }
        foreach ($emails as $email) {
            $email = strtolower(trim($email));
            if ($invite = $this->store($email, $organizationId)) {
                $this->sendInviteEmail($email, $organization, $invite);
            }
        }
        return true;
    }
    /**
     * @param integer $teamId
     * @param array $input
     * @param User $user
     *
     * @return bool
     */
    public function inviteTeamMember($teamId, array $input, User $user)
    {
        $team = $this->getTeam($teamId);
        $organizationId = $team->organization_id;

        if (!$organization = Organization::find($organizationId)) {
            throw new NotFoundHttpException();
        }
        $this->validateOrganizationManager($user, $organizationId);
        if (!$this->validateEmails('emails', $input)) {
            return false;
        }
        $emails = explode(',', $input['emails']);
        if (!$this->validateExistedTeamMember($emails, $organizationId)) {
            return false;
        }
        foreach ($emails as $email) {
            $email = strtolower(trim($email));
            $userOrganization = UserOrganization::join('users', 'user_organization.user_id', '=', 'users.id')
                ->where('users.email', $email)
                ->select('user_organization.*')
                ->first();
            if ($userOrganization) {
                $userOrganization->team_id = $teamId;
                $userOrganization->save();
            } else {
                if ($invite = $this->store($email, $organizationId, $teamId)) {
                    $this->sendInviteEmail($email, $team, $invite);
                }
            }
        }
        return true;
    }

    /**
     * @param string $field
     * @param array $input
     * @param bool $isRequired
     *
     * @return bool
     */
    protected function validateEmails($field, $input, $isRequired = true)
    {
        // validate required rule
        if ($isRequired && empty($input[$field])) {
            if (!$this->isValid($input, [$field => 'required'])) {
                return false;
            }
        }
        if (isset($input[$field])) {
            $value = $input[$field];
            $value = str_replace(' ','',$value);
            $array = explode(',', $value);
            // validate email rule for each item
            foreach($array as $i => $email) {
                $messages = ["$field.email" => "The $field.{$i} must be a valid email address."];
                if (!$this->isValid([$field => ($email ? $email : 'wrong email')], [$field => 'email'], $messages)) {
                    return false;
                }
            }
        }
        return true;
    }

    protected function validateExistedMember($emails, $organizationId) {
        $valid = true;
        foreach ($emails as $email) {
            $email = strtolower(trim($email));
            $user = User::join('user_organization', 'user_organization.user_id', '=', 'users.id')
                ->where('users.email', $email)->first();
            /*
             * temporary remove check organization because one user can join one organization only atm
             */
//                ->where('user_organization.organization_id', $organizationId)->first();
            if ($user) {
                $this->errors["{$email}"] = "This user is already in this domain.";
                $valid = false;
            }
        }
        return $valid;
    }
    protected function validateExistedTeamMember($emails, $organizationId) {
        $valid = true;
        foreach ($emails as $email) {
            $email = strtolower(trim($email));
            $user = User::join('user_organization', 'user_organization.user_id', '=', 'users.id')
                ->where('users.email', $email)->first();
            /*
             * temporary remove check organization because one user can join one organization only atm
             */
//                ->where('user_organization.organization_id', $organizationId)->first();
            if ($user) {
                if ($user->team_id) {
                    $this->errors["{$email}"] = "This user is already in this domain.";
                    $valid = false;
                }
            }
        }
        return $valid;
    }

    /**
     * @param $token
     *
     * @return PasswordReset
     */
    public function getByToken($token)
    {
        return $this->getModel()->whereToken($token)->orderBy('created_at', 'desc')->first();
    }

    /**
     * @param $token
     *
     * @return User
     */
    public function getUserByToken($token)
    {
        $user = null;
        $reset = $this->getByToken($token);
        if ($reset) {
            $user = $this->userRepo->getByEmail($reset->email);
        }
        if (!$user) {
            $this->errors['token'] = 'The password reset token is invalid or expired.';
        }
        return $user;
    }

    /**
     *
     * @param string        $email
     * @param mixed  $object
     * @param InviteMember  $invite
     */
    public function sendInviteEmail($email, $object, InviteMember $invite)
    {
        $data = [
            'team' => $object,
            'link' => config('app.invite_url') . '?token=' . $invite->token,
        ];
        \Mail::send('emails.inviteJoinTeam', $data, function(Message $m) use ($email) {
            $m->to($email)->subject('Invite Join Team');
        });
    }

    /**
     * Create/update InviteMember model
     *
     * @param $email
     * @param $teamId
     *
     * @return InviteMember|null
     */
    public function store($email, $organizationId, $teamId = null)
    {
        $email = strtolower(trim($email));
        $invite = InviteMember::getByEmailAndTeamId($email, $organizationId, $teamId);
        $invite->generateToken($email, $organizationId, $teamId);
        if (!$invite->save()) {
            $invite = null;
        }
        return $invite;
    }

    /**
     * Delete PasswordReset
     *
     * @param $email
     *
     * @return bool|null
     * @throws \Exception
     */
    public function destroy($email)
    {
        $reset = $this->getModel()->whereEmail($email)->first();
        if ($reset) {
            return $reset->delete();
        }
        return false;
    }
}
