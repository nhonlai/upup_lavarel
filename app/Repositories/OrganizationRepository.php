<?php

namespace App\Repositories;

use App\Constants\UserRole;
use App\Model\BadgeReward;
use App\Model\Organization;
use App\Model\User;
use App\Model\UserOrganization;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class OrganizationRepository extends BaseRepository
{
    /**
     * @return Organization
     */
    public function getModel()
    {
        return new Organization();
    }

    /**
     * @return array
     */
    public function getSearchFields()
    {
        return [
            'first_name',
            'last_name',
            'email'
        ];
    }

    /**
     * @param string|null $type
     * @param array $params
     *
     * @return array
     */
    public function getRules($type = null, array $params = []) {
        return [
            'name'      => "required|max:255|unique:organizations,name",
        ];
    }

    /**
     * Store model to database
     *
     * @param array $input
     * @param User $user
     * @return User|bool
     */
    public function store(array $input, User $user)
    {
        if (!$user) {
            throw new AccessDeniedHttpException;
        }
        if (!$this->isValid($input, $this->getRules())) {
            return false;
        }
        $organization = new Organization();
        $organization->fill($input);
        $organization->created_by = $user->id;
        if ($organization->save()) {
            $userOrganization = new UserOrganization();
            $userOrganization->user_id = $user->id;
            $userOrganization->organization_id = $organization->id;
            $userOrganization->role_id = $user->getRole(UserRole::FOUNDER)->id;
            $userOrganization->save();
            $this->copyDefaultBadges($organization->id);
        }
        return $organization;
    }

    public function verifyName($name)
    {
        $organization = $this->getModel()->whereName($name)->first();
        if (!$organization) {
            return false;
        }
        return true;
    }

    public function copyDefaultBadges($organizationId) {
        $defaultBadgeList = BadgeReward::whereOrganizationId(null)->get();
        foreach ($defaultBadgeList as $defaultBadge) {
            $badge = new BadgeReward();
            $badge->fill($defaultBadge->attributesToArray());
            $badge->organization_id = $organizationId;
            $badge->save();
        }
    }
}
