<?php

namespace App\Repositories;

use App\Model\Organization;
use App\Model\TaskStatus;
use App\Model\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SectionRepository extends BaseRepository
{
    /**
     * @return TaskStatus
     */
    public function getModel()
    {
        return new TaskStatus();
    }

    /**
     * @return array
     */
    public function getSearchFields()
    {
        return [
            'title',
        ];
    }

    /**
     * @param string|null $type
     * @param array $params
     *
     * @return array
     */
    public function getRules($type = null, array $params = []) {
        return [
            'name'      => "required|max:40",
            'description'      => "max:255",
            'icon'      => "max:255",
            'color'      => "max:255",
        ];
    }

    /**
     * Store model to database
     *
     * @param integer $id
     * @param array $input
     * @param User $user
     * @return TaskStatus|bool
     */
    public function store($id, array $input, User $user)
    {
        /* @var TaskStatus $taskStatus */
        if (!$taskStatus = $this->getModelByID($id)) {
            return false;
        }

        $this->validateOrganization($taskStatus, $user);

        if (!$this->isValid($input, $this->getRules(null, ['taskStatus' => $taskStatus, 'user' => $user]))) {
            return false;
        }
        $taskStatus->fill($input);
        $taskStatus->is_system = 0;
        $taskStatus->organization_id = $user->organization_id;
        $taskStatus->save();
        return $taskStatus->fresh();
    }



    /**
     * Get model by given id.
     *
     * @param integer $id
     * @param User $user
     * @return TaskStatus|bool
     */
    public function get($id, User $user)
    {
        /* @var TaskStatus $taskStatus */
        if (!$taskStatus = $this->getModelByID($id)) {
            return false;
        }

        $this->validateOrganization($taskStatus, $user);

        return $taskStatus;
    }

    /**
     * @param integer $organizationId
     * @param Request $request
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function listSections($organizationId, $request)
    {
        /* @var Organization $organization */
        if (empty($organizationId) || !$organization = Organization::find($organizationId)) {
            throw new NotFoundHttpException();
        }
        $model = TaskStatus::whereOrganizationId($organizationId)->orWhere('is_system', true);

        return $this->getList($request, $model);
    }
}
