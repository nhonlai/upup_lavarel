<?php


namespace App\Repositories;


use App\Model\Device;
use App\Model\User;

class DeviceRepository extends BaseRepository
{

    /**
     * @return Device
     */
    public function getModel()
    {
        return new Device();
    }

    /**
     * @return array
     */
    public function getSearchFields()
    {
        return [];
    }

    /**
     * @param string|null $type
     * @param array $params
     *
     * @return array
     */
    public function getRules($type = null, array $params = [])
    {
        return [
            'device_type' => 'in:android,ios,web',
        ];
    }

    /**
     * @param string    $token
     * @param string    $type
     * @param User      $user
     *
     * @return Device|null
     */
    public function store($token, $type, User $user)
    {
        if (empty($token) || empty($type)) return null;
        $model  = $this->getModel();
        $device = $model->getByToken($token);
        if (!$device) {
            $device = $model;
        }
        $device->user_id = $user->id;
        $device->device_token = $token;
        $device->setDeviceType($type);
        $device->save();

        return $device;
    }

    /**
     * @param $token
     * @param $type
     *
     * @return $this
     */
    public function destroy($token, $type)
    {
        if (empty($token) || empty($type)) return null;
        $model  = $this->getModel();
        $device = $model->getByToken($token);
        if ($device) {
            $device->delete();
        }
        return $this;
    }

    /**
     * Get device tokens by given user id.
     *
     * @param int $userID
     * @param bool $groupByType
     * @return array
     * @access public
     */
    public function getDevicesForSendingPush($userID, $groupByType = false)
    {
        $listDevices = Device::where('user_id', '=', $userID)->get();
        $listByTypes = [];
        foreach ($listDevices as $device) {
            /* @var Device $device */
            if ($groupByType) {
                $listByTypes[$device->type_id][] = $device->device_token;
            } else {
                $listByTypes[] = $device->device_token;
            }
        }
        return $listByTypes;
    }
}
