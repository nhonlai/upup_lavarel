<?php

namespace App\Repositories;

use App\Jobs\SendBadgeRewardNotification;
use App\Model\KeyResult;
use App\Model\Task;
use App\Model\TaskStatus;
use App\Model\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TaskRepository extends BaseRepository
{
    /**
     * @return Task
     */
    public function getModel()
    {
        return new Task();
    }

    /**
     * @return array
     */
    public function getSearchFields()
    {
        return [
            'name',
        ];
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return [

        ];
    }

    /**
     * @param string|null $type
     * @param array $params
     *
     * @return array
     */
    public function getRules($type = null, array $params = []) {
        return [
            'title'      => "required|max:255",
            'description'     => "string|max:255",
            'status_id' => "exists:task_statuses,id|not_in:" . TaskStatus::APPROVED,
            'key_result_id' => "exists:key_results,id",
            'badge_reward_id' => "exists:badge_rewards,id",
            'assignee_id' => "exists:users,id",
            'due_date'      => "required|date_format:Y-m-d H:i:s",
            "required_level" => "int|min:1"
        ];
    }

    /**
     * Store model to database
     *
     * @param integer $organizationId
     * @param array $input
     * @param User $user
     * @return Task|bool
     */
    public function store($organizationId, array $input, User $user)
    {
        $userOrganization = $this->validateOrganizationManager($user, $organizationId);
        if (!$this->isValid($input, $this->getRules())) {
            return false;
        }
        $task = new Task();
        $task->fill($input);
        $task->created_by = $user->id;
        $task->organization_id = $userOrganization->organization_id;
        $task->save();

        return $task->fresh();
    }

    public function update($taskId, array $input, User $user)
    {
        if (!$task = $this->getModelByID($taskId)) {
            return false;
        }
        $this->validateOrganizationMember($user, $task->organization_id);
        if (!$this->isValid($input, $this->getRules())) {
            return false;
        }
        $task->fill($input);
        $task->save();

        return $task->fresh();
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function listTasksByUser($organizationId, $request, $user)
    {
        $this->validateOrganizationMember($user, $organizationId);
        return $this->getList($request, Task::whereOrganizationId($organizationId));
    }

    /**
     * @param $organizationId
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function listStatusesByUser($organizationId, $request, $user)
    {
        $this->validateOrganizationManager($user, $organizationId);

        return TaskStatus::whereOrganizationId($organizationId)
            ->orWhereNull('organization_id')
            ->orderBy('is_system', 'desc')->get();
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function listKeyResultsByUser($request, $user)
    {
        if (!$user->organization_id) {
            throw new AccessDeniedHttpException();
        }
        return KeyResult::whereOrganizationId($user->organization_id)
            ->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get model by given id.
     *
     * @param integer $id
     * @param User $user
     * @return bool
     */
    public function destroy($id, User $user)
    {
        if (!$task = $this->getModelByID($id)) {
            return false;
        }
        $this->validateOrganizationManager($user, $task->organization_id);
        $task->delete();

        return true;
    }

    /**
     * @param Task $task
     * @return bool
     */
    public function addBadgeRewardForMember(Task $task)
    {
        $assignee = $task->assignee;
        $badgeReward = $task->badgeReward;
        if ($assignee && $badgeReward) {
            $assignee->badges += $badgeReward->badges;
            $assignee->points += $badgeReward->points;
            $assignee->save();
            dispatch(new SendBadgeRewardNotification($task));
            return true;
        }
        return false;
    }

    /**
     * @param Task $task
     * @return bool
     */
    public function addBadgeRewardForManager(Task $task)
    {
        $manager = $task->creator;
        $badgeReward = $task->badgeReward;
        if ($manager && $badgeReward) {
            $manager->badges += $badgeReward->badges;
            $manager->points += $badgeReward->points;
            $manager->save();
            return true;
        }
        return false;
    }

    /**
     * @param Task $task
     * @param User $user
     */
    public function approve(Task $task, User $user)
    {
        $this->validateOrganizationManager($user, $task->organization_id);

        if ($task->status_id != TaskStatus::DONE) {
            throw new BadRequestHttpException('This task is not done yet.');
        }
        if ($task->status_id == TaskStatus::APPROVED) {
            throw new BadRequestHttpException('This task is already approved.');
        }
        if (!$task->badge_reward_id) {
            throw new BadRequestHttpException('Task is no badge reward.');
        }
        if (!$task->assignee_id) {
            throw new BadRequestHttpException('Task is no assignee.');
        }
        if (!$task->key_result_id) {
            throw new BadRequestHttpException('Task should be linked to a key result before approving.');
        }
        $task->status_id = TaskStatus::APPROVED;
        $task->save();
    }
}
