<?php

namespace App\Repositories;

use App\Model\Objective;
use App\Model\Organization;
use App\Model\KeyResult;
use App\Model\Task;
use App\Model\TaskStatus;
use App\Model\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class KeyResultRepository extends BaseRepository
{
    /**
     * @var ObjectiveRepository
     */
    public $objectiveRepository;

    /**
     * KeyResultRepository constructor.
     *
     * @param ObjectiveRepository $objectiveRepository
     */
    public function __construct(ObjectiveRepository $objectiveRepository)
    {
        $this->objectiveRepository = $objectiveRepository;
    }

    /**
     * @return KeyResult
     */
    public function getModel()
    {
        return new KeyResult();
    }

    /**
     * @return array
     */
    public function getSearchFields()
    {
        return [
            'title',
        ];
    }

    /**
     * @param string|null $type
     * @param array $params
     *
     * @return array
     */
    public function getRules($type = null, array $params = []) {
        /* @var KeyResult $keyResult */
        $keyResult = $params['keyResult'];
        /* @var User $user */
        $user = $params['user'];
        return [
            'title'      => "required|max:255",
            'due_date'      => "required|date_format:Y-m-d H:i:s",
            'start_value'   => "integer|min:0",
            'target_value'   => "required|integer|min:1|max:100",
            'weight'    => "required|integer|min:1",
        ];
    }

    /**
     * @param KeyResult $keyResult
     * @param Objective $objective
     * @return bool
     */
    public function validateObjective($keyResult, Objective $objective)
    {
        if ($keyResult->id && ($keyResult->objective_id != $objective->id || $keyResult->organization_id != $objective->organization_id)) {
            throw new AccessDeniedHttpException;
        }

        return true;
    }

    /**
     * Store model to database
     *
     * @param integer $id
     * @param array $input
     * @param Objective $objective
     * @param User $user
     * @return KeyResult|bool
     */
    public function store($id, array $input, Objective $objective, User $user)
    {
        /* @var KeyResult $keyResult */
        if (!$keyResult = $this->getModelByID($id)) {
            return false;
        }
        $this->validateOrganizationMember($user, $objective->organization_id);
        $this->validateObjective($keyResult, $objective);

        if (!$this->isValid($input, $this->getRules(null, ['keyResult' => $keyResult, 'user' => $user]))) {
            return false;
        }

        $keyResult->fill($input);

        // start_value must less than or equal to target_value
        if ($keyResult->start_value > $keyResult->target_value) {
            throw new BadRequestHttpException('start_value must less than or equal to target_value');
        }
        // due_date of key result must less than or equal due_date of objective
        if ($keyResult->due_date > $objective->due_date) {
            throw new BadRequestHttpException('due_date of key result must less than or equal due_date of objective');
        }
        // TODO: validate weight, KR-weight must <= Objective-Weight(100%) - SUM(other KR-weights)

        $keyResult->objective_id = $objective->id;
        $keyResult->created_by = $user->id;
        $keyResult->organization_id = $objective->organization_id;
        $keyResult->save();
        return $keyResult->fresh();
    }

    /**
     * Get model by given id.
     *
     * @param integer $id
     * @param User $user
     * @return KeyResult|bool
     */
    public function get($id, User $user)
    {
        /* @var KeyResult $keyResult */
        if (!$keyResult = $this->getModelByID($id)) {
            return false;
        }

        $this->validateOrganization($keyResult, $user);

        return $keyResult;
    }

    /**
     * @param integer $objectiveId
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function listKeyResults($objectiveId, $request, User $user)
    {
        $objective = $this->objectiveRepository->get($objectiveId, $user);

        return $this->getList($request, KeyResult::whereObjectiveId($objective->id));
    }

    /**
     * Get model by given id.
     *
     * @param integer $id
     * @return bool
     */
    public function destroy($id)
    {
        /* @var KeyResult $keyResult */
        if (!$keyResult = $this->getModelByID($id)) {
            return false;
        }

        $keyResult->delete();

        return true;
    }

    /**
     * @param $keyResultId
     * @param $statusId
     * @return int
     */
    public function getBadgesByStatus($keyResultId, $statusId = null)
    {
        $builder = Task::leftJoin('badge_rewards', 'badge_rewards.id', '=', 'tasks.badge_reward_id')
            ->where('tasks.key_result_id', $keyResultId);
        if ($statusId) {
            $builder->where('tasks.status_id', $statusId);
        }
        return (int) $builder->sum('badge_rewards.badges');
    }

    /**
     * @param $keyResultId
     * @return int
     */
    public function getTotalBadges($keyResultId)
    {
        return (int) $this->getBadgesByStatus($keyResultId);
    }

    /**
     * @param $keyResultId
     * @return int
     */
    public function getCompletedBadges($keyResultId)
    {
        return (int) $this->getBadgesByStatus($keyResultId, TaskStatus::APPROVED);
    }

    /**
     * @param $keyResultId
     * @return int
     */
    public function calculateCompletedPercent($keyResultId)
    {
        $total = $this->getTotalBadges($keyResultId);
        if (!$total) {
            return 0;
        }
        $completed = $this->getCompletedBadges($keyResultId);
        return (int) (100 * $completed / $total);
    }
}
