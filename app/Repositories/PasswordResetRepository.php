<?php


namespace App\Repositories;

use App\Model\PasswordReset;
use App\Model\User;
use Illuminate\Mail\Message;

/**
 * Class PasswordResetRepository
 *
 * @package App\Repositories
 */
class PasswordResetRepository extends BaseRepository
{
    const SUBMIT  = 'submit';
    const CONFIRM = 'confirm';
    const RESET  = 'reset';
    const UPDATE  = 'update';
    const PARAM_CURRENT_PASSWORD = 'current_password';
    const PARAM_NEW_PASSWORD = 'new_password';

    /**
     * @var UserRepository
     */
    protected $userRepo;

    /**
     * PasswordResetRepository constructor.
     *
     * @param UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * @return PasswordReset
     */
    public function getModel()
    {
        return new PasswordReset();
    }

    /**
     * @return array
     */
    public function getSearchFields()
    {
        return [];
    }

    /**
     * @param string|null  $type
     * @param array $params
     *
     * @return array
     */
    public function getRules($type = null, array $params = [])
    {
        $rules = [
            static::SUBMIT  => [
                'email' => "required|max:255|email",
            ],
            static::CONFIRM => [
                'token' => "required",
                'password' => 'required|min:5|max:50',
            ],
            static::RESET => [
                'token' => "required",
                'password' => 'required|min:5|max:50',
                'confirm_password' => 'same:password',
            ],
            static::UPDATE => [
                static::PARAM_CURRENT_PASSWORD => "required",
                static::PARAM_NEW_PASSWORD => 'required|min:5|max:50',
            ],
        ];
        return isset($rules[$type]) ? $rules[$type] : [];
    }

    /**
     * @param array $input
     *
     * @return bool
     */
    public function submitReset(array $input)
    {
        if (!$this->isValid($input, $this->getRules(static::SUBMIT))) {
            return false;
        }
        $user = $this->userRepo->getByEmail($input['email']);
        if (!$user) {
            $this->errors['email'] = 'User with such email is not found.';
            return false;
        }
        if ($reset = $this->store($user->email)) {
            $this->sendResetEmail($user, $reset);
        }
        return true;
    }

    /**
     *
     * @param array $input
     *
     * @return User
     */
    public function confirmReset(array $input)
    {
        if (!$this->isValid($input, $this->getRules(static::CONFIRM))) {
            return false;
        }
        $user = null;
        $reset = $this->getByToken($input['token']);
        if ($reset) {
            $user = $this->userRepo->getByEmail($reset->email);
            if ($user) {
                $user->password = $input['password'];
                $user->save();
                $reset->delete();
            } else {
                $this->errors['user'] = 'User not found';
            }
        } else {
            $this->errors['token'] = 'Invalid token';
        }

        return $user;
    }

    /**
     * Update password for given user.
     *
     * @param array $input
     * @param User $user
     * @return boolean
     */
    public function updatePassword(array $input, $user)
    {
        if (!$user || !$this->isValid($input, $this->getRules(static::UPDATE))) {
            return false;
        }
        if (!$user->passwordCheck($input[static::PARAM_CURRENT_PASSWORD])) {
            $this->errors[static::PARAM_CURRENT_PASSWORD] = 'Invalid current password.';
            return false;
        }
        $user->password = $input[static::PARAM_NEW_PASSWORD];
        $user->save();
        return true;
    }

    /**
     * @param array $input
     * @return bool
     */
    public function resetPassword(array $input)
    {
        $token = isset($input['token']) ? $input['token'] : null;
        $reset = $this->getByToken($token);
        if (!$user = $this->getUserByToken($token)) {
            return false;
        }
        if (!$this->isValid($input, $this->getRules(static::RESET))) {
            return false;
        }

        $user->password = $input['password'];
        $user->save();
        $reset->delete();

        return true;
    }

    /**
     * @param $token
     *
     * @return PasswordReset
     */
    public function getByToken($token)
    {
        return $this->getModel()->whereToken($token)->orderBy('created_at', 'desc')->first();
    }

    /**
     * @param $token
     *
     * @return User
     */
    public function getUserByToken($token)
    {
        $user = null;
        $reset = $this->getByToken($token);
        if ($reset) {
            $user = $this->userRepo->getByEmail($reset->email);
        }
        if (!$user) {
            $this->errors['token'] = 'The password reset token is invalid or expired.';
        }
        return $user;
    }

    /**
     *
     * @param User          $user
     * @param PasswordReset $reset
     */
    public function sendResetEmail(User $user, PasswordReset $reset)
    {
        $data = [
            'user' => $user,
            'link' => config('app.reset_password_url') . '?token=' . $reset->token,
        ];
        \Mail::send('emails.resetPassword', $data, function(Message $m) use ($user) {
            $m->to($user->email, $user->name)->subject('Password Reset Confirmation');
        });
    }

    /**
     * Create/update PasswordReset model
     *
     * @param $email
     *
     * @return PasswordReset|null
     */
    public function store($email)
    {
        $model = $this->getModel();
        /** @var PasswordReset $reset */
        $reset = $model->whereEmail($email)->firstOrNew([]);
        $reset->generateToken($email);
        if (!$reset->save()) {
            $reset = null;
        }
        return $reset;
    }

    /**
     * Delete PasswordReset
     *
     * @param $email
     *
     * @return bool|null
     * @throws \Exception
     */
    public function destroy($email)
    {
        $reset = $this->getModel()->whereEmail($email)->first();
        if ($reset) {
            return $reset->delete();
        }
        return false;
    }
}
