<?php

namespace App\Repositories;

use App\Constants\UserRole;
use App\Model\Organization;
use App\Model\Team;
use App\Model\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TeamRepository extends BaseRepository
{
    /**
     * @return Team
     */
    public function getModel()
    {
        return new Team();
    }

    /**
     * @return array
     */
    public function getSearchFields()
    {
        return [
            'name',
        ];
    }

    /**
     * @param string|null $type
     * @param array $params
     *
     * @return array
     */
    public function getRules($type = null, array $params = []) {
        return [
            'name'      => "required|max:255|unique:teams,name",
            'notes'     => "string|max:255",
        ];
    }

    /**
     * Store model to database
     *
     * @param integer $organizationId
     * @param array $input
     * @param User $user
     * @return Team|bool
     */
    public function store($organizationId, array $input, User $user)
    {
        $userOrganization = $this->validateOrganizationManager($user, $organizationId);
        if (!$this->isValid($input, $this->getRules())) {
            return false;
        }
        $team = new Team();
        $team->fill($input);
        $team->created_by = $user->id;
        $team->organization_id = $userOrganization->organization_id;
        $team->save();

        return $team->fresh();
    }

    public function update($teamId, array $input, User $user)
    {
        if (!$team = $this->getModelByID($teamId)) {
            return false;
        }
        $this->validateOrganizationManager($user, $team->organization_id);
        if (!$this->isValid($input, $this->getRules())) {
            return false;
        }
        $team->fill($input);
        $team->save();

        return $team->fresh();
    }

    /**
     * @param $organization_id
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function listTeamsByUser($organizationId, $request, $user)
    {
        $this->validateOrganizationMember($user, $organizationId);

        return $this->getList($request, Team::whereOrganizationId($organizationId));
    }

    /**
     * Get model by given id.
     *
     * @param integer $id
     * @param User $user
     * @return bool
     */
    public function destroy($id, User $user)
    {
        if (!$team = $this->getModelByID($id)) {
            return false;
        }
        $this->validateOrganizationManager($user, $team->organization_id);

        $team->delete();

        return true;
    }

    public function getTeamMembers($request, $teamId, $user)
    {
        if (!$team = Team::find($teamId)) {
            throw new NotFoundHttpException();
        }
        $organizationId = $team->organization_id;
        if (!Organization::find($organizationId)) {
            throw new NotFoundHttpException();
        }
        $this->validateOrganizationMember($user, $organizationId);
        $model = User::getModel()
            ->join('user_organization', 'users.id', '=', 'user_organization.user_id')
            ->where('user_organization.organization_id', $organizationId)
            ->where('user_organization.team_id', $teamId)
            ->where('user_organization.role_id', $user->getRole(UserRole::MEMBER)->id)
            ->select('users.*');

        return $this->getList($request, $model);
    }
}
