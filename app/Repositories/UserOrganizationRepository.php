<?php


namespace App\Repositories;

use App\Constants\UserRole;
use App\Model\User;
use App\Model\UserOrganization;
use App\Model\Organization;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class InviteMemberRepository
 *
 * @package App\Repositories
 */
class UserOrganizationRepository extends BaseRepository
{
    /**
     * @return UserOrganization
     */
    public function getModel()
    {
        return new UserOrganization();
    }

    function getSearchFields()
    {
        return [];
    }

    public function getRules($type = null, array $params = [])
    {
        return [
//            'user_id' => "exists:users,id",
//            'organization_id' => "exists:organizations,id",
        ];
    }

    public function store(array $input)
    {
        $userTeam = new UserOrganization();
        $userTeam->fill($input);
        if (!$userTeam->save()) {
            $userTeam = null;
        }
        return $userTeam;
    }

    public function getOrganizationMembers($request, $organizationId, $user)
    {
        if (!Organization::find($organizationId)) {
            throw new NotFoundHttpException();
        }
        $this->validateOrganizationMember($user, $organizationId);
        $model = User::getModel()
            ->join('user_organization', 'users.id', '=', 'user_organization.user_id')
            ->where('user_organization.organization_id', $organizationId)
            ->where('user_organization.role_id', $user->getRole(UserRole::MEMBER)->id)
            ->select('users.*');

        return $this->getList($request, $model);
    }

    public function getByUserId($userId)
    {
        return $this->getModel()->whereUserId($userId)->first();
    }
}
