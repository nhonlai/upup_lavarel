<?php

namespace App\Repositories;

use App\Constants\UserRole;
use App\Extensions\Traits\SearchTrait;
use App\Model\User;
use App\Model\UserOrganization;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\MessageBag;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract class BaseRepository
{
    use SearchTrait;

    /**
     * @var MessageBag
     */
    protected $errors;


    /**
     * @var array
     */
    protected $searchebleFields = [];


    /**
     * Execute a Closure within a transaction.
     *
     * @param Closure $callback
     * @return mixed
     * @throws Throwable
     * @access public
     */
    public function transaction(\Closure $callback)
    {
        return DB::transaction($callback);
    }


    /**
     * @return \Illuminate\Support\MessageBag
     */
    public function errors()
    {
        return $this->errors;
    }

    /**
     * Added error to message bag. If bag not exists then it will be created.
     *
     * @param $key
     * @param $error
     */
    protected function addError($key, $error)
    {
        if (!$this->errors) {
            $this->errors = new MessageBag();
        }
        $this->errors->add($key, $error);
    }


    /**
     * Get visible fields from model
     *
     */
    public function getVisibleFields()
    {
        $model = $this->getModel();
        return $model->getVisible();
    }


    /**
     * Get new model of model by id (used for store method)
     *
     * @param $id
     *
     * @return mixed
     */
    public function getModelByID($id = null)
    {
        $model = $this->getModel();
        if (!empty($id)) {
            $model = $model->find($id);
        }
        if (!$model) {
            throw new NotFoundHttpException;
        }
        return $model;
    }

    /**
     * Validate model
     * If validation fails $errors property will be filled.
     *
     * @param array $input
     * @param array $rules
     * @param array $messages
     *
     * @return bool
     */
    public function isValid(array $input, array $rules, $messages = array())
    {
        $validation = \Validator::make($input, $rules, $messages);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();
        return false;
    }


    /**
     * Get model of repository
     *
     * @return Model
     */
    abstract function getModel();


    /**
     * Get list of searchable fields
     *
     */
    abstract function getSearchFields();

    /**
     * Get validation rules for model
     *
     * @param string|null $type
     * @param array $params
     *
     * @return mixed
     */
    abstract public function getRules($type = null, array $params = []);

    /**
     * @param $user
     * @param $organizationId
     * @return UserOrganization
     */
    public function validateOrganizationMember($user, $organizationId)
    {
        $userOrganization = UserOrganization::where('user_id', $user->id)->where('organization_id', $organizationId)->first();
        if (!$userOrganization) {
            throw new AccessDeniedHttpException;
        }
        return $userOrganization;
    }
    /**
     * @param $user
     * @param $organizationId
     * @return UserOrganization
     */
    public function validateOrganizationManager($user, $organizationId)
    {
        $userOrganization = UserOrganization::where('user_id', $user->id)
            ->where('organization_id', $organizationId)
            ->where('role_id', $user->getRole(UserRole::FOUNDER)->id)
            ->orWhere('role_id', $user->getRole(UserRole::MANAGER)->id)
            ->first();
        if (!$userOrganization) {
            throw new AccessDeniedHttpException;
        }
        return $userOrganization;
    }
}
