<?php

namespace App\Repositories;

use App\Model\Organization;
use App\Model\BadgeReward;
use App\Model\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BadgeRewardRepository extends BaseRepository
{
    /**
     * @return BadgeReward
     */
    public function getModel()
    {
        return new BadgeReward();
    }

    /**
     * @return array
     */
    public function getSearchFields()
    {
        return [
            'name',
        ];
    }

    /**
     * @param string|null $type
     * @param array $params
     *
     * @return array
     */
    public function getRules($type = null, array $params = []) {
        /* @var BadgeReward $badgeReward */
        $badgeReward = $params['badgeReward'];
        /* @var User $user */
        $user = $params['user'];
        return [
            'title'      => "required|max:255",
            'description'     => "string|max:255",
            'badge_reward_id' => "exists:badge_rewards,id",
        ];
    }

    /**
     * Store model to database
     *
     * @param integer $id
     * @param array $input
     * @param User $user
     * @return BadgeReward|bool
     */
    public function store($id, array $input, User $user)
    {
        /* @var BadgeReward $badgeReward */
        if (!$badgeReward = $this->getModelByID($id)) {
            return false;
        }

        $this->validateOrganizationManager($user, $badgeReward->organization_id);

        if (!$this->isValid($input, $this->getRules(null, ['badgeReward' => $badgeReward, 'user' => $user]))) {
            return false;
        }
        $badgeReward->fill($input);
        $badgeReward->save();
        return $badgeReward->fresh();
    }

    /**
     * @param integer $organizationId
     * @param User $user
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function listBadgeRewardsByOrganization($organizationId, $user)
    {
        $this->validateOrganizationMember($user, $organizationId);

        return BadgeReward::whereOrganizationId($organizationId)->get();
    }

    /**
     * Get model by given id.
     *
     * @param integer $id
     * @param User $user
     * @return bool
     */
    public function destroy($id, User $user)
    {
        if (!$modelObject = $this->getModelByID($id)) {
            return false;
        }

        $this->validateOrganization($modelObject, $user);

        $modelObject->delete();

        return true;
    }
}
