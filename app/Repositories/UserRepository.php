<?php

namespace App\Repositories;

use App\Api\V1\Exceptions\ModelException;
use App\Constants\UserRole;
use App\Model\InviteMember;
use App\Model\Organization;
use App\Model\User;
use App\Model\UserStatus;
use App\Model\UserOrganization;
use App\Services\SocialService\SocialProfile;
use App\Services\SocialService\SocialTypeInterface;
use Dingo\Api\Exception\InternalHttpException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Dingo\Api\Exception\ResourceException;
use Tymon\JWTAuth\Providers\Auth\AuthInterface;

class UserRepository extends BaseRepository
{
    /**
     * @var \Tymon\JWTAuth\Providers\Auth\AuthInterface
     */
    protected $auth;

    /**
     * @param \Tymon\JWTAuth\Providers\Auth\AuthInterface $auth
     */
    public function __construct(AuthInterface $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @return User
     */
    public function getModel()
    {
        return new User();
    }

    /**
     * @return array
     */
    public function getSearchFields()
    {
        return [
            'first_name',
            'last_name',
            'email'
        ];
    }

    /**
     * @param string|null $type
     * @param array $params
     *
     * @return array
     */
    public function getRules($type = null, array $params = []) {
        $rules = [];
        switch($type) {
            case 'code_request':
                $rules = [
                    'email' => 'required|max:255|email',
                ];
                break;
            case 'code_verify':
                $rules = [
                    'email' => 'required|max:255|email',
                    'confirmationCode' => 'required',
                ];
                break;
            case 'auth':
                $rules = [
                    'email' => 'required|email',
                    'password' => 'required|min:6|max:50',
                ];
                break;
            case 'store':
                $user = $params['user'];
                $rules = [
                    'email'          => "required|max:255|email|unique:users,email,{$user->id},id",
                    'first_name'     => "required|max:255",
                    'last_name'      => "required|max:255",
                    'password'       => "min:6|max:50",
                ];
                break;
            case 'create_member':
                $rules = [
                    'token' => 'required',
                    'email'          => "required|max:255|email",
                    'first_name'     => "required|max:255",
                    'last_name'      => "required|max:255",
                    'password'       => "required|min:6|max:50",
                ];
                break;
            case 'social_login':
                $rules = [
                    'social_type' => 'required|in:' . implode(',', $this->getSocialTypes()),
                    'social_token' => "required",
                ];
                break;
        }
        return $rules;
    }

    /**
     * @return array
     */
    protected function getSocialTypes()
    {
        return [\App\Services\SocialService\SocialType::FB_TYPE, \App\Services\SocialService\SocialType::LINKED_IN_TYPE];
    }

    /**
     * Validate input and login user on success.
     *
     * @param array $credentials
     *
     * @return Token if login succeed else null.
     */
    public function JWTAuth(array $credentials)
    {
        try {
            if (!$token = \JWTAuth::attempt($credentials)) {
                throw new ModelException(401, 'Invalid email or password');
            }
        } catch (JWTException $e) {
            throw new ModelException(500, 'Could not create token');
        }
        return $token;
    }

    /**
     * Authenticate by user id.
     *
     * @param integer $userId
     * @return string Token if login succeed else null.
     */
    public function authenticateById($userId)
    {
        try {
            if (! $this->auth->byId($userId)) {
                return false;
            }
            $token = \JWTAuth::fromUser($this->auth->user());
        } catch (JWTException $e) {
            throw new ModelException(500, 'Could not create token');
        }
        return $token;
    }

    /**
     * Get user from token
     *
     * @param $token
     * @return User|null
     */
    public function getByToken($token)
    {
        return \JWTAuth::toUser($token);
    }

    /**
     * Get user by email.
     *
     * @param $email
     * @return User|null
     */
    public function getByEmail($email)
    {
        return $this->getModel()->whereEmail($email)->first();
    }

    /**
     * Request confirmation code by `email`.
     *
     * @param string $email
     * @return string
     */
    public function requestConfirmationCode($email)
    {
        $user = $this->getByEmail($email);
        if (!$user) {
            $user = new User();
            $user->email = strtolower($email);
            $user->status_id = UserStatus::PENDING;
        }
        switch ($user->status_id) {
            case UserStatus::PENDING:
                $user->confirmation_code = mt_rand(100000, 999999);
                $user->save();
                $this->sendConfirmationCodeEmail($user);
                return $user->confirmation_code;
            case UserStatus::VERIFIED:
                $user->confirmation_code = mt_rand(100000, 999999);
                $user->status_id = UserStatus::PENDING;
                $user->save();
                $this->sendConfirmationCodeEmail($user);
                return $user->confirmation_code;
            default:
                $this->addError('email', 'This email was taken.');
                return null;
        }
    }

    /**
     * @param User          $user
     */
    public function sendConfirmationCodeEmail(User $user)
    {
        $data = [
            'code' => $user->confirmation_code
        ];

        \Mail::send('emails.request-confirmation-code', $data, function(Message $m) use ($user) {
            $m->to($user->email)->subject('Confirmation Code');
        });
    }

    /**
     * Verify confirmation code by `email` and `code`.
     *
     * @param string $email
     * @param string $code
     * @return boolean
     */
    public function verifyConfirmationCode($email, $code)
    {
        $user = $this->getByEmail($email);
        if (!$user) {
            $this->addError('email', 'The email is invalid.');
            return false;
        }
        if ($user->status_id != UserStatus::PENDING) {
            $this->addError('email', 'This email is already verified.');
            return false;
        }
        if ($user->confirmation_code !== $code) {
            $this->addError('confirmationCode', 'The confirmation code is invalid.');
            return false;
        }
        $user->confirmation_code = null;
        $user->status_id = UserStatus::VERIFIED;
        $user->save();
        return true;
    }

    /**
     * Init user info when status user is pending.
     *
     * @param  array $input
     * @return mixed
     */
    public function initUserInfo(array $input)
    {
        $email = isset($input['email']) ? $input['email'] : null;
        $user = $this->getByEmail($email);

        if (!$user) {
            if (!$this->isValid($input, $this->getRules('create_member'))) {
                return false;
            }
            /** @var InviteMember $invite */
            $invite = InviteMember::whereToken($input['token'])->first();
            if (!$invite) {
                throw new ResourceException('Token is invalid');
            }
            $user = new User();
            $user->email = $invite->email;
            $user->first_name = $input['first_name'];
            $user->last_name = $input['last_name'];
            $user->password = $input['password'];
            $user->status_id = UserStatus::ACTIVE;

            if ($user->save()) {
                $userOrganization = new UserOrganization();
                $userOrganization->user_id = $user->id;
                $userOrganization->organization_id = $invite->organization_id;
                if ($invite->team_id) {
                    $userOrganization->team_id = $invite->team_id;
                }
                $userOrganization->role_id = $user->getRole(UserRole::MEMBER)->id;
                $userOrganization->save();
                $invite->delete();
            }
            return $user->fresh();

        } else {
            if ($user->status_id == UserStatus::PENDING) {
                throw new UpdateResourceFailedException('This email was not verified.');
            }
            if ($user->status_id == UserStatus::ACTIVE || $user->first_name) {
                /**
                 * Forbidden exception if:
                 * - status of user is ACTIVE
                 * - or status of user is VERIFIED but username is updated before that.
                 */
                throw new AccessDeniedHttpException;
            }
            return $this->store($user->id, $input);
        }
    }

    /**
     * Store user to database
     *
     * @param  integer $id
     * @param  array $input
     * @return mixed
     */
    public function store($id, array $input)
    {
        /** @var User $user */
        if (!$user = $this->getModelByID($id)) {
            return false;
        }
        if (!$this->isValid($input, $this->getRules('store', ['user' => $user]))) {
            return false;
        }
        $user->fill($input);
        if (!empty($input['password'])) {
            $user->password = $input['password'];
        }
        $user->save();
        return $user->fresh();
    }

    /**
     * Delete User
     *
     * @param  integer $id
     * @return mixed
     */
    public function destroy($id)
    {
        /** @var User $user */
        if (!$user = $this->getModelByID($id)) {
            return false;
        }
        if (\Gate::denies('delete', $user)) {
            throw new AccessDeniedHttpException('You cannot delete this user');
        }
        if ($user->hasLoggedDevice()) {
            $this->addError('user', 'User was logged in the system');
            return false;
        }
        $user->delete();
        return true;
    }

    /**
     * Get user details
     *
     * @param integer $userId
     * @param User $authenticatedUser
     * @return User
     */
    public function getProfile($userId, User $authenticatedUser)
    {
        if (!$user = User::find($userId)) {
            throw new NotFoundHttpException();
        }
        if ($user->id == $authenticatedUser->id) {
            $user->setProfileVisible();
        }
        $user = $this->getAdditionalInfo($user);
        return $user;
    }

    /**
     * @param array $input
     * @return User
     */
    public function socialLogin(array $input)
    {
        if (!$this->isValid($input, $this->getRules('social_login'))) {
            return false;
        }

        $profile = $this->getSocialProfile($input['social_type'], $input['social_token']);
        $connectedUser = $this->getUserBySocialProfile($profile);

        if (!$connectedUser) {
            $user = $this->getByEmail($profile->email);
            if ($user) {
                return $this->connectWithSocialProfile($user, $profile);
            } else {
                return $this->createUserFromSocialProfile($profile);
            }
        }

        return $connectedUser;
    }

    /**
 * @param User $user
 * @param SocialProfile $socialProfile
 * @return User
 */
    protected function connectWithSocialProfile(User $user, SocialProfile $socialProfile)
    {
        $user->social_type = $socialProfile->type->getName();
        $user->social_id = $socialProfile->id;
        $user->save();
        return $user;
    }

    /**
     * @param SocialProfile $socialProfile
     * @return User
     */
    protected function createUserFromSocialProfile(SocialProfile $socialProfile)
    {
        $invite = InviteMember::whereEmail($socialProfile->email)->first();
        if (!$invite) {
            throw new ResourceException('No invitation on this email.');
        }
        $user = new User();
        $user->email = strtolower($socialProfile->email);
        $user->first_name = $socialProfile->firstName;
        $user->last_name = $socialProfile->lastName;
        $user->organization_id = $invite->organization_id;
        $user->status_id = UserStatus::ACTIVE;
        $user->social_type = $socialProfile->type->getName();
        $user->social_id = $socialProfile->id;

        if ($user->save()) {
            $user->attachRole($user->getRole('member'));
        }
        return $user;
    }

    /**
     * @param SocialProfile $socialProfile
     * @return User
     */
    protected function getUserBySocialProfile(SocialProfile $socialProfile)
    {
        return $this->getModel()->whereSocialType($socialProfile->type->getName(), $socialProfile->id)->first();
    }

    /**
     * @param string $type
     * @param string $token
     * @return SocialProfile
     */
    public function getSocialProfile($type, $token)
    {
        if ($type == SocialTypeInterface::FB_TYPE) {
            return $this->getFacebookProfileByToken($token);
        } else if ($type == SocialTypeInterface::LINKED_IN_TYPE) {
            return $this->getLinkedInProfileByToken($token);
        } else {
            throw new BadRequestHttpException('The social type is not supported yet.');
        }
    }

    /**
     * @return \Facebook\Facebook
     */
    protected function getFacebookService()
    {
        return new \Facebook\Facebook(config('services.facebook'));
    }

    /**
     * @param string $token
     * @return SocialProfile
     */
    protected function getFacebookProfileByToken($token)
    {
        $fb = $this->getFacebookService();

        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get('/me?fields=id,name,picture,birthday,email,gender,first_name,last_name,middle_name', $token);
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            throw new BadRequestHttpException('Graph returned an error: ' . $e->getMessage());
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            throw new InternalHttpException('Facebook SDK returned an error: ' . $e->getMessage());
        }

        $user = $response->getGraphUser();

        return SocialProfile::createFromFacebookProfile($user);
    }

    /**
     * @return \Happyr\LinkedIn\LinkedIn
     */
    protected function getLinkedInService()
    {
        return new \Happyr\LinkedIn\LinkedIn(config('services.linked_in.app_id'), config('services.linked_in.app_secret'));
    }

    /**
     * @param string $token
     * @return SocialProfile
     */
    protected function getLinkedInProfileByToken($token)
    {
        try {
            $linkedIn = $this->getLinkedInService();
            $linkedIn->setAccessToken($token);
            /** @var array $user user info as array */
            $user = $linkedIn->get('v1/people/~:(id,firstName,lastName,emailAddress,pictureUrl)');
            return  SocialProfile::createFromLinkedInProfile($user);
        } catch(\Exception $e) {
            throw new InternalHttpException('LinkedIn REST API returned an error: ' . $e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param integer $organizationId
     * @param User $user
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function getMembers($request, $organizationId, User $user)
    {
        if (!$organization = Organization::find($organizationId)) {
            throw new NotFoundHttpException();
        }
        $this->validateOrganization($organization, $user);
        $model = User::whereOrganizationId($user->organization_id)
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.role_id', 3)
            ->selectRaw('users.*');

        return $this->getList($request, $model);
    }

    public function getAdditionalInfo($user) {
        if ($userOrganization = UserOrganization::whereUserId($user->id)->first()) {
            $user->organization_id = $userOrganization->organization_id;
            $user->role = $userOrganization->role;
        }
        return $user;
    }
}
