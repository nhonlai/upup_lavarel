<?php

namespace App\Extensions\Providers;

use Dingo\Api\Auth\Provider\JWT;
use Dingo\Api\Routing\Route;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;


class JWTExt extends JWT
{
    const DEFAULT_CODE = 101;
    const EXPIRED_CODE = 102;
    const INVALID_CODE = 103;

    /**
     * Authenticate request with a JWT.
     *
     * @param Request|\Illuminate\Http\Request $request
     * @param Route|\Dingo\Api\Routing\Route   $route
     *
     * @return mixed
     * @throws UnauthorizedHttpException
     * @throws \Exception
     */
    public function authenticate(Request $request, Route $route)
    {
        try {
            $token = $this->getToken($request);
            if (!$user = $this->auth->setToken($token)->authenticate()) {
                throw new UnauthorizedHttpException('JWTAuth', 'Unable to authenticate with invalid token.', null, static::DEFAULT_CODE);
            }
        } catch(TokenExpiredException $exception) {
            throw new UnauthorizedHttpException('JWTAuth', $exception->getMessage(), $exception, static::EXPIRED_CODE);
        } catch(TokenInvalidException $exception) {
            throw new UnauthorizedHttpException('JWTAuth', $exception->getMessage(), $exception, static::INVALID_CODE);
        } catch(JWTException $exception) {
            throw new UnauthorizedHttpException('JWTAuth', $exception->getMessage(), $exception, static::DEFAULT_CODE);
        }

        return $user;
    }
}