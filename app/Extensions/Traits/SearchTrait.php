<?php

namespace App\Extensions\Traits;


use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

trait SearchTrait
{
    /**
     * Return default sort
     *
     * @return array
     */
    public function getDefaultSort()
    {
        return [];
    }

    /**
     * Apply default sort
     *
     * @param object $model
     * @return Builder
     */
    public function applyDefaultSort($model)
    {
        foreach ($this->getDefaultSort() as $field => $direction) {
            $direction = ($direction === 'desc') ? $direction : 'asc';
            $model = $model->orderBy($field, $direction);
        }
        return $model;
    }

    /**
     * Return limit
     *
     * @param Request $request
     * @return int
     */
    protected function getLimit(Request $request)
    {
        $maxLimit = Config::get('api.maxLimit');
        $limit = (int)$request->get('limit');
        // Set default if limit is empty
        if ($limit == 0) {
            $limit = 10;
        }
        return (($limit < 1 || $limit > $maxLimit) ? $maxLimit : $limit);
    }


    /**
     * Return array of fields for select
     *
     * @param Request $request
     * @return array
     */
    protected function getFields(Request $request)
    {
        $visibleFields = $this->getVisibleFields();
        $fields = [];
        if ($request->get('fields')) {
            $fields = explode(',', $request->get('fields'));
            foreach ($fields AS $k => $field) {
                $field = trim($field);
                if (!in_array($field, $visibleFields)) {
                    unset($fields[$k]);
                }
            }
        }
        if (empty($fields)) {
            $fields = ['*'];
        }
        return $fields;
    }


    /**
     * Return Builder object after apply search
     *
     * @param object $model
     * @param Request $request
     * @return Builder
     */
    protected function search($model, Request $request)
    {
        $searchFields = $this->getSearchFields();

        if (!$request->get('search') || !count($searchFields)) {
            return $model;
        }

        $keyword = '%' . $request->get('search') . '%';

        $model = $model->where(function($query) use ($searchFields, $keyword){
            $isFirstWhere = true;
            foreach ($searchFields AS $field) {
                $operator = 'like';
                if ($isFirstWhere) {
                    $query = $query->where($field, $operator, $keyword);
                } else {
                    $query = $query->orWhere($field, $operator, $keyword);
                }
                $isFirstWhere = false;
            }
        });

        return $model;
    }

    /**
     * Return Builder object after apply filters
     *
     * @param object $model
     * @param Request $request
     * @return Builder
     */
    protected function filter($model, Request $request)
    {
        if (!$request->get('filter')) {
            return $model;
        }
        $visibleFields = $this->getVisibleFields();
        $filters = explode(',', $request->get('filter'));
        foreach ($filters AS $fieldValuePair) {
            $fieldValue = explode(':', $fieldValuePair);
            $field = trim($fieldValue[0]);
            $value = isset($fieldValue[1]) ? trim($fieldValue[1]) : null;
            if (in_array($field, $visibleFields) && !is_null($value) && $value !== '') {
                $model = $model->where($field, '=', $value);
            }
        }
        return $model;

    }


    /**
     * Return Builder object after apply order
     *
     * @param object $model
     * @param Request $request
     * @return Builder
     */
    protected function sort($model, Request $request)
    {
        if (!$request->get('sort')) {
            return $this->applyDefaultSort($model);
        }
        $visibleFields = $this->getVisibleFields();
        $sortFields = explode(',', $request->get('sort'));
        foreach ($sortFields AS $field) {
            $field = trim($field);
            $direction = 'asc';
            if (mb_substr($field, 0, 1) === '-') {
                $direction = 'desc';
                $field = mb_substr($field, 1, strlen($field));
            }
            if (in_array($field, $visibleFields)) {
                $model = $model->orderBy($field, $direction);
            }
        }
        return $model;
    }

    /**
     * @param LengthAwarePaginator $paginator
     * @param Request $request
     * @return LengthAwarePaginator
     */
    protected function addUrlParams(LengthAwarePaginator $paginator, Request $request)
    {
        $additionalParams = [
            'limit',
            'fields',
            'search',
            'filter',
            'sort'
        ];
        foreach ($additionalParams AS $param) {
            if (!empty($request->get($param))) {
                $paginator->appends($param, $request->get($param));
            }
        }
        return $paginator;
    }


    /**
     * Return result of paginate function
     *
     * @param Request $request
     * @param object $model
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function getList(Request $request, $model = null)
    {
        if (empty($model)) {
            $model = $this->getModel();
        }
        $limit = $this->getLimit($request);
        $fields = $this->getFields($request);

        $model = $this->search($model, $request);
        $model = $this->filter($model, $request);
        $model = $this->sort($model, $request);
        $paginator = $model->paginate($limit, $fields);

        return $this->addUrlParams($paginator, $request);
    }
}
