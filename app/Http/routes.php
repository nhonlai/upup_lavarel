<?php
if (!defined('API_NS')) {
    define('API_NS', '\App\Api\V1\Controllers\\');
}

// Iterate of files in directore and include
$require = function ($routeFolder) {
    $directoryIterator = new DirectoryIterator($routeFolder);
    foreach ($directoryIterator as $directory) {
        if ($directory->isFile()) {
            require $routeFolder . $directory->getFilename();
        }
    }
};

$require(__DIR__ . '/Routes/web/');
$require(__DIR__ . '/Routes/API/v1/');