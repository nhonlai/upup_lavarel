<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    $api->group(['middleware' => ['api.auth']], function ($api) {
        $api->delete('auth/logout', [
            'as'   => 'auth.logout',
            'uses' => API_NS . 'AuthenticateController@logout'
        ]);
    });

    $api->get('ping', [
        'as' => 'auth.ping',
        'uses' => API_NS . 'AuthenticateController@ping'
    ]);

    $api->post('auth/confirmationCode/request', [
        'as' => 'auth.confirmationCode.request',
        'uses' => API_NS . 'AuthenticateController@requestConfirmationCode'
    ]);

    $api->post('auth/confirmationCode/verify', [
        'as' => 'auth.confirmationCode.verify',
        'uses' => API_NS . 'AuthenticateController@verifyConfirmationCode'
    ]);

    $api->post('auth/login', [
        'as'   => 'auth.authenticate',
        'uses' => API_NS . 'AuthenticateController@authenticate'
    ]);

    $api->post('auth/password/reset', [
        'as'   => 'auth.password.reset',
        'uses' => API_NS . 'AuthenticateController@resetPassword'
    ]);

    $api->put('auth/password/reset', [
        'as'   => 'auth.password.confirm',
        'uses' => API_NS . 'AuthenticateController@confirmResetPassword'
    ]);

    $api->post('auth/login/social', [
        'as'   => 'auth.login.social',
        'uses' => API_NS . 'AuthenticateController@socialLogin'
    ]);
});



