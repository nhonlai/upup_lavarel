<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api){
    $api->group(['middleware' => 'api.auth'], function ($api) {
        $api->post('organizations/{id}/tasks', [
            'as' => 'tasks.store',
            'uses' => API_NS . 'TaskController@store'
        ]);
        $api->get('organizations/{id}/tasks', [
            'as' => 'tasks.index',
            'uses' => API_NS . 'TaskController@index',
        ]);
        $api->put('tasks/{id}', [
            'as' => 'tasks.update',
            'uses' => API_NS . 'TaskController@update'
        ]);
        $api->get('tasks/{id}', [
            'as' => 'tasks.show',
            'uses' => API_NS . 'TaskController@show'
        ]);
        $api->delete('tasks/{id}', [
            'as' => 'tasks.delete',
            'uses' => API_NS . 'TaskController@destroy'
        ]);
        $api->post('tasks/{id}/approve', [
            'as' => 'tasks.approve',
            'uses' => API_NS . 'TaskController@approve'
        ]);
        $api->get('organizations/{id}/task_statuses', [
            'as' => 'task_statuses.index',
            'uses' => API_NS . 'TaskController@listTaskStatuses',
        ]);
        $api->get('key_results', [
            'as' => 'key_results.index',
            'uses' => API_NS . 'TaskController@listKeyResults',
        ]);
        $api->get('badge_rewards', [
            'as' => 'badge_rewards.index',
            'uses' => API_NS . 'BadgeRewardController@index',
        ]);
    });
});
