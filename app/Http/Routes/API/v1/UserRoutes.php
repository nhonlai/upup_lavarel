<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api){
    $api->group(['middleware' => 'api.auth'], function ($api) {
        $api->get('users', [
            'as' => 'users.index',
            'uses' => API_NS . 'UserController@index'
        ]);

        $api->get('users/profile', [
            'as' => 'users.profile',
            'uses' => API_NS . 'UserController@profile'
        ]);

        $api->get('users/{id}', [
            'as' => 'users.show',
            'uses' => API_NS . 'UserController@show'
        ]);

        $api->delete('users/{id}', [
            'as' => 'users.destroy',
            'middleware' => ['middleware' => 'role:admin'],
            'uses' => API_NS . 'UserController@destroy'
        ]);

        $api->put('users/{id}', [
            'as' => 'users.update',
            'uses' => API_NS . 'UserController@update'
        ]);

        $api->put('users/me/password', [
            'as' => 'users.password.update',
            'uses' => API_NS . 'UserController@updatePassword'
        ]);

        $api->get('users/me/ship_tos', [
            'as' => 'users.ship_tos.index',
            'uses' => API_NS . 'UserController@listShipTos',
        ]);
        $api->post('users/me/ship_tos/{id}', [
            'as' => 'users.ship_tos.add',
            'uses' => API_NS . 'UserController@attachShipTo',
        ]);
        $api->delete('users/me/ship_tos/{id}', [
            'as' => 'users.ship_tos.delete',
            'uses' => API_NS . 'UserController@detachShipTo',
        ]);
    });

    $api->post('users', [
        'as' => 'users.store',
        'uses' => API_NS . 'UserController@store'
    ]);
});



