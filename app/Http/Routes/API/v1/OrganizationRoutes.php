<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api){
    $api->group(['middleware' => 'api.auth'], function ($api) {
        $api->post('organizations', [
            'as' => 'organizations.store',
            'uses' => API_NS . 'OrganizationController@store'
        ]);
        $api->get('organizations/{id}/members', [
            'as' => 'organizations.members',
            'uses' => API_NS . 'OrganizationController@getMembers'
        ]);
        $api->post('organizations/{id}/invite', [
            'as' => 'organizations.invite',
            'uses' => API_NS . 'OrganizationController@invite'
        ]);
    });
    $api->post('organizations/verifyName', [
        'as' => 'organizations.verifyName',
        'uses' => API_NS . 'OrganizationController@verifyName'
    ]);
});
