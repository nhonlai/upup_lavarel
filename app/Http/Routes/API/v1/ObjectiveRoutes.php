<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api){
    $api->group(['middleware' => 'api.auth'], function ($api) {
        $api->get('organizations/{id}/objectives', [
            'as' => 'objectives.list',
            'uses' => API_NS . 'ObjectiveController@index'
        ]);
        $api->post('organizations/{id}/objectives', [
            'as' => 'objectives.store',
            'uses' => API_NS . 'ObjectiveController@store'
        ]);
        $api->put('objectives/{id}', [
            'as' => 'objectives.update',
            'uses' => API_NS . 'ObjectiveController@update'
        ]);
        $api->get('objectives/{id}', [
            'as' => 'objectives.show',
            'uses' => API_NS . 'ObjectiveController@show'
        ]);
    });
});
