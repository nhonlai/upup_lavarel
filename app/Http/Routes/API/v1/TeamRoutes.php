<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api){
    $api->group(['middleware' => 'api.auth'], function ($api) {
        $api->get('organizations/{id}/teams', [
            'as' => 'users.teams.index',
            'uses' => API_NS . 'TeamController@index',
        ]);
        $api->post('organizations/{id}/teams', [
            'as' => 'teams.store',
            'uses' => API_NS . 'TeamController@store'
        ]);
        $api->get('teams/{id}', [
            'as' => 'teams.show',
            'uses' => API_NS . 'TeamController@show'
        ]);
        $api->put('teams/{id}', [
            'as' => 'teams.update',
            'uses' => API_NS . 'TeamController@update'
        ]);
        $api->delete('teams/{id}', [
            'as' => 'teams.delete',
            'uses' => API_NS . 'TeamController@destroy'
        ]);
        $api->post('teams/{id}/invite', [
            'as' => 'teams.invite',
            'uses' => API_NS . 'TeamController@invite'
        ]);
        $api->get('teams/{id}/members', [
            'as' => 'teams.members',
            'uses' => API_NS . 'TeamController@getMembers'
        ]);
    });
});
