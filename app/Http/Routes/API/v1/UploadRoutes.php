<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    $api->group(['middleware' => 'api.auth'], function ($api) {
        $api->get('upload/signature', [
            'as' => 'upload.signature',
            'uses' => API_NS . 'UploadController@getSignature'
        ]);
    });
});



