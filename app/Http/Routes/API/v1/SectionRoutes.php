<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api){
    $api->group(['middleware' => 'api.auth', 'role:founder|manager'], function ($api) {
        $api->post('sections', [
            'as' => 'sections.store',
            'uses' => API_NS . 'SectionController@store'
        ]);
        $api->put('sections/{id}', [
            'as' => 'sections.update',
            'uses' => API_NS . 'SectionController@update'
        ]);
        $api->delete('sections/{id}', [
            'as' => 'sections.delete',
            'uses' => API_NS . 'SectionController@destroy'
        ]);
    });

    $api->group(['middleware' => 'api.auth'], function ($api) {
        $api->get('sections/{id}', [
            'as' => 'sections.show',
            'uses' => API_NS . 'SectionController@show'
        ]);

        $api->get('sections', [
            'as' => 'sections.list',
            'uses' => API_NS . 'SectionController@index'
        ]);
    });

});
