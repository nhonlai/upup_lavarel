<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api){
    $api->group(['middleware' => 'api.auth'], function ($api) {
        $api->post('objectives/{id_objective}/key_results', [
            'as' => 'key_results.store',
            'uses' => API_NS . 'KeyResultController@store'
        ]);
        $api->put('objectives/{id_objective}/key_results/{id}', [
            'as' => 'key_results.update',
            'uses' => API_NS . 'KeyResultController@update'
        ]);
        $api->delete('objectives/{id_objective}/key_results/{id}', [
            'as' => 'key_results.delete',
            'uses' => API_NS . 'KeyResultController@destroy'
        ]);
        $api->get('objectives/{id_objective}/key_results/{id}', [
            'as' => 'key_results.show',
            'uses' => API_NS . 'KeyResultController@show'
        ]);

        $api->get('objectives/{id_objective}/key_results', [
            'as' => 'key_results.list',
            'uses' => API_NS . 'KeyResultController@index'
        ]);
    });
});
