<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api){
    $api->group(['middleware' => 'api.auth'], function ($api) {
        $api->get('organizations/{id}/badges', [
            'as' => 'organizations.badges.index',
            'uses' => API_NS . 'BadgeRewardController@index',
        ]);
        $api->put('badges/{id}', [
            'as' => 'badges.update',
            'uses' => API_NS . 'BadgeRewardController@update'
        ]);
    });
});



