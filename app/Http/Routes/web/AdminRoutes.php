<?php


Route::group(['prefix' => 'admin', 'middleware' => ['auth:web', 'role:admin']], function(){
    Route::get('users', [
        'as' => 'admin.users',
        'uses' => 'Admin\\UserController@index',
    ]);

    Route::get('users/add', [
        'as' => 'admin.users.add.form',
        'uses' => 'Admin\\UserController@showAddForm',
    ]);

    Route::post('users/add', [
        'as' => 'admin.users.add',
        'uses' => 'Admin\\UserController@add',
    ]);

    Route::get('users/{id}/edit', [
        'as' => 'admin.users.edit.form',
        'uses' => 'Admin\\UserController@showEditForm',
    ]);

    Route::post('users/{id}/edit', [
        'as' => 'admin.users.update',
        'uses' => 'Admin\\UserController@update',
    ]);

    Route::get('users/{id}', [
        'as' => 'admin.users.view',
        'uses' => 'Admin\\UserController@view',
    ]);

    Route::delete('users/{id}', [
        'as' => 'admin.users.delete',
        'uses' => 'Admin\\UserController@delete',
    ]);

    Route::get('promotions', [
        'as' => 'admin.promotions',
        'uses' => 'Admin\\PromotionController@index',
    ]);

    Route::get('promotions/{id}/edit', [
        'as' => 'admin.promotions.edit.form',
        'uses' => 'Admin\\PromotionController@showEditForm',
    ]);

    Route::post('promotions/{id}/edit', [
        'as' => 'admin.promotions.edit',
        'uses' => 'Admin\\PromotionController@update',
    ]);

    Route::get('promotions/add', [
        'as' => 'admin.promotions.add.form',
        'uses' => 'Admin\\PromotionController@showAddForm',
    ]);

    Route::get('promotions/{id}', [
        'as' => 'admin.promotions.view',
        'uses' => 'Admin\\PromotionController@view',
    ]);

    Route::delete('promotions/{id}', [
        'as' => 'admin.promotions.delete',
        'uses' => 'Admin\\PromotionController@delete',
    ]);

    Route::post('promotions/add', [
        'as' => 'admin.promotions.add',
        'uses' => 'Admin\\PromotionController@add',
    ]);

    Route::get('locations', [
        'as' => 'admin.locations',
        'uses' => 'Admin\\LocationController@index',
    ]);

    Route::get('locations/add', [
        'as' => 'admin.locations.add.form',
        'uses' => 'Admin\\LocationController@showAddForm',
    ]);

    Route::post('locations/add', [
        'as' => 'admin.locations.add',
        'uses' => 'Admin\\LocationController@add',
    ]);

    Route::get('locations/{id}/edit', [
        'as' => 'admin.locations.edit.form',
        'uses' => 'Admin\\LocationController@showEditForm',
    ]);

    Route::post('locations/{id}/edit', [
        'as' => 'admin.locations.edit',
        'uses' => 'Admin\\LocationController@update',
    ]);

    Route::get('locations/{id}', [
        'as' => 'admin.locations.view',
        'uses' => 'Admin\\LocationController@view',
    ]);

    Route::delete('locations/{id}', [
        'as' => 'admin.locations.delete',
        'uses' => 'Admin\\LocationController@delete',
    ]);
});