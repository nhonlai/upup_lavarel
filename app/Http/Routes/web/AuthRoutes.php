<?php

Route::group(['prefix' => 'admin'], function () {
    Route::get('login', [
        'as'   => 'admin.getLogin',
        'uses' => 'Auth\\AuthController@showLoginForm'
    ]);
    Route::post('login', [
        'as'   => 'admin.postLogin',
        'uses' => 'Auth\\AuthController@login',
    ]);
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth:web']], function(){
    Route::get('logout', [
        'as' => 'admin.getLogout',
        'uses' => 'Auth\\AuthController@logout',
    ]);
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth:web', 'role:admin']], function(){

});

Route::group(['middleware' => ['web']], function () {
    Route::get('password/reset', 'Auth\PasswordController@showResetPasswordForm');
    Route::post('password/reset', 'Auth\PasswordController@resetPassword');
});

Route::get('/facebook/login', [
    'as'   => 'facebook.callback',
    'uses' =>  "FacebookController@showLogin"
]);

Route::get('/facebook/callback', [
    'as'   => 'facebook.callback',
    'uses' =>  "FacebookController@callback"
]);


Route::get('/linked_in/login', [
    'as'   => 'linked_in.callback',
    'uses' =>  "LinkedInController@showLogin"
]);

Route::get('/linked_in/callback', [
    'as'   => 'linked_in.callback',
    'uses' =>  "LinkedInController@callback"
]);
