<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;

/**
 * This class used for operation with payments
 */
class HomeController extends Controller
{

    /**
     * @route sessions.create
     * @return mixed
     */
    public function index()
    {
        return View::make('home.index');
    }


}