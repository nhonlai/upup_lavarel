<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;

/**
 * This class used for operation with payments
 */
class ExampleController extends Controller
{

    /**
     * @route example.index
     * @return mixed
     */
    public function index()
    {
        return View::make('example.index');
    }


    /**
     * @route example.show
     * @return mixed
     */
    public function show()
    {
        return View::make('example.show');
    }
}