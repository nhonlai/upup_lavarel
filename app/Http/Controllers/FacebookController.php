<?php

namespace App\Http\Controllers;

/**
 * This class used for operation with payments
 */
class FacebookController extends Controller
{
    const FB_APP_ID = '267313637049366';
    const FB_APP_SECRET = 'c0eb4bd2b51467ebe740aec59272c433';

    /**
     * @return \Facebook\Facebook
     */
    protected function getFacebookService()
    {
        return new \Facebook\Facebook(config('services.facebook'));
    }

    /**
     * @route sessions.create
     * @return mixed
     */
    public function callback()
    {
        if(!session_id()) {
            session_start();
        }
        $fb = $this->getFacebookService();

        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }

        // Logged in
        echo '<h3>Access Token</h3>';
        dump($accessToken->getValue());


        $user = $this->getProfile($accessToken->getValue());
        echo '<h3>Profile</h3>';
        dump($user);

        exit();
    }

    /**
     * @param string $accessToken
     * @return \Facebook\GraphNodes\GraphUser
     */
    protected function getProfile($accessToken)
    {
        $fb = $this->getFacebookService();
        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get('/me?fields=id,name,picture,birthday,email,gender,first_name,last_name,middle_name', $accessToken);
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        return $response->getGraphUser();
    }

    /**
     * @route sessions.create
     * @return mixed
     */
    public function showLogin()
    {
        if(!session_id()) {
            session_start();
        }
        //$userRepo = app(\App\Repositories\UserRepository::class);
        //$token = $userRepo->authenticateById(2);
        //dump($token);

        $fb = $this->getFacebookService();
        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email']; // Optional permissions
        $loginUrl = $helper->getLoginUrl('http://upup.lvh.me/facebook/callback', $permissions);

        echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
        exit();
    }
}
