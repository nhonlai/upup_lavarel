<?php

namespace App\Http\Controllers;

/**
 * This class used for operation with payments
 */
class LinkedInController extends Controller
{
    const LINKED_ID_APP_ID = 'dyccsp1nslrn';
    const LINKED_ID_APP_SECRET = 'BJqTpVqFiFJdSzv6';

    /**
     * @return \Happyr\LinkedIn\LinkedIn
     */
    protected function getLinkedInService()
    {
        return new \Happyr\LinkedIn\LinkedIn(config('services.linked_in.app_id'), config('services.linked_in.app_secret'));
    }

    /**
     * @return string
     */
    protected function getScope()
    {
        return 'r_basicprofile,r_emailaddress';
    }
    /**
     * @route sessions.create
     * @return mixed
     */
    public function callback()
    {
        if(!session_id()) {
            session_start();
        }

        $linkedIn = $this->getLinkedInService();

        $accessToken = $linkedIn->getAccessToken();

        // Logged in
        echo '<h3>Access Token</h3>';
        dump($accessToken->getToken());

        $user = $this->getProfile($accessToken);
        echo '<h3>Profile</h3>';
        dump($user);

        exit();
    }

    /**
     * @param string $accessToken
     * @return array
     */
    protected function getProfile($accessToken)
    {
        $linkedIn = $this->getLinkedInService();
        $linkedIn->setAccessToken($accessToken);
        return $linkedIn->get('v1/people/~:(id,firstName,lastName,emailAddress,pictureUrl)');
    }

    /**
     * @route sessions.create
     * @return mixed
     */
    public function showLogin()
    {
        if(!session_id()) {
            session_start();
        }
        //$access_token = 'AQXCFhTAdJvhNcLMf3BKQtpEJ8dmeE-xKuQCznvOPz7AMwX_IXw6F87C9ohGMykiRyrDEtPAU1h5TkWoXV5Ou-8EBQXKhsLgyuyrYBuSrduAkOgf-_wRQgJn6wMGSIdEq4Ow7g_K9xij7i30aHpJgZSU7o5kzejnRvi9i1JCEdHgSMo90_c';
        //$access_token = 'AQVqlJSTFuA3vD6D1pEUhDDbxE4_dQ0ml0vISA_fIQxbebjywkmyrymgQQChrx2NVcxdMJNnzsZkMkIJRLA0FOPmTdysLPkQnx5dzutObmhihbQKIYzryRVbFgCckC5MkRs8dF4nsOEQ3CyQ9BZEnB_L7Hs1wZuvPlZNgQEj1-ve5f1RtbY';
        $linkedIn = $this->getLinkedInService();

        $loginUrl = $linkedIn->getLoginUrl([
            'scope' => $this->getScope(),
            'redirect_uri' => 'http://upup.lvh.me/linked_in/callback',
        ]);

        echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with LinkedIn!</a>';
        exit();
    }
}
