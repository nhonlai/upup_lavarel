<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\PasswordResetRepository;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    public $repo;

    /**
     * Create a new password controller instance.
     *
     * @param PasswordResetRepository $repo
     */
    public function __construct(PasswordResetRepository $repo)
    {
        $this->repo = $repo;
        $this->middleware('guest');
    }

    /**
     * Display the password reset view.
     *
     * @param Request $request
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function showResetPasswordForm(Request $request)
    {
        $view = view('auth.passwords.reset');
        if (!$this->repo->getUserByToken($request->input('token'))) {
            $view->withErrors($this->repo->errors());
        }

        return $view;
    }

    /**
     * Display the password reset view.
     *
     * @param Request $request
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function resetPassword(Request $request)
    {
        $view = $this->showResetPasswordForm($request);

        if (!$this->repo->errors()) {
            if(!$this->repo->resetPassword($request->input())) {
                return $view->withErrors($this->repo->errors());
            }
        }

        return $view->with('message', 'Your password is reset successfully!');
    }
}
