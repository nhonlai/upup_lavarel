<?php


namespace App\Http\Controllers\Admin;

use Dingo\Api\Http\Response;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class AdminController extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /**
     * Get the response factory instance.
     *
     * @return \Dingo\Api\Http\Response\Factory
     */
    protected function ajaxResponse()
    {
        return app('Dingo\Api\Http\Response\Factory');
    }

    /**
     * Get the internal api dispatcher instance.
     *
     * @return \Dingo\Api\Dispatcher
     */
    protected function api()
    {
        return app('Dingo\Api\Dispatcher');
    }
}
