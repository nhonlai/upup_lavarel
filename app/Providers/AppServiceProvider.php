<?php

namespace App\Providers;

use App\Model\Task;
use App\Observers\TaskObserver;
use Dingo\Api\Exception\Handler as ExceptionHandler;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Blade::directive('selected', function($value) {
            return "<?php echo ({$value}) ? 'selected' : ''; ?>";
        });
        Task::observe(TaskObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
        if ($this->app->environment() == 'local') {
            $this->app->register(\Iber\Generator\ModelGeneratorProvider::class);
            $this->app->register(\Way\Generators\GeneratorsServiceProvider::class);
            $this->app->register(\Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class);
        }

        $this->app['api.exception']->register(function(\Exception $ex) {
            $request = app('request');
            if (!$request->ajax() && !$request->is('api/*')) {
                return app(\App\Exceptions\Handler::class)->render($request, $ex);
            }
            return null;
        });
    }
}
