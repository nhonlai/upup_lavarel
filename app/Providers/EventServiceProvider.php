<?php

namespace App\Providers;

use App\Events\TaskApproved;
use App\Events\TaskDone;
use App\Listeners\TaskApprovedListener;
use App\Listeners\TaskDoneListener;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
        'Dingo\Api\Event\ResponseWasMorphed' => [
            'App\Listeners\AddPaginationLinksToResponse'
        ],
        TaskDone::class => [
            TaskDoneListener::class,
        ],
        TaskApproved::class => [
            TaskApprovedListener::class,
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
