<?php

namespace App\Providers;

use App\Constants\Ability;
use App\Model\Task;
use App\Model\TaskStatus;
use App\Model\User;
use App\Policies\UserPolicy;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $gate->define(Ability::TASK_APPROVE, function(User $user, Task $task) {
            return $user && $task
                && $user->hasManagerRole()
                && $task->organization_id == $user->organization_id;
        });

        $gate->define(Ability::SECTION_CREATE, function(User $user) {
            return $user && $user->hasManagerRole();
        });

        $gate->define(Ability::SECTION_UPDATE, function(User $user, TaskStatus $taskStatus) {
            return $user && $taskStatus
                && $user->hasManagerRole()
                && $taskStatus->organization_id == $user->organization_id;
        });

        $gate->define(Ability::SECTION_DELETE, function(User $user, TaskStatus $taskStatus) {
            return $user && $taskStatus
            && $user->hasManagerRole()
            && $taskStatus->organization_id == $user->organization_id;
        });
    }
}
