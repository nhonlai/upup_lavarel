<?php

namespace App\Policies;

use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given user can be updated by current user.
     *
     * @param  User  $currentUser
     * @param  User  $updateUser
     * @return bool
     */
    public function update(User $currentUser, User $updateUser)
    {
        return $currentUser->id === $updateUser->id || $currentUser->isAdmin();
    }

    /**
     * @param User $currentUser
     * @param User $deleteUser
     *
     * @return bool
     */
    public function delete(User $currentUser, User $deleteUser)
    {
        return $currentUser->id != $deleteUser->id && $currentUser->isAdmin();
    }
}
