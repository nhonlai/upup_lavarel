<?php

namespace App\Model;

use App\Repositories\KeyResultRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\KeyResult
 *
 * @property integer $id
 * @property string $title
 * @property \Carbon\Carbon $due_date
 * @property integer $start_value
 * @property integer $target_value
 * @property integer $weight
 * @property integer $completed_percent
 * @property integer $objective_id
 * @property integer $team_id
 * @property integer $organization_id
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Model\User $creator
 * @property-read \App\Model\Objective $objective
 * @property-read \App\Model\Team $team
 * @property-read \App\Model\Organization $organization
 * @method static \Illuminate\Database\Query\Builder|\App\Model\KeyResult whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\KeyResult whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\KeyResult whereDueDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\KeyResult whereStartValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\KeyResult whereTargetValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\KeyResult whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\KeyResult whereCompletedPercents($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\KeyResult whereObjectiveId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\KeyResult whereTeamId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\KeyResult whereOrganizationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\KeyResult whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\KeyResult whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\KeyResult whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class KeyResult extends Model
{
    /**
     * @var array
     */
    protected static $mileStones = [
        20,
        50,
        80,
        100,
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'due_date',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'due_date',
        'start_value',
        'target_value',
        'weight',
        'completed_percent',
        'objective_id',
        'team_id',
    ];

    /**
     * Get creator.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * Get organization.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function objective()
    {
        return $this->belongsTo(Objective::class);
    }

    /**
     * Get organization.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    /**
     * Get organization.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    /**
     * @return $this
     */
    public function updateCompletedPercent()
    {
        $this->completed_percent = app(KeyResultRepository::class)->calculateCompletedPercent($this->id);
        \Log::debug("[Key result][id:$this->id] completed percent: $this->completed_percent");
        $this->save();

        if ($this->objective) {
            $this->objective->updateCompletedPercent();
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function goToNextMileStone()
    {
        if ($nextMileStone = $this->getNextMileStone()) {
            $this->completed_percent = $nextMileStone;
            $this->save();

            if ($this->objective) {
                $this->objective->updateCompletedPercent();
            }
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getNextMileStone()
    {
        foreach (static::$mileStones as $mileStone) {
            if ($this->completed_percent < $mileStone) {
                return $mileStone;
            }
        }
        return null;
    }

    /**
     * @return int
     */
    public function getPreviousMileStone()
    {
        foreach (array_reverse(static::$mileStones) as $mileStone) {
            if ($this->completed_percent > $mileStone) {
                return $mileStone;
            }
        }
        return 0;
    }
}
