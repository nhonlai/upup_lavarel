<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DeviceType
 *
 * @mixin \Eloquent
 * @package App\Model
 * @property integer $id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\App\Model\DeviceType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\DeviceType whereName($value)
 */
class DeviceType extends Model
{
    const IOS     = 1;
    const ANDROID = 2;
    const WEB     = 3;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
    ];
}
