<?php


namespace App\Model;


use Illuminate\Database\Eloquent\Model;

/**
 * Class PasswordReset
 *
 * @package App\Model
 * @property string $email
 * @property string $token
 * @property \Carbon\Carbon $created_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\PasswordReset whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\PasswordReset whereToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\PasswordReset whereCreatedAt($value)
 * @mixin \Eloquent
 */
class PasswordReset extends Model
{
    /**
     * @var string
     */
    protected $primaryKey = 'email';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email' => 'string',
    ];

    /**
     * Get the name of the "updated at" column.
     *
     * @return string
     */
    public function getUpdatedAtColumn()
    {
        return null;
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function setUpdatedAt($value)
    {
        // disable updated at
        return $this;
    }

    /**
     * @param $email
     *
     * @return $this
     */
    public function generateToken($email)
    {
        $this->attributes['email'] = $email;
        $this->token = md5(uniqid());
        return $this;
    }
}
