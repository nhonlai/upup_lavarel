<?php

namespace App\Model;


use App\Constants\UserRole;
use Bican\Roles\Models\Role;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;


/**
 * App\Model\User
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property integer $status_id
 * @property string $remember_token
 * @property string $confirmation_code
 * @property string $phone
 * @property string $social_type
 * @property string $social_id
 * @property string $avatar
 * @property \Carbon\Carbon                                                                 $created_at
 * @property \Carbon\Carbon                                                                 $updated_at
 * @property \Carbon\Carbon                                                               $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\NotificationType[] $notificationTypes
 * @property-read mixed $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Device[] $devices
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereOrganizationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereTeamId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereConfirmationCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereSocialType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereSocialId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\User whereDeletedAt($value)
 * @mixin \Eloquent
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable, CanResetPassword;


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

//    protected $appends = [
//        'role',
//    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'email',
        'first_name',
        'last_name',
        'phone',
        'avatar',
        'created_at',
        'role',
        'organization_id',
    ];

    /**
     * The attributes that should be visible in arrays for self profile.
     *
     * @var array
     */
    protected $profileVisible = [
        'id',
        'email',
        'first_name',
        'last_name',
        'phone',
        'avatar',
        'role',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'first_name',
        'last_name',
        'phone',
        'avatar'
    ];

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
        'password',
        'remember_token',
        'confirmation_code',
        'status_id',
        'created_at',
        'updated_at',
        'api_token'
    ];

    /**
     * Property for caching shipTos.
     *
     * @var \Illuminate\Database\Eloquent\Collection|null
     */
    protected $shipTos;

    /**
     * Property for caching notification types.
     *
     * @var \Illuminate\Database\Eloquent\Collection|null
     */
    protected $notificationTypes;

    /**
     * User belongs to many notification types.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function notificationTypes()
    {
        return $this->belongsToMany(NotificationType::class, "notification_settings")->withPivot('on')->withTimestamps();
    }

    /**
     * Get all notification types as collection.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getNotificationTypes()
    {
        return (!$this->notificationTypes) ? $this->notificationTypes = $this->notificationTypes()->get() : $this->notificationTypes;
    }

    /**
     * Update notification setting for a user.
     *
     * @param int|NotificationType $notificationType
     * @param null|bool $on
     * @return null|bool
     */
    public function updateNotificationSetting($notificationType, $on = null)
    {
        if (is_null($on)) {
            $on = $notificationType->defaultOn;
        }
        if (!$this->notificationTypes()->get()->contains($notificationType)) {
            $this->notificationTypes()->attach($notificationType, ['on' => $on]);
        } else {
            $this->notificationTypes()->updateExistingPivot($notificationType, ['on' => $on]);
        }
        return true;
    }

    /**
     * Get all notification settings.
     *
     * @return array
     */
    public function getNotificationSettings()
    {
        // Check if has some notification types be not added as notification settings of user
        // then init notification settings for user
        if (count($this->getNotificationTypes()) != NotificationType::count()) {
            $this->initNotificationSettings();
        }
        $settings = [];
        foreach ($this->getNotificationTypes() as $notificationSetting) {
            $setting = $notificationSetting->pivot->toArray();
            $settings[] = [
                'name' => $notificationSetting->name,
                'notification_type_id' => $setting['notification_type_id'],
                'on' => (bool) $setting['on'],

            ];
        }
        return $settings;
    }

    /**
     * Init notification settings for a user.
     *
     * @return void
     */
    private function initNotificationSettings()
    {
        foreach (NotificationType::all() as $notificationType) {
            if (!$this->getNotificationTypes()->contains($notificationType)) {
                $this->updateNotificationSetting($notificationType);
            }
        }

        // update caching notification types
        $this->notificationTypes = $this->notificationTypes()->get();
    }

    /**
     * Set visible attributes for self profile
     *
     * @return User
     */
    public function setProfileVisible()
    {
        $this->setVisible($this->profileVisible);
        return $this;
    }

    /**
     * Validate password
     *
     * @param string $password
     *
     * @return boolean
     */
    public function passwordCheck($password)
    {
        return password_verify($password, $this->password);
    }

    /**
     * Sets the user's password.
     *
     * @param string $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * @param $slug
     *
     * @return mixed
     */
    public function getRole($slug)
    {
        return Role::whereSlug($slug)->first();
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }


    /**
     * Get notification emails
     *
     * @param bool $primaryEmailInclude
     * @return string[]
     */
    public function getNotificationEmails($primaryEmailInclude = false)
    {
        $emails = [];
        if (!empty($this->notification_emails)) {
            $emails = explode(',', $this->notification_emails);
        }

        if ($primaryEmailInclude) {
            $emails = array_merge([$this->email], $emails);
        }
        return $emails;
    }

//    public function getRoleAttribute()
//    {
//        if ($this->hasManagerRole()) {
//            $role = $this->getRole(UserRole::FOUNDER);
//        } else if ($this->isMember()) {
//            $role = $this->getRole(UserRole::MEMBER);
//        } else {
//            return null;
//        }
//        return [
//            'id' => $role->id,
//            'name' => title_case($role->slug == UserRole::FOUNDER ? UserRole::MANAGER : $role->name),
//        ];
//    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function devices()
    {
        return $this->hasMany(Device::class, 'user_id', 'id');
    }

    /**
     * @return bool
     */
    public function hasLoggedDevice()
    {
        return $this->devices()->count() > 0;
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::saving(function(User $user) {
            if ($user->status_id == UserStatus::VERIFIED && $user->first_name && empty($user->getOriginal('first_name'))) {
                $user->status_id = UserStatus::ACTIVE;
            }
        });
    }

//    /**
//     * @return bool
//     */
//    public function isMember()
//    {
//        return $this->is(UserRole::MEMBER);
//    }
//
//    /**
//     * @return bool
//     */
//    public function hasManagerRole()
//    {
//        return $this->is([UserRole::FOUNDER, UserRole::MANAGER]);
//    }
}
