<?php

namespace App\Model;

use App\Repositories\ObjectiveRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\Objective
 *
 * @property integer $id
 * @property string $title
 * @property \Carbon\Carbon $due_date
 * @property integer $completed_percent
 * @property integer $organization_id
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Model\User $creator
 * @property-read \App\Model\Organization $organization
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Objective whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Objective whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Objective whereDueDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Objective whereCompletedPercents($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Objective whereOrganizationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Objective whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Objective whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Objective whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Objective extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'due_date',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'due_date',
        'organization_id',
    ];

    /**
     * Get creator.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * Get organization.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    /**
     * @return $this
     */
    public function updateCompletedPercent()
    {
        $this->completed_percent = app(ObjectiveRepository::class)->calculateCompletedPercent($this->id);
        \Log::debug("[Objective][id:$this->id] completed percent: $this->completed_percent");
        $this->save();
        return $this;
    }
}
