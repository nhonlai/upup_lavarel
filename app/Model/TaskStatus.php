<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\TaskStatus
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $icon
 * @property string $color
 * @property boolean $is_system
 * @property integer $organization_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TaskStatus whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TaskStatus whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TaskStatus whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TaskStatus whereIcon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TaskStatus whereColor($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TaskStatus whereIsSystem($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TaskStatus whereOrganizationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TaskStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\TaskStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TaskStatus extends Model
{
    const OPEN = 1;
    const IN_PROGRESS = 2;
    const DONE = 3;
    const APPROVED = 4;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_system' => 'boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'description',
        'icon',
        'color',
        'is_system',
        'organization_id',
    ];
}
