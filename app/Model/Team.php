<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\Team
 *
 * @property integer $id
 * @property string $name
 * @property string $notes
 * @property integer $created_by
 * @property integer $organization_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Model\User $creator
 * @property-read \App\Model\Organization $organization
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Team whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Team whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Team whereNotes($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Team whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Team whereOrganizationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Team whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Team whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Team extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'notes',
    ];

    /**
     * Get creator.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get organization.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
