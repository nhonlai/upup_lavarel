<?php

namespace App\Model;

use Bican\Roles\Models\Role;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\UserOrganization
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $organization_id
 * @property integer $team_id
 * @property integer $badges
 * @property integer $points
 * @property integer $level
 * @property integer $role_id
 * @property-read integer $role
 * @property \Carbon\Carbon                                                                 $created_at
 * @property \Carbon\Carbon                                                                 $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\UserOrganization whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\UserOrganization whereOrganizationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\UserOrganization whereTeamId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\UserOrganization whereBadges($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\UserOrganization wherePoints($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\UserOrganization whereLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\UserOrganization whereRoleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\UserOrganization whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\UserOrganization whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class UserOrganization extends Model
{
    protected $table = 'user_organization';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'organization_id',
        'team_id',
    ];
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
