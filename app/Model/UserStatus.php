<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\UserStatus
 *
 * @property integer $id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\App\Model\UserStatus whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\UserStatus whereName($value)
 * @mixin \Eloquent
 */
class UserStatus extends Model
{
    const PENDING = 1;
    const VERIFIED = 2;
    const ACTIVE = 3;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
    ];
}
