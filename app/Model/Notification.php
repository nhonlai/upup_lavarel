<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\Notification
 *
 * @property integer $id
 * @property integer $notification_type_id
 * @property integer $user_id
 * @property string $subject
 * @property string $message
 * @property string $data
 * @property \Carbon\Carbon $delivered_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Notification whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Notification whereNotificationTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Notification whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Notification whereSubject($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Notification whereMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Notification whereData($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Notification whereDeliveredAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Notification whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Notification whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Notification extends Model
{

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'delivered_at',
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'notification_type_id',
        'user_id',
        'subject',
        'message',
        'data',
        'created_at',
        'updated_at',
        'delivered_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'notification_type_id',
        'user_id',
        'subject',
        'message',
        'data'
    ];

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'created_at',
        'updated_at',
        'delivered_at',
    ];

}