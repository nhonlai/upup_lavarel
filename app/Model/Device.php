<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Device
 *
 * @package App\Model
 * @property integer $id
 * @property integer $user_id
 * @property integer $type_id
 * @property string $device_token
 * @property string $hash
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Device whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Device whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Device whereTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Device whereDeviceToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Device whereHash($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Device whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Device whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Device extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'type_id',
        'device_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $typeIDs = [
        'ios'     => 1,
        'android' => 2,
        'web'     => 3,
    ];

    /**
     * Get device type id from name
     *
     * @param $name
     *
     * @return null
     */
    public function getTypeIdFromName($name)
    {
        if (isset($this->typeIDs[$name])) {
            return $this->typeIDs[$name];
        }
        return null;
    }

    /**
     * Set device token value
     *
     * @param $value
     *
     * @return $this
     */
    public function setDeviceTokenAttribute($value)
    {
        $this->attributes['device_token'] = $value;
        $this->attributes['hash']         = $this->makeHash($value);
        return $this;
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    protected function makeHash($value)
    {
        return sha1($value);
    }

    /**
     * @param $type
     *
     * @return $this
     */
    public function setDeviceType($type)
    {
        $this->type_id = $this->getTypeIdFromName(strtolower($type));
        return $this;
    }

    /**
     * @param $token
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getByToken($token)
    {
        return $this->where('hash', '=', $this->makeHash($token))->first();
    }
}
