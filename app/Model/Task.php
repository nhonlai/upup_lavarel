<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\Task
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property \Carbon\Carbon $due_date
 * @property integer $points
 * @property integer $required_level
 * @property integer $status_id
 * @property integer $badge_reward_id
 * @property integer $key_result_id
 * @property integer $organization_id
 * @property integer $assignee_id
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Model\TaskStatus $status
 * @property-read \App\Model\BadgeReward $badgeReward
 * @property-read \App\Model\User $assignee
 * @property-read \App\Model\KeyResult $keyResult
 * @property-read \App\Model\User $creator
 * @property-read \App\Model\Organization $organization
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Task whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Task whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Task whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Task whereDueDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Task wherePoints($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Task whereRequiredLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Task whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Task whereBadgeRewardId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Task whereKeyResultId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Task whereOrganizationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Task whereAssigneeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Task whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Task whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Task whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Task extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'due_date',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'points',
        'required_level',
        'due_date',
        'status_id',
        'badge_reward_id',
        'key_result_id',
        'organization_id',
        'assignee_id',
    ];

    protected $hidden = [
        'points',
    ];

    /**
     * Get creator.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * Get assignee.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assignee()
    {
        return $this->belongsTo(User::class, 'assignee_id');
    }

    /**
     * Get organization.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    /**
     * Get key result.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function keyResult()
    {
        return $this->belongsTo(KeyResult::class);
    }

    /**
     * Get status.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(TaskStatus::class);
    }

    /**
     * Get badge reward.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function badgeReward()
    {
        return $this->belongsTo(BadgeReward::class);
    }

    /**
     * @return bool
     */
    public function isApproved()
    {
        return $this->status_id == TaskStatus::APPROVED;
    }
}
