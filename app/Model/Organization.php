<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\Organization
 *
 * @property integer $id
 * @property string $name
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \App\Model\User $creator
 * @property-read \App\Model\User[] $members
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Organization whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Organization whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Organization whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Organization whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Organization whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Organization whereDeletedAt($value)
 * @mixin \Eloquent
 */
class Organization extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Get members of an organization.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function members()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Get creator.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class);
    }
}
