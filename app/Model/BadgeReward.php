<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\BadgeReward
 *
 * @property integer $id
 * @property string $title
 * @property string $icon
 * @property string $description
 * @property string $organization_id
 * @property integer $badges
 * @property integer $points
 * @method static \Illuminate\Database\Query\Builder|\App\Model\BadgeReward whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\BadgeReward whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\BadgeReward whereBadges($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\BadgeReward wherePoints($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\BadgeReward whereOrganizationId($value)
 * @mixin \Eloquent
 */
class BadgeReward extends Model
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'icon',
        'organization_id',
        'badges',
        'points',
    ];
}
