<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\NotificationType
 *
 * @property integer $id
 * @property string $name
 * @property boolean $defaultOn
 * @property \Carbon\Carbon $created_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\App\Model\NotificationType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\NotificationType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\NotificationType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\NotificationType whereDefaultOn($value)
 * @mixin \Eloquent
 */
class NotificationType extends Model
{

    /**
     * Id of "App" type.
     * @var int
     */
    const TYPE_APP = 1;

    /**
     * Id of "Email" type.
     * @var int
     */
    const TYPE_EMAIL = 2;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'defaultOn' => 'boolean'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'name',
        'defaultOn',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'defaultOn',
    ];

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
        'created_at',
    ];

    /**
     * Notification types belongs to many users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
}
