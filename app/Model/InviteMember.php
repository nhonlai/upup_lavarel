<?php


namespace App\Model;


use Illuminate\Database\Eloquent\Model;

/**
 * Class InviteMember
 *
 * @package App\Model
 * @property string $email
 * @property string $token
 * @property string $organization_id
 * @property string $team_id
 * @property \Carbon\Carbon $created_at
 * @property-read \App\Model\Organization $organization
 * @property-read \App\Model\Team $team
 * @method static \Illuminate\Database\Query\Builder|\App\Model\InviteMember whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\InviteMember whereToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\InviteMember whereOrganizationId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\InviteMember whereTeamId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\InviteMember whereCreatedAt($value)
 * @mixin \Eloquent
 */
class InviteMember extends Model
{
    /**
     * @var string
     */
    protected $primaryKey = 'email';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email' => 'string',
    ];

    /**
     * Get the name of the "updated at" column.
     *
     * @return string
     */
    public function getUpdatedAtColumn()
    {
        return null;
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function setUpdatedAt($value)
    {
        // disable updated at
        return $this;
    }

    /**
     * Get team.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
    /**
     * @param $email
     * @param $teamId
     * @return InviteMember
     */
    static public function getByEmailAndTeamId($email, $organizationId, $teamId = null)
    {
        if (!$teamId) {
            return static::where('email', '=', $email)->where('organization_id', '=', $organizationId)->firstOrNew([]);
        } else {
            return static::where('email', '=', $email)->where('team_id', '=', $teamId)->firstOrNew([]);
        }
    }

    /**
     * @param $email
     * @param $organizationId
     * @param $teamId
     *
     * @return $this
     */
    public function generateToken($email, $organizationId, $teamId = null)
    {
        $this->attributes['email'] = $email;
        $this->attributes['organization_id'] = $organizationId;
        if ($teamId)
            $this->attributes['team_id'] = $teamId;
        $this->token = md5(uniqid());
        return $this;
    }
}
