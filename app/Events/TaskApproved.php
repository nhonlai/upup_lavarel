<?php

namespace App\Events;

use App\Model\Task;
use Illuminate\Queue\SerializesModels;

/**
 * Event fires, when task is approved by manager.
 *
 * @package App\Events
 */
class TaskApproved extends Event
{
    use SerializesModels;

    /**
     * @var Task
     */
    public $task;

    /**
     * @param Task $task
     */
    public function __construct($task)
    {
        $this->task = $task;
    }
}
