<?php

namespace App\Events;

use App\Model\Task;
use Illuminate\Queue\SerializesModels;

/**
 * Event fires, when task is done by assignee.
 *
 * @package App\Events
 */
class TaskDone extends Event
{
    use SerializesModels;

    /**
     * @var Task
     */
    public $task;

    /**
     * @param Task $task
     */
    public function __construct($task)
    {
        $this->task = $task;
    }
}
