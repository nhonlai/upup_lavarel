<?php

namespace App\Jobs;

use App\Model\DeviceType;
use App\Model\Notification;
use App\Model\NotificationType;
use App\Model\User;
use App\Repositories\DeviceRepository;
use App\Services\PushServices\PushNotificationManager;
use Illuminate\Mail\Message;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNotificationJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var string
     */
    const DEFAULT_QUEUE = 'notification';

    /**
     * The id of notification.
     *
     * @var $id
     */
    protected $id;


    /**
     * Create a new job instance.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
        $this->onQueue(static::DEFAULT_QUEUE);
    }

    /**
     * Execute the job.
     *
     * @return void
     * @access public
     */
    public function handle()
    {
        /* @var Notification $notification */
        \Log::debug('[SendNotificationJob::handle] id = ' . $this->id);
        if (!$notification = Notification::find($this->id)) {
            return;
        }

        /* @var User $user */
        if (!$user = User::find($notification->user_id)) {
            return;
        }

        if ($notification->notification_type_id == NotificationType::TYPE_EMAIL) {
            $emails = $user->getNotificationEmails(true);
            $this->sendEmailNotification($notification, $emails, $user);
        } else {
            $notification->badge = 1;
            $deviceRepo = app(DeviceRepository::class);
            $devices = $deviceRepo->getDevicesForSendingPush($notification->user_id);
            $this->sendAppNotification($notification, $devices);
        }

        $this->markAsSent($notification);
    }

    /**
     * Send email notification.
     *
     * @param Notification $notification
     * @param array $emails
     * @param User $user
     * @access private
     */
    private function sendEmailNotification($notification, $emails, $user)
    {
        $subject = $notification->subject;
        $params = $notification->data ? json_decode($notification->data, TRUE) : [];

        \Mail::send(
            'emails.order-delivered',
            [
                'user' => $user,
                'text' => $notification->message,
                'params' => $params
            ],
            function (Message $mail) use ($subject, $emails) {
                $mail->to($emails)->subject($subject);
            }
        );
    }

    /**
     * Send push notification.
     *
     * @param Notification $notification
     * @param array $devices
     * @access private
     */
    private function sendAppNotification($notification, $devices)
    {
        if (empty($devices)) {
            return;
        }
        $customData = json_decode($notification->data, TRUE);
        $data = [
            'id' => (int) $notification->id,
            'type' => (int) $notification->notification_type_id,
            'subject' => $notification->subject,
            'message' => $notification->message,
            'badge' => (int) $notification->badge,
            'custom' => $customData ? $customData : NULL,
        ];

        $pushManager = PushNotificationManager::getInstance();
        if (!empty($devices[DeviceType::ANDROID])) {
            $pushManager->sendPushAndroid($devices[DeviceType::ANDROID], json_encode($data));
        }
        if (!empty($devices[DeviceType::IOS])) {
            $options = [
                'badge' => (int) $notification->badge
            ];
            $pushManager->sendPushIOS($devices[DeviceType::IOS], json_encode($data), $options);
        }
    }

    /**
     * Mark the notification as sent.
     *
     * @param Notification $notification
     * @access private
     */
    private function markAsSent($notification)
    {
        unset($notification->badge);
        $notification->delivered_at = date('Y-m-d H:i:s');
        $notification->update();
    }
}