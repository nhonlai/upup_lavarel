<?php

namespace App\Jobs;

use App\Repositories\DeviceRepository;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class SendFCM extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var string
     */
    const DEFAULT_QUEUE = 'notification';
    const TITLE = 'FCM Title';
    const BODY = 'FCM Body';
    const NOTIFICATION_ONLY = 1;
    const DATA_ONLY = 2;
    const NOTIFICATION_AND_DATA = 3;

    /**
     * @var int
     */
    protected $userId;

    /**
     * Create a new job instance.
     * @param $userId
     */
    public function __construct($userId)
    {
        $this->onQueue(static::DEFAULT_QUEUE);
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return static::TITLE;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return static::BODY;
    }

    /**
     * @return array
     */
    public function getPayloadData()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getTokens()
    {
        $deviceRepo = app(DeviceRepository::class);
        return $this->userId ? $deviceRepo->getDevicesForSendingPush($this->userId) : [];
    }

    /**
     * @param int $mode
     * @throws \LaravelFCM\Message\InvalidOptionException
     */
    public function send($mode = self::NOTIFICATION_AND_DATA)
    {
        $tokens = $this->getTokens();
        if (!$tokens) {
            \Log::debug('[Send FCM] No tokens for sending');
            return;
        }
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($this->getTitle());
        $notificationBuilder->setBody($this->getBody())
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($this->getPayloadData());

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        \Log::debug(['Send FCM', [
            'title' => $notificationBuilder->getTitle(),
            'body' => $notificationBuilder->getBody(),
            'data' => $dataBuilder->getData(),
            'tokens' => $tokens,
        ]]);

        // send notification based on mode
        switch ($mode) {
            case static::NOTIFICATION_ONLY:
                $downstreamResponse = FCM::sendTo($tokens, $option, $notification, null);
                break;
            case static::DATA_ONLY:
                $downstreamResponse = FCM::sendTo($tokens, $option, null, $data);
                break;
            default:
                $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
        }

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        // return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

        // return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify();

        // return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();

        // return Array (key:token, value:errror)
        // - in production you should remove from your database the tokens present in this array
        $downstreamResponse->tokensWithError();
    }

    /**
     * Execute the job.
     *
     * @return void
     * @access public
     */
    public function handle()
    {
        $this->send(static::NOTIFICATION_ONLY);
        $this->send(static::DATA_ONLY);
        $this->send(static::NOTIFICATION_AND_DATA);
    }
}
