<?php

namespace App\Jobs;

use App\Model\Task;

class SendTaskDoneNotification extends SendFCM
{
    const TITLE = 'Task Done';
    const BODY = 'Task is done';

    /** @var Task */
    protected $task;

    /**
     * Create a new job instance.
     */

    /**
     * SendTaskDoneNotification constructor.
     * @param Task $task
     */
    public function __construct($task)
    {
        parent::__construct($task->created_by);
        $this->task = $task;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return 'Task "' .  $this->task->title . '" is done!';
    }

    /**
     * @return array
     */
    public function getPayloadData()
    {
        return $this->task->toArray();
    }
}
