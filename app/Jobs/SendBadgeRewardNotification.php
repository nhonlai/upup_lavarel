<?php

namespace App\Jobs;

use App\Model\BadgeReward;
use App\Model\Task;

class SendBadgeRewardNotification extends SendFCM
{
    const TITLE = "You have been received a badge reward";

    /** @var Task */
    protected $task;

    /** @var BadgeReward */
    protected $reward;

    /**
     * Create a new job instance.
     */

    /**
     * SendTaskDoneNotification constructor.
     * @param Task $task
     */
    public function __construct($task)
    {
        parent::__construct($task->assignee_id);
        $this->task = $task;
        $this->reward = $task->badgeReward;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->reward ? $this->reward->title : '';
    }

    /**
     * @return array
     */
    public function getPayloadData()
    {
        return [
            'task' => $this->task->toArray(),
            'badge_reward' => $this->reward ? $this->reward->toArray() : null,
        ];
    }
}
