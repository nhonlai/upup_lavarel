#!/bin/bash

## Alias
echo -e "alias goapp='cd /home/www/app'" >> /root/.bashrc

## Create folders for laravel
echo "Creating laravel environment"
cd /home/www/app && mkdir -p app/storage/{cache,debugbar,logs,meta,sessions,views}
touch /home/www/app/storage/logs/laravel.log
#touch /home/www/app/storage/logs/push.log
chmod -R 777 /home/www/app/storage/logs/

### Chown app dir
echo "Owning app dir by nginx"
chown -R nginx:nginx /home/www/app

### Generate robots.txt
if [[ "$APP_ENV" == "development" ]]; then
    echo "create robots.txt for development environment"
    echo -e "User-agent: *\nDisallow: /" > /home/www/app/public/robots.txt
fi

cd /home/www/app/

### Generate laravel .env file
mv .env.$APP_ENV .env

### Install dependencies
composer install --prefer-source --no-interaction --optimize-autoloader

### Run migrations
php artisan migrate

### Cache routes
php artisan route:cache
php artisan api:cache
php artisan config:cache


### Start supervisord
echo "Starting nginx"
supervisorctl start nginx

echo "Starting php-fpm"
supervisorctl start php

echo "Clean up and exit"
exit 0
